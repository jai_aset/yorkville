module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        credentials: grunt.file.readJSON('credentials.json'),
        devPath: 'https-server/public',
        clean: {
            target: ["target"]
        },
        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            chrome: {
                browsers: ['Chrome'],
                singleRun: false
            },
            phantom: {
                browsers: ['PhantomJS'],
                singleRun: true,
                preprocessors: {'https-server/public/js/app/**/*.js': ['coverage']}
            }
        },
        concat: {
            widgetJS: {
                src: [
                    '<%=devPath%>/js/wrapper/start.js',
                    '<%=devPath%>/js/app/messageService.js',
                    '<%=devPath%>/js/app/loggerService.js',
                    '<%=devPath%>/js/app/timerService.js',
                    '<%=devPath%>/js/app/SFService.js',
                    '<%=devPath%>/js/app/phoneNumberService.js',
                    '<%=devPath%>/js/app/storageService.js',
                    '<%=devPath%>/js/app/windowService.js',
                    '<%=devPath%>/js/app/videoInvitationService.js',
                    '<%=devPath%>/js/app/rendererService.js',
                    '<%=devPath%>/js/app/contactService.js',
                    '<%=devPath%>/js/app/fcsService.js',
                    '<%=devPath%>/js/app/listenerService.js',
                    '<%=devPath%>/js/app/logService.js',
                    '<%=devPath%>/js/app/callService.js',
                    '<%=devPath%>/js/app/widget.js',
                    '<%=devPath%>/js/wrapper/end.js'
                ],
                dest: 'target/dev/js/widget.js'
            },
            popupJS: {
                src: [
                    '<%=devPath%>/js/wrapper/start.js',
                    '<%=devPath%>/js/app/messageService.js',
                    '<%=devPath%>/js/app/loggerService.js',
                    '<%=devPath%>/js/app/timerService.js',
                    '<%=devPath%>/js/app/SFService.js',
                    '<%=devPath%>/js/app/phoneNumberService.js',
                    '<%=devPath%>/js/app/storageService.js',
                    '<%=devPath%>/js/app/windowService.js',
                    '<%=devPath%>/js/app/audioToneService.js',
                    '<%=devPath%>/js/app/noteService.js',
                    '<%=devPath%>/js/app/rendererService.js',
                    '<%=devPath%>/js/app/contactService.js',
                    '<%=devPath%>/js/app/fcsService.js',
                    '<%=devPath%>/js/app/listenerService.js',
                    '<%=devPath%>/js/app/callService.js',
                    '<%=devPath%>/js/app/dtmfService.js',
                    '<%=devPath%>/js/app/popup.js',
                    '<%=devPath%>/js/wrapper/end.js'
                ],
                dest: 'target/dev/js/popup.js'
            },
            callLogsJS: {
                src: [
                    '<%=devPath%>/js/wrapper/start.js',
                    '<%=devPath%>/js/app/loggerService.js',
                    '<%=devPath%>/js/app/SFService.js',
                    '<%=devPath%>/js/app/storageService.js',
                    '<%=devPath%>/js/app/phoneNumberService.js',
                    '<%=devPath%>/js/app/contactService.js',
                    '<%=devPath%>/js/app/fcsService.js',
                    '<%=devPath%>/js/app/listenerService.js',
                    '<%=devPath%>/js/app/logService.js',
                    '<%=devPath%>/js/app/call_logs.js',
                    '<%=devPath%>/js/wrapper/end.js'
                ],
                dest: 'target/dev/js/calllogs.js'
            },
            videoCallJS: {
                src: [
                    '<%=devPath%>/js/wrapper/start.js',
                    '<%=devPath%>/js/app/loggerService.js',
                    '<%=devPath%>/js/app/fcsService.js',
                    '<%=devPath%>/js/app/audioToneService.js',
                    '<%=devPath%>/js/app/videoCall.js',
                    '<%=devPath%>/js/wrapper/end.js'
                ],
                dest: 'target/dev/js/videocall.js'
            }

        },
        copy: {
            dev: {
                files: [{
                        cwd: 'https-server/public/assets',
                        src: '**/*',
                        dest: 'target/dev/assets',
                        expand: true
                    }, {
                        cwd: 'https-server/public/instructions_files',
                        src: '**/*',
                        dest: 'target/dev/instructions_files',
                        expand: true
                    }, {
                        cwd: 'https-server/public/js/lib',
                        src: '**/*',
                        dest: 'target/dev/js/lib',
                        expand: true
                    }, {
                        cwd: 'https-server/public/plugin',
                        src: '**/*',
                        dest: 'target/dev/plugin',
                        expand: true
                    }, {
                        cwd: 'https-server/public',
                        src: '*.html',
                        dest: 'target/dev',
                        expand: true
                    }
                ]
            },
            prod: {
                files: [{
                        cwd: 'https-server/public/assets',
                        src: '**/*',
                        dest: 'target/prod/assets',
                        expand: true
                    }, {
                        cwd: 'https-server/public/instructions_files',
                        src: '**/*',
                        dest: 'target/prod/instructions_files',
                        expand: true
                    }, {
                        cwd: 'https-server/public/js/lib',
                        src: '**/*',
                        dest: 'target/prod/js/lib',
                        expand: true
                    }, {
                        cwd: 'https-server/public/plugin',
                        src: '**/*',
                        dest: 'target/prod/plugin',
                        expand: true
                    }, {
                        cwd: 'https-server/public',
                        src: '*.html',
                        dest: 'target/prod',
                        expand: true
                    }
                ]
            },
            beta: {
                files: [{
                        cwd: 'https-server/public/assets',
                        src: '**/*',
                        dest: 'target/beta/assets',
                        expand: true
                    }, {
                        cwd: 'https-server/public/instructions_files',
                        src: '**/*',
                        dest: 'target/beta/instructions_files',
                        expand: true
                    }, {
                        cwd: 'https-server/public/js/lib',
                        src: '**/*',
                        dest: 'target/beta/js/lib',
                        expand: true
                    }, {
                        cwd: 'https-server/public/plugin',
                        src: '**/*',
                        dest: 'target/beta/plugin',
                        expand: true
                    }, {
                        cwd: 'https-server/public',
                        src: '*.html',
                        dest: 'target/beta',
                        expand: true
                    }
                ]
            },
            forcecom: {
                files: [{
                        cwd: 'force_com-package',
                        src: '**/*',
                        dest: 'target/force_com-package',
                        expand: true
                    }
                ]
            }
        },
        replace: {
            dev: {
                src: [
                    'target/force_com-package/src/pages/*.page',
                    'target/dev/eula.html',
                    'target/dev/js/*.js'
                ],
                overwrite: true, // overwrite matched source files
                replacements: [{
                        from: /<%=contextPath%>/g,
                        to: 'https://localhost:7000'
                    }]
            },
            prod: {
                src: [
                    'target/force_com-package/src/pages/*.page',
                    'target/prod/eula.html',
                    'target/dev/js/*.js'
                ],
                overwrite: true, // overwrite matched source files
                replacements: [{
                        from: /<%=contextPath%>/g,
                        to: 'https://spidrsfdc.genband.com'
                    }]
            },
            beta: {
                src: [
                    'target/force_com-package/src/pages/*.page',
                    'target/prod/eula.html',
                    'target/dev/js/*.js'
                ],
                overwrite: true, // overwrite matched source files
                replacements: [{
                        from: /<%=contextPath%>/g,
                        to: 'https://spidrsfdc.genband.com/beta'
                    }]
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            devToProd: {
                files: [{
                        expand: true,
                        cwd: 'target/dev/js/',
                        src: ['*.js'],
                        dest: 'target/prod/js/',
                        ext: '.js'//.min.js
                    }]
            },
            fcs: {
                files: [{
                        expand: true,
                        cwd: 'target/prod/js/lib/',
                        src: ['fcs-jsl-3.1.1.js'],
                        dest: 'target/prod/js/lib/'
                    }]
            }
        },
        cssmin: {
            dev: {
                files: [{
                        expand: true,
                        cwd: 'https-server/public/css',
                        src: ['*.css'],
                        dest: 'target/dev/css/',
                        ext: '.min.css'
                    }]
            },
            beta: {
                files: [{
                        expand: true,
                        cwd: 'https-server/public/css',
                        src: ['*.css'],
                        dest: 'target/beta/css/',
                        ext: '.min.css'
                    }]
            },
            prod: {
                files: [{
                        expand: true,
                        cwd: 'https-server/public/css',
                        src: ['*.css'],
                        dest: 'target/prod/css/',
                        ext: '.min.css'
                    }]
            }
        },
        compress: {
            prod: {
                options: {
                    archive: 'target/https-server.zip'
                },
                files: [
                    {expand: true, cwd: 'target/prod/', src: ['**'], dest: 'https-server/'}

                ]
            },
            beta: {
                options: {
                    archive: 'target/https-server.zip'
                },
                files: [
                    {expand: true, cwd: 'target/beta/', src: ['**'], dest: 'https-server/'}

                ]
            },
            forcecom: {
                options: {
                    archive: 'target/force_com-package.zip'
                },
                files: [
                    {expand: true, cwd: 'target/force_com-package/', src: ['**'], dest: 'force_com-package/'}

                ]
            }
        },
        antdeploy: {
            options: {},
            // specify one deploy target
            developmentServer: {
                options: {
                    user: '<%= credentials.username %>',
                    pass: '<%= credentials.password %>',
                    token: '<%= credentials.security_token %>',
                    serverurl: 'https://login.salesforce.com', // default => https://login.salesforce.com
                    root: 'target/force_com-package/src',
                    apiVersion: '35.0'

                },
                pkg: {
                    apexpage: ['*'],
                    apexclass: ['*'],
                    staticresource: ['*'],
                    customobject: ['*'],
                    homepagecomponent: ['*'],
                    homepagelayout: ['*'],
                    customtab: ['*'],
                    custompageweblink: ['*']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-ant-sfdc');
    grunt.loadNpmTasks('grunt-karma');


    grunt.task.registerTask('tests', ['karma:phantom']);
    grunt.task.registerTask('tests_chrome', ['karma:chrome']);
    grunt.task.registerTask('build_dev', ['clean', 'karma:phantom', 'concat', 'copy:dev', 'replace:dev', 'cssmin:dev']);
    grunt.task.registerTask('build_prod', ['clean', 'karma:phantom', 'concat', 'copy:prod', 'copy:forcecom', 'replace:prod', 'uglify', 'cssmin:prod', 'compress:prod', 'compress:forcecom']);
    grunt.task.registerTask('build_beta', ['clean', 'karma:phantom', 'concat', 'copy:beta', 'copy:forcecom', 'replace:beta', 'uglify', 'cssmin:beta', 'compress:beta', 'compress:forcecom']);
    grunt.task.registerTask('deploy_dev', ['clean', 'karma:phantom', 'concat', 'copy:dev', 'copy:forcecom', 'replace:dev', 'cssmin:dev', 'antdeploy']);
    grunt.task.registerTask('deploy_prod', ['clean', 'karma:phantom', 'concat', 'copy:prod', 'copy:forcecom', 'replace:prod', 'uglify', 'cssmin:prod', 'compress:prod', 'compress:forcecom', 'antdeploy']);
    grunt.task.registerTask('deploy_beta', ['clean', 'karma:phantom', 'concat', 'copy:beta', 'copy:forcecom', 'replace:beta', 'uglify', 'cssmin:beta', 'compress:beta', 'compress:forcecom', 'antdeploy']);
};