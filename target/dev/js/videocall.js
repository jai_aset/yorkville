/*!
 * RTC Client for SFDC
 *
 * Copyright 2016, Genband
 */

(function() {
    
/* global fcsService, contactService, SFService, sforce, fcs */

var logger = new function () {

    var self = this,
            logFn = function (log) {
                console.info(log);
            },
            infoFn = function (log) {
                console.info(log);
            },
            debugFn = function (log) {
                console.debug(log);
            },
            warnFn = function (log) {
                console.warn(log);
            },
            errorFn = function (log) {
                console.error(log);
            };

    function printLog(handler, logObject) {
        var printFormat;

        if (handler && logObject) {
            printFormat = logObject.timestamp + ' - ' + logObject.logger + ' - ' + logObject.message;
            if (logObject.args) {
                handler(printFormat, logObject.args);
            } else {
                handler(printFormat);
            }
        }
    }


    function jslLogHandler(loggerName, level, logObject) {
        var LOG_LEVEL = fcs.logManager.Level;

        switch (level) {
            case LOG_LEVEL.DEBUG:
                self.debug(logObject.message, logObject);
                break;
            case LOG_LEVEL.FATAL:
            case LOG_LEVEL.ERROR:
                self.error(logObject.message, logObject);
                break;
            case LOG_LEVEL.WARN:
                self.warn(logObject.message, logObject);
                break;
            default:
                self.info(logObject.message, logObject);
        }
    }

    function createLogObject(message, logObject, logLevel, args) {
        // If logObject not defined, then it is an UI log and we need to define it
        if (!logObject) {
            logObject = {
                timestamp: Date.now(),
                logger: 'UI',
                level: logLevel,
                user: fcs.getUser(),
                message: message,
                args: args
            };
        }
        return logObject;
    }

    this.log = function (msg, logObject, args) {
        printLog(logFn, createLogObject(msg, logObject, 'LOG', args));
    };

    this.info = function (msg, logObject, args) {
        printLog(infoFn, createLogObject(msg, logObject, 'INFO', args));
    };

    this.debug = function (msg, logObject, args) {
        printLog(debugFn, createLogObject(msg, logObject, 'DEBUG', args));
    };

    this.warn = function (msg, logObject, args) {
        printLog(warnFn, createLogObject(msg, logObject, 'WARN', args));
    };

    this.error = function (msg, logObject, args) {
        if (msg.message) {
            args = {stack: msg.stack};
            msg = msg.message;
            logObject = null;
        }
        printLog(errorFn, createLogObject(msg, logObject, 'ERROR', args));
    };

    fcs.logManager.initLogging(jslLogHandler, true);
}();

/* global fcs, audioToneService, SFService, timerService, callService, rendererService, storageService, dtmfService, contactService, message, logger, phoneNumberService, windowService */

var fcsService = new function () {

    var calls = {},
            self = this,
            connected = false,
            subscriptionInProgress = false;

    window.config = {
        serverUrl: "https://localhost:7000/",
        pluginLink: "https://localhost:7000/plugin/gencomweb-plugin_3.1.502.0/GCFWEnabler.exe"
    };

    this.isConnected = function () {
        return connected;///fcs.isConnected();
    };

    this.setup = function (credentials) {
        fcs.setup({
            "restUrl": credentials.rest_server,
            "restPort": credentials.rest_port,
            "protocol": "https",
            "cors": "true",
            "notificationType": fcs.notification.NotificationTypes.WEBSOCKET,
            "websocketIP": credentials.ws_server,
            "websocketPort": credentials.ws_port,
            "websocketProtocol": "wss",
            "continuity": true,
            "services": ["call"]
        });
    };

    this.setUserAuth = function (credentials) {
        fcs.setUserAuth(credentials.username, credentials.password);
    };

    this.notificationStart = function (onSuccess, onFailure, isAnonymous) {
        fcs.notification.start(function () {
            logger.log("Notification start successful");
            onSuccess();
        }, function (error) {
            logger.warn("Notification start failed: ", error);
            onFailure(error);
        }, isAnonymous === true);
    };

    this.initMedia = function (onSuccess, onFailure) {

        SFService.loadCredentials(function (credentials) {
            if(credentials.webRtcDTLS == "false"){
                credentials.webRtcDTLS = false;
            }
            else {
                credentials.webRtcDTLS = true;
            }

            fcs.call.initMedia(onSuccess, onFailure, {
                iceserver: {
                    url: credentials.iceServerUrl
                },
                pluginMode: {
                    mode: "auto"
                },
                notificationType: "websocket",
                webrtcdtls: credentials.webRtcDTLS
            });
        });
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.setup, fcs.call.initMedia and fcs.notification.start functions
     * 
     * @param {type} onSuccess Success callback for fcs subscribtion
     * @param {type} onFailure Fail callback for fcs subscribtion
     * 
     * @example 
     * fcsService.subscribe(function(){
     *     window.console.log('Subscription successful');
     * },function(){
     *     window.console.log('Subscription failed');
     * });
     */
    this.subscribe = function (onSuccess, onFailure) {
        if (!subscriptionInProgress) {
            connected = false;
            SFService.loadCredentials(function (credentials) {
                self.setUserAuth(credentials);
                self.setup(credentials);
                self.initMedia(function () {
                    logger.log("Media initialized successfully");
                    subscriptionInProgress = true;
                    self.notificationStart(function () {
                        connected = true;
                        subscriptionInProgress = false;
                        if (onSuccess)
                            onSuccess();
                    }, function (error) {
                        subscriptionInProgress = false;
                        if (onFailure)
                            onFailure(error);
                    });
                }, function (error) {
                    logger.warn("Media initialization failed: ", error);
                    if (window.i_am_widget) {
                        if (error === fcs.call.MediaErrors.WRONG_VERSION || error === fcs.call.MediaErrors.NOT_FOUND) {
                            $("#plugin_install").show();
                            $('#download_plugin').on('click', function () {
                                window.open(window.config.pluginLink, "_blank");
                            });
                        }
                    }
                    onFailure();
                });
            });
        }
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.notification.stop function
     * 
     * @example 
     * fcsService.unsubscribe();
     */
    this.unsubscribe = function (successCallback) {
        jQuery.each(calls, function (callId, call) {
            self.endCall(callId);
        });
        if (self.isConnected()) {
            connected = false;
            if (successCallback)
                fcs.notification.stop(successCallback, function () {
                }, true);
            else
                fcs.notification.stop();
        }
    };

    this.onCallReceived = function (call) {
        var callId = call.getId();
        var callObj = {
            id: callId,
            contact: call.callerName,
            number: call.callerNumber,
            canReject: call.canReject(),
            canReceiveRemoteVideo: call.isVideoNegotationAvailable(callId)
        };
        callService.addIncomingCall(callObj);
        rendererService.renderIncomingCalls();
        if (window.i_am_widget) {
            windowService.openPopup();
        }
        contactService.getSFContacts(call.callerNumber, function (contactsMap) {
            var contacts = contactsMap[call.callerNumber];
            if (contacts && contacts.length > 0 && contacts[0].Id) {
                var contact = contacts[0];
                callService.setIncomingCallSFContact(callId, contact.Name, contact.Id);
                rendererService.renderIncomingCalls();
            }
        });
        contactService.searchSpidrContact(call.callerNumber, function (contacts) {
            if (contacts) {
                var index;
                for (index in contacts) {
                    if (call.callerName === contacts[index].firstName + ' ' + contacts[index].lastName && contacts[index].photoUrl) {
                        callService.setIncomingCallContactPhoto(callId, contacts[index].photoUrl);
                        rendererService.renderIncomingCalls();
                    }
                }
            }
        });
    };

    //just for testing
    this.addtoCalls = function (callId, call) {
        calls[callId] = call;
    };
    //just for testing
    this.deleteFromCalls = function (callId) {
        delete calls[callId];
    };

    this.startCall = function () {
        logger.log('Starting call...');
        rendererService.setStatus("connecting");
        timerService.stopCallTimer();

        var call = callService.getOutgoingCall();

        var callNumberForStorage = phoneNumberService.standardizePhoneNumber(call.number).forCompare;
        callNumberForStorage = phoneNumberService.appendDomain(callNumberForStorage);

        call.number = phoneNumberService.standardizePhoneNumber(call.number).forCall;
        call.number = phoneNumberService.appendDomain(call.number);

        logger.log(call.number + ' is getting called...');

        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            self.holdCall(activeCall.id);
        }

        fcs.call.startCall(fcs.getUser(), '', call.number, function (outgoingCall) {
            logger.log('Call start to ' + call.number + ' successful');
            callService.deleteOutgoingCall();
            var callId = outgoingCall.getId();
            //calls[callId] = outgoingCall;
            self.addtoCalls(callId, outgoingCall);

            var activeCall = {};
            activeCall.id = callId;
            activeCall.type = 'out';
            activeCall.contact = call.contact;
            activeCall.sf_contact_id = call.sf_contact_id;
            activeCall.number = callNumberForStorage;
            activeCall.status = 'active';
            activeCall.mute = 0;

            callService.addCall(activeCall);
            rendererService.renderCalls();

            contactService.searchSpidrContact(call.number, function (contacts) {
                if (contacts) {
                    var index;
                    for (index in contacts) {
                        if (contacts[index].photoUrl) {
                            callService.setCallContactPhoto(callId, contacts[index].photoUrl);
                            rendererService.renderCalls();
                        }
                    }
                }
            });

            outgoingCall._isOutgoing = true;
            setStateChange(outgoingCall);
            setStreamAdded(outgoingCall);

        }, function (error) {
            logger.warn('Call start to ' + call.number + ' failed: ' + error);
            callService.deleteOutgoingCall();
            rendererService.setStatus("call failed");
            rendererService.renderCalls();
            setTimeout(function () {
                audioToneService.stop(audioToneService.BUSY);
            }, 2000);
        }, false, false);
    };

    this.handleIncomingCall = function () {
        var incomingCalls = callService.getIncomingCalls();
        jQuery.each(incomingCalls, function (callId, incomingCall) {
            if (!calls[callId]) {
                logger.info('Handling incoming call from ' + incomingCall.number);
                var call = fcs.call.getIncomingCallById(callId);
                if (call) {
                    //calls[callId] = call;
                    self.addtoCalls(callId, call);

                    call._isIncoming = true;
                    setStateChange(call);
                    setStreamAdded(call);
                }
            }

            //removes storage item with callId composed by getIncomingCallById
            storageService.removeItem(callId);

            var command = incomingCall.command;
            if (command === 'answer') {
                self.answerAudioCall(callId);
            }
            else if (command === 'answer_video') {
                self.answerVideoCall(callId);
            }
            else if (command === 'decline') {
                self.declineCall(callId);
            }
            else if (command === 'ignore') {
                self.ignoreCall(callId);
            }
        });
        rendererService.renderIncomingCalls();
    };

    function setStateChange(call) {
        if (call) {
            var callId = call.getId();

            call.onStateChange = function (state, statusCode) {
                logger.info("callStateChange \tstate:" + state + " \tstatusCode:" + statusCode, fcs.call.States);
                switch (state) {
                    case fcs.call.States.RINGING:
                        audioToneService.play(audioToneService.RING_OUT);
                        rendererService.setStatus("ringing");
                        break;
                    case fcs.call.States.ENDED:
                        rendererService.setStatus("ended");
                        if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                            audioToneService.play(audioToneService.BUSY);

                        setTimeout(function () {
                            if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                                audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isIncoming) {
                                callService.deleteIncomingCall(callId);
                                rendererService.renderIncomingCalls();
                            }
                            rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.REJECTED:
                        rendererService.setStatus("rejected");
                        audioToneService.play(audioToneService.BUSY);
                        setTimeout(function () {
                            audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isOutgoing)
                                rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.RENEGOTIATION:
                    case fcs.call.States.TRANSFERRED:
                    case fcs.call.States.IN_CALL:
                        if (call._isIncoming)
                            audioToneService.stop(audioToneService.RING_IN);
                        else if (call._isOutgoing)
                            audioToneService.stop(audioToneService.RING_OUT);
                        rendererService.setStatus("in call");
                        callService.setCallStartTime(callId);
                        var activeCall = callService.getActiveCall();
                        timerService.setCallTimer(activeCall.start_time);
                        break;
                    case fcs.call.States.ON_REMOTE_HOLD:
                        rendererService.setStatus("call held remotely");
                        break;
                    case fcs.call.States.OUTGOING:
                        rendererService.setStatus("There is an outgoing call");
                        break;
                }
                call._state = state;
                rendererService.renderCalls();
                //rendererService.renderIncomingCalls();
            };
        }
    }

    function setStreamAdded(call) {
        if (call) {
            call.onStreamAdded = function (streamURL) {
                var activeCall = callService.getActiveCall();
                if (activeCall && call.getId() === activeCall.id) {
                    logger.info('Stream adding for ' + call.callerNumber + '...');
                    // Setting up source (src tag) of remote video container
                    if (streamURL) {
                        $("#remoteVideo").attr("src", streamURL);
                    }
                    if (call.canReceiveVideo()) {
                        logger.info('Call can receive video');
                        $("#remoteVideo").show();
                    }
                    else {
                        logger.info('Call cannot receive video');
                        $("#remoteVideo").hide();
                    }

                    if (!call.canSendVideo()) {
                        $("#localVideo").hide();
                    }
                    logger.info('Stream added for ' + call.callerNumber + ':' + streamURL);
                }
            };

            call.onLocalStreamAdded = function (streamURL) {
                logger.info('Local stream adding for ' + call.callerNumber + '...');
                // Setting up source (src tag) of remote video container
                if (call.canSendVideo()) {
                    logger.info('Call can send video');
                    $("#localVideo").attr("src", streamURL).attr('muted', true).show();
                }
                else {
                    logger.info('Call cannot send video');
                    $("#localVideo").attr("src", "");
                }
                logger.info('Local stream added for ' + call.callerNumber + ':' + streamURL);
            };
        }
    }

    function answerCall(call, video, resolution) {
        logger.log('Answering call from ' + call.callerNumber + '...');
        var callId = call.getId(),
                incomingCalls = callService.getIncomingCalls();
        var callObj = {};
        callObj.id = callId;
        callObj.type = 'in';
        callObj.contact = call.callerName;
        callObj.number = call.callerNumber;
        callObj.status = 'active';
        callObj.mute = 0;
        if (incomingCalls && incomingCalls[callId]) {
            callObj.contact = incomingCalls[callId].contact;
            if (incomingCalls[callId].sf_contact_id) {
                callObj.sf_contact_id = incomingCalls[callId].sf_contact_id;
            }
            if (incomingCalls[callId].contactPhoto) {
                callObj.contactPhoto = incomingCalls[callId].contactPhoto;
            }
        }
        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            if (activeCall.id !== callId) {
                self.holdCall(activeCall.id);
            }
        }
        callService.addCall(callObj);
        call.answer(function () {
            logger.log('Call answered successfully');
            callService.deleteIncomingCall(callId);
            rendererService.renderCalls();
            rendererService.renderIncomingCalls();
        }, function (e) {
            callService.deleteIncomingCall(callId);
            callService.endCall(callId);
            rendererService.renderCalls();
            message.error('Call answer failed', 2000);
            logger.warn('Call answer failed: ' + e);
        }, video, resolution);
    }

    this.answerAudioCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, false);
        }
    };

    this.answerVideoCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, true, '320x240');
        }
    };

    this.endCall = function (callId) {
        var call = calls[callId];
        if (call) {
            audioToneService.stop(audioToneService.RING_OUT);
            call.end(function () {
                logger.log('Call ended!');
                if (callService.isActiveCall(callId)) {
                    rendererService.setStatus("call ended");
                }
                callService.endCall(callId);
                //delete calls[callId];
                self.deleteFromCalls(callId);
                setTimeout(function () {
                    rendererService.renderCalls();
                }, 1000);
            }, function (error) {
                message.error('Call could not be ended', 2000);
                logger.warn('Call could not be ended! ' + error);
            });
        }
    };

    this.holdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Holding call ' + call.callerNumber + '...');
            call.hold(function () {
                logger.log('Call held ' + call.callerNumber);
                callService.holdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be held ' + call.callerNumber, 2000);
                logger.warn('Call could not be held! ' + error);
            });
        }
    };

    this.unholdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Unholding call ' + call.callerNumber + '...');
            var activeCall = callService.getActiveCall();
            if (activeCall) {
                logger.log('Active calls getting held...');
                this.holdCall(activeCall.id);
            }
            call.unhold(function () {
                logger.log('Call unheld ' + call.callerNumber);
                callService.unholdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be retrieved' + call.callerNumber, 2000);
                logger.warn('Call could not be retrieved! ' + error);
            });
        }
    };

    this.muteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.mute();
            callService.muteCall(callId);
            rendererService.renderCalls();
        }
    };
    this.unmuteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.unmute();
            callService.unmuteCall(callId);
            rendererService.renderCalls();
        }
    };

    this.ignoreCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.ignore(function () {
                logger.log('Ignore call successful');
            }, function () {
                message.error('Call could not be ignored', 2000);
                logger.warn('Ignore call failed');
            });
        }
        audioToneService.stop(audioToneService.RING_IN);
        callService.ignoreIncomingCall(callId);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.declineCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.reject(function () {
                logger.log('Decline call successful');
            }, function () {
                message.error('Call could not be rejected', 2000);
                logger.warn('Decline call failed');
            });
        }
        callService.declineIncomingCall(callId);
        audioToneService.stop(audioToneService.RING_IN);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.sendDTMF = function (callId, tone) {
        var call = calls[callId];
        if (call) {
            dtmfService.sendDTMF(call, tone);
        }
    };

    // searches SF contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchDirectory = function (criteria, successCallback) {
        fcs.addressbook.searchDirectory(criteria, fcs.addressbook.SearchType.USERNAME, function (contacts) {
            if (successCallback)
                successCallback(contacts);
        }, function (error) {
            message.error('Search contact failed', 2000);
            logger.warn('Search contact failed :' + error);
        });
    };

    this.retrieveCalllog = function (onSuccess, onFailure) {
        SFService.loadCredentials(function (credentials) {
            self.setUserAuth(credentials);
            self.setup(credentials);
            fcs.calllog.retrieve(onSuccess, onFailure);
        });
    };
}();

var audioToneService = new function () {

    // initializes audio tone playing
    this.init = function () {
        var $audioTonesDiv = $('#audio_tones');

        this.useAudioTag = (typeof window.HTMLAudioElement !== "undefined");

        if (this.useAudioTag) {
            this.RING_IN = $("<audio id='ring_in_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringin.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringin.ogg' type='audio/ogg'></audio>")[0];
            this.RING_OUT = $("<audio id='ring_out_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringout.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringout.ogg' type='audio/ogg'></audio>")[0];
            this.BUSY = $("<audio id='busy_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/busy.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/busy.ogg' type='audio/ogg'></audio>")[0];
        }
        else {
            this.RING_IN = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/ringin.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            this.RING_OUT = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/ringout.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            this.BUSY = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/busy.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            $audioTonesDiv.removeAttr('style');
        }

        $audioTonesDiv.append(this.RING_IN);
        $audioTonesDiv.append(this.RING_OUT);
        $audioTonesDiv.append(this.BUSY);
    };

    // start playing tone
    this.play = function (tone) {
        if (!tone || tone.readyState === 0) {
            return;
        }

        if (this.useAudioTag) {
            tone.play();
        } else {
            tone.Play();
        }
    };

    // stops playing tone
    this.stop = function (tone) {
        if (!tone || tone.readyState === 0) {
            return;
        }

        if (this.useAudioTag) {
            tone.pause();
            tone.currentTime = 0;
        }
        else {
            tone.StopPlay();
        }
    };
}();
/* global fcs, fcsService, audioToneService */

(function () {
    var isAnonymous = true,
            contact = {},
            mediaDeferred = $.Deferred(),
            $videoContainer = $("#plug-in-container"),
            currentCall = null,
            intraframe = null,
            hashParams = getUrlHashParameters(window.location.hash),
            address = hashParams.to,
            streamURL;

    if (window._testonly) {
        window.setHashParams = function (hash) {
            hashParams = getUrlHashParameters(hash);
        };
        window.setCurrentCall = function (call) {
            currentCall = call;
        };
    }

    audioToneService.init();

    $(function () {
        $("#media_plugin_dialog").dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 358,
            height: 235,
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Download: function () {
                    window.open(window.config.pluginLink, "_blank");
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            open: function () {

            }
        });
    });

    $('#start_button').on('click', function () {
        var subscribeDeferred = $.Deferred(),
                valid_from = hashParams.valid_from,
                valid_to = hashParams.valid_to,
                rest_server = hashParams.rest_server,
                rest_port = hashParams.rest_port,
                webSocketServer = hashParams.ws_server,
                websocketPort = hashParams.ws_port,
                current_time = +new Date;

        $('.error').hide();
        // Check url validity
        if (valid_from && current_time < valid_from) {
            $('#meeting_not_started_error').show();
            return;
        }

        if (valid_to && current_time > valid_to) {
            $('#meeting_over_error').show();
            return;
        }

        contact.firstName = $('#first_name').val().trim();
        contact.lastName = $('#last_name').val().trim();

        if (!contact.firstName.length && !contact.lastName.length) {
            $('#no_name_error').show();
            return;
        }

        fcsService.setUserAuth({
            username: address
        });

        fcsService.setup({
            rest_server: rest_server,
            rest_port: rest_port,
            ws_server: webSocketServer,
            ws_port: websocketPort
        });

        fcsService.notificationStart(
                function () {
                    subscribeDeferred.resolve();
                },
                function (e) {
                    console.log("Subscribe error: ", e);
                    subscribeDeferred.reject();
                },
                isAnonymous
                );

        if (mediaDeferred.state() === "pending") {
            fcsService.initMedia(
                    function () {
                        window.console.log("Media initialized successfully");
                        mediaDeferred.resolve();
                    },
                    function (error) {
                        window.console.log("Media initialization error: ", error);
                        $('#media_plugin_dialog').dialog("open");
                        mediaDeferred.reject();
                    }
            );
        }

        $.when(mediaDeferred, subscribeDeferred).done(function () {
            fcs.call.startCall(
                    'sip:anonymous@invalid.net',
                    contact,
                    address,
                    videoCallSuccess,
                    videoCallFailure,
                    true,
                    true
                    );
        });
    });

    $('#end_call').on('click', function () {
        audioToneService.stop(audioToneService.RING_OUT);

        if (!currentCall) {
            return;
        }

        currentCall.end(function () {
            disposeStream();
            currentCall = null;
            toggleLoginPage(true);
        });
    });

    function videoCallSuccess(call) {
        audioToneService.play(audioToneService.RING_OUT);
        currentCall = call;

        var fcsCallStates = fcs.call.States;

        call.onStreamAdded = function (url) {
            streamURL = url;
            renderStream();
        };

        call.onStateChange = function (state, statusCode) {
            //Play the ring out when we receive the 180
            if (state === fcsCallStates.RINGING && statusCode !== "183") {
                audioToneService.play(audioToneService.RING_OUT);
            } else {
                audioToneService.stop(audioToneService.RING_OUT);
            }

            if (state === fcsCallStates.IN_CALL) {
                audioToneService.stop(audioToneService.RING_OUT);

                // Workaround for video plugin, which shows local streaming
                // only after container resize
                $videoContainer.height(400);
                startIntraFrame();
            } else if (state === fcsCallStates.ENDED) {
                disposeStream();
                toggleLoginPage(true);
            } else if (state === fcsCallStates.ON_HOLD) {
                disposeStream();
                stopIntraFrame();
            }
        };

        toggleLoginPage(false);
    }

    function videoCallFailure(e) {
        // TODO: Handle error case
    }

    function stopIntraFrame() {
        if (intraframe) {
            clearInterval(intraframe);
        }
    }

    function startIntraFrame() {
        intraframe = setInterval(function () {
            if (currentCall && currentCall.canSendVideo()) {
                currentCall.sendIntraFrame();
            } else {
                stopIntraFrame();
            }
        },
                5000);
    }

    function toggleLoginPage(show) {
        show ? $videoContainer.removeClass('visible') : $videoContainer.addClass('visible');
        $('#login_content').toggle(show);
        $('#buttons').toggle(!show);

        if (!show) {
            return;
        }

        clearResources(
                function () {
                    setTimeout(function () {
                        if (!window._testonly) {
                            window.location.reload();
                        }
                    }, 1000);
                },
                true,
                false
                );
    }

    function getUrlHashParameters(hash) {
        var paramsStart = hash.indexOf('?'),
                pairString,
                pairs = {},
                pairStrings = paramsStart > -1 ? hash.slice(paramsStart + 1).split('&') : [],
                pair = {};
        for (var i = 0; i < pairStrings.length; i++) {
            pairString = pairStrings[ i ];
            pair = pairString.split('='); // Split into name/value array
            pairs[ pair[ 0 ] ] = pair[ 1 ];
        }
        return pairs;
    }

    function clearResources(done, clearUserCredentials, synchronous) {
        fcs.notification.stop(function () {
            //onsuccess
            window.localStorage.removeItem("SubscriptionStamp");
        }, function () {
            //onfailure, can be used in the future
        }, true);
        if (clearUserCredentials) {
            window.localStorage.removeItem("USERNAME");
            window.localStorage.removeItem("PASSWORD");
        }
        if (typeof done === 'function') {
            done();
        }
    }

    function renderStream() {
        streamURL && fcs.call.createStreamRenderer(streamURL, $("#plug-in-container").get(0));
    }

    function disposeStream() {
        streamURL && fcs.call.disposeStreamRenderer(streamURL, $("#plug-in-container").get(0));
    }
})();
})( );