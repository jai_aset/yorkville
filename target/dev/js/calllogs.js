/*!
 * RTC Client for SFDC
 *
 * Copyright 2016, Genband
 */

(function() {
    
/* global fcsService, contactService, SFService, sforce, fcs */

var logger = new function () {

    var self = this,
            logFn = function (log) {
                console.info(log);
            },
            infoFn = function (log) {
                console.info(log);
            },
            debugFn = function (log) {
                console.debug(log);
            },
            warnFn = function (log) {
                console.warn(log);
            },
            errorFn = function (log) {
                console.error(log);
            };

    function printLog(handler, logObject) {
        var printFormat;

        if (handler && logObject) {
            printFormat = logObject.timestamp + ' - ' + logObject.logger + ' - ' + logObject.message;
            if (logObject.args) {
                handler(printFormat, logObject.args);
            } else {
                handler(printFormat);
            }
        }
    }


    function jslLogHandler(loggerName, level, logObject) {
        var LOG_LEVEL = fcs.logManager.Level;

        switch (level) {
            case LOG_LEVEL.DEBUG:
                self.debug(logObject.message, logObject);
                break;
            case LOG_LEVEL.FATAL:
            case LOG_LEVEL.ERROR:
                self.error(logObject.message, logObject);
                break;
            case LOG_LEVEL.WARN:
                self.warn(logObject.message, logObject);
                break;
            default:
                self.info(logObject.message, logObject);
        }
    }

    function createLogObject(message, logObject, logLevel, args) {
        // If logObject not defined, then it is an UI log and we need to define it
        if (!logObject) {
            logObject = {
                timestamp: Date.now(),
                logger: 'UI',
                level: logLevel,
                user: fcs.getUser(),
                message: message,
                args: args
            };
        }
        return logObject;
    }

    this.log = function (msg, logObject, args) {
        printLog(logFn, createLogObject(msg, logObject, 'LOG', args));
    };

    this.info = function (msg, logObject, args) {
        printLog(infoFn, createLogObject(msg, logObject, 'INFO', args));
    };

    this.debug = function (msg, logObject, args) {
        printLog(debugFn, createLogObject(msg, logObject, 'DEBUG', args));
    };

    this.warn = function (msg, logObject, args) {
        printLog(warnFn, createLogObject(msg, logObject, 'WARN', args));
    };

    this.error = function (msg, logObject, args) {
        if (msg.message) {
            args = {stack: msg.stack};
            msg = msg.message;
            logObject = null;
        }
        printLog(errorFn, createLogObject(msg, logObject, 'ERROR', args));
    };

    fcs.logManager.initLogging(jslLogHandler, true);
}();

/* global  sforce, logger */

var SFService = new function () {
    var self = this;

    // initializes sforce session id
    this.initSFApi = function () {
        if (!window.__sfdcSessionId) {
            sforce.sessionId = getCookie('sid');
        } else {
            sforce.sessionId = window.__sfdcSessionId;
        }

        sforce.connection.sessionId = sforce.sessionId;

        if (sforce.connection.sessionId) {
            logger.log('sforce api initialized successfully');
        } else {
            logger.warn('sforce api initialization error');
        }
    };

    // returns ajax request for SF (apex classes)
    this.loadSFData = function (methodName, param) {
        if (!param)
            param = '';
        var requestBody = '<se:Envelope xmlns:se="http://schemas.xmlsoap.org/soap/envelope/">' +
                '<se:Header xmlns:sfns="http://soap.sforce.com/schemas/package/GenbandController">' +
                '<sfns:SessionHeader><sessionId>' + sforce.sessionId + '</sessionId></sfns:SessionHeader>' +
                '</se:Header>' +
                '<se:Body>' +
                '<' + methodName + ' xmlns="http://soap.sforce.com/schemas/package/GenbandController">' +
                param +
                '</' + methodName + '>' +
                '</se:Body>' +
                '</se:Envelope>';
        return $.ajax({
            url: '../services/Soap/package/GenbandController',
            type: 'POST',
            data: requestBody,
            headers: {
                "SOAPAction": '""',
                "Content-Type": "text/xml; charset=UTF-8"
            }
        });
    };

    // parses result retrieved from SF
    this.parseSFResult = function (xml) {
        return $(xml).find('result').text();
    };

    // retrieves credentials from SF async and calls successCallback with credentials object
    this.loadCredentials = function (successCallback) {
        $.when(
                self.loadSFData('loadGenbandCredentials')
                ).done(function (creds) {
                    var credentials = {};
                    jscreds = self.parseSFResult(creds);
                    parsedCreds = JSON.parse(jscreds);

                    var sp_server =  parsedCreds[2];
                    var sp_wsServer = parsedCreds[3];

                    credentials.username = parsedCreds[0];
                    credentials.password = parsedCreds[1];
                    credentials.iceServerUrl = parsedCreds[4];
                    credentials.webRtcDTLS = parsedCreds[3];
                    var url = document.createElement('a');

                    if (sp_server.indexOf("://") === -1) {
                        url.href = "https://" + sp_server;
                    } else {
                        url.href = sp_server;
                    }

                    credentials.rest_server = url.hostname;
                    credentials.rest_port = getPort(url);

                    var webSocketServerUrl = document.createElement('a');

                    if (sp_wsServer.indexOf("://") === -1) {
                        webSocketServerUrl.href = "http://" + sp_wsServer;
                    } else {
                        webSocketServerUrl.href = sp_wsServer;
                    }

                    credentials.ws_server = webSocketServerUrl.hostname;
                    credentials.ws_port = getPort(webSocketServerUrl);

                if (successCallback) {
                    successCallback(credentials);
                }
        });

        function getPort(url) {
            if (!url.port) {
                if (url.protocol === "https:") {
                    return 443;
                } else if (url.protocol === "http:") {
                    return 80;
                }
            } else {
                return url.port;
            }
        }
    };

    if (!window._testonly)
        this.initSFApi();
}();
var storageService = new function () {
    var self = this;
    
    // if any change in localstorage, calls storageEventHandler function with parameter StorageEvent
    this.listen = function (storageEventHandler) {
        window.addEventListener('storage', storageEventHandler, false);
    };

    // returns item from localstorage as string
    this.getItem = function (key) {
        return localStorage.getItem(key);
    };

    // returns item from localstorage as JSON object
    this.getItemAsJSON = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };

    // sets value of item
    this.setItem = function (key, value) {
        localStorage.setItem(key, value);
    };

    // converts JSON object to string and sets as value of item
    this.setItemFromJSON = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };

    // removes item from localstorage
    this.removeItem = function (key) {
        localStorage.removeItem(key);
    };

    // clears localstorage saved by app
    this.clear = function () {
        self.removeItem('calls');
        self.removeItem('call_status');
        self.removeItem('incoming_call');
        self.removeItem('outgoing_call');
        self.removeItem('end_call');
        self.removeItem('hold_call');
        self.removeItem('unhold_call');
        self.removeItem('mute_call');
        self.removeItem('unmute_call');
        self.removeItem('popup_opened');
    };
}();
/* global fcs */

var phoneNumberService = new function () {
    var self = this;

    //Convert a valid phone number to a standard format:
    //No non-numeric character except initial plus (+)
    //E.g. +1234567890
    this.standardizePhoneNumber = function (number) {
        var forCall = number.trim().replace(/\s+/g, ""),
                forDisplay = forCall, forCompare = forCall, checkedPhoneNumber;
        if (isPhoneNumberForService(forCall)) {
            forDisplay = forCall;
            forCompare = forCall;
        }
        else if (isPhoneNumberValid(forCall)) {
            forDisplay = forCall.replace(/\D+/g, "");
            checkedPhoneNumber = checkPlusAndDoubleZero({forCall: forCall, forDisplay: forDisplay, forCompare: forDisplay});
            forCall = checkedPhoneNumber.forCall;
            forDisplay = checkedPhoneNumber.forDisplay;
            forCompare = checkedPhoneNumber.forCompare;
        }
        return {forCall: forCall, forDisplay: forDisplay, forCompare: forCompare};
    };

    function checkPlusAndDoubleZero(phoneNumber) {
        var forCall = phoneNumber.forCall,
                forDisplay = phoneNumber.forDisplay,
                forCompare = phoneNumber.forCompare;
        if (forCall.substring(0, 2) === '00') {
            forCall = forDisplay;
            forDisplay = '+' + forDisplay.replace(/00/, "");
        }
        else if (forCall.substring(0, 1) === '+' || forCall.substring(0, 2) === '(+') {
            forDisplay = forCall.replace(/\D+/g, "");
            forCall = '+' + forDisplay;
            forDisplay = forCall;
        }
        else {
            forDisplay = forCall.replace(/\D+/g, "");
            forCall = forDisplay;
        }

        return {forCall: forCall, forDisplay: forDisplay, forCompare: forCompare};
    }

    //Check whether the number is possibly a phone number
    //Followings are some valid examples:
    //123456789
    //+123456789
    //+1 234-56-789
    //+1 (234) 56-789
    //(+1) 234 56-789
    //*25#
    function isPhoneNumberValid(number) {
        var newNumber = number.replace(/\s+/g, ""), isValid,
                phoneNumberRegex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
        isValid = phoneNumberRegex.test(newNumber) || isPhoneNumberForService(number);
        return isValid;
    }

    //Check whether the number is a service number
    //E.g. *25#, *123# etc.
    function isPhoneNumberForService(number) {
        var newNumber = number.trim(), isServiceNumber,
                serviceNumberRegex = /^\*\d+#$/;
        isServiceNumber = serviceNumberRegex.test(newNumber);
        return isServiceNumber;
    }

    this.appendDomain = function (number) {
        var userDomain,
                emailRegex = new RegExp('\\S+@\\S+\\.\\S+');

        if (!emailRegex.test(number)) {
            userDomain = fcs.getDomain();
            if (userDomain) {
                return number + '@' + userDomain;
            }
        }
        return number;
    };
}();
/* global sforce, callService, SFService, fcsService, message, storageService, logger, windowService */

var contactService = new function () {
    var container_selector = '#add_contact_dialog',
            dialogCaller,
            self = this;

    // opens add contact dialog (create if not exists, otherwise show)
    this.openDialog = function (number) {
        dialogCaller = number;
        if (!$(container_selector).hasClass('ui-dialog-content')) {
            createDialog();
        }
        $(container_selector).dialog("open");
    };
    // creates add contact dialog
    function createDialog(caller) {
        $(container_selector).dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 400,
            height: 'auto',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Add: function () {
                    addContact();
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            open: function () {
                var number;
                // Set default value of phone field
                if (dialogCaller) {
                    number = dialogCaller.substr(0, dialogCaller.indexOf('@'));
                    if (number && !isNaN(number)) {
                        $(this).find('#add_contact_phone').val(number);
                    } else {
                        $(this).find('#add_contact_phone').val(dialogCaller);
                    }
                }
                $('#contact-existance').hide();
            }
        });
    }

    // validates form and checks existing (similar) contacts
    function addContact() {
        var $dialog = $(container_selector),
                caller = $dialog.find('#add_contact_phone').val(),
                firstName = $dialog.find('#add_contact_firstname').val(),
                lastName = $dialog.find('#add_contact_lastname').val(),
                type = $('#add_contact_select_type').val(),
                contactId = null;

        $dialog.off('click');
        if (lastName) {
            var contactInfo = {
                'firstName': firstName || '',
                'lastName': lastName
            },
            contact = new sforce.SObject('Contact'),
                    contacts = sforce.apex.execute("GenbandController", "checkContactExistence", contactInfo);

            if (contacts.length) {
                $('#contact-existance').show();
                $('.existed-contacts').empty();

                contacts.forEach(function (item) {
                    if (item.Name) {
                        $('.existed-contacts').append('<div class="contacts-container"><span class="existed-contact" data-id="' + item.Id + '">' + item.Name + '</span></div>');
                    }
                });

                $dialog.on('click', '.existed-contact', function () {
                    var $this = $(this),
                            type = $('#add_contact_select_type').val(), // Checking If User Change His Decision
                            result = null;

                    contact.Id = $this.data('id');
                    contact[type] = caller;

                    result = sforce.connection.update([contact]);

                    if (result[0].getBoolean('success')) {
                        self.setContactTitle(caller);
                        $dialog.dialog("close");
                    } else {
                        message.error('Contact update failed', 2000);
                    }
                });

                $dialog.on('click', '#add-contact-anyway', function () {
                    contactId = addContactToSalesforce(caller, type, firstName, lastName);
                    self.setContactTitle(caller);
                    $dialog.dialog("close");
                });
            } else {
                contactId = addContactToSalesforce(caller, type, firstName, lastName);
                self.setContactTitle(caller);
                $dialog.dialog("close");
            }
        } else {
            message.warning('Please Enter a Last Name', 2000);
        }
    }

    // saves contact to SF
    function addContactToSalesforce(phoneNumber, type, firstName, lastName) {
        var objContact = new sforce.SObject("Contact"),
                result = null;

        objContact[type] = phoneNumber;
        objContact.FirstName = firstName;
        objContact.LastName = lastName;
        objContact.OwnerId = window.UserContext.userId;

        result = sforce.connection.create([objContact]);

        return result[0].id;
    }

    // sets contact title of active call according to SF contact 
    this.setContactTitle = function (number, name) {
        logger.log('Setting contact title for number ' + number);
        var activeCall = callService.getActiveCall();
        if (activeCall.sf_contact_id) {
            logger.log('There is an active call with sf contact id: ' + activeCall.sf_contact_id);
            $("#active_call").find("#contact").html("<a id='contact_redirect' href='javascript:'>" + activeCall.contact + "</a>");
            $('#contact_redirect').click(function () {
                windowService.openContactPage(activeCall.sf_contact_id);
            });
        } else {
            logger.log('There is no active call to set title');
            if (!name)
                name = number;
            //number = '(212) 842-5611';//'meetme@genband.com';
            $("#active_call").find("#contact").text(name);
            $("#active_call").find("#contact").attr('title', number);
            if (number) {
                logger.log('Getting SF contacts with number: ' + number);
                self.getSFContacts(number, function (contactsMap) {
                    logger.log('Contacts map found: ' + JSON.stringify(contactsMap));
                    var contacts = contactsMap[number];
                    if (contacts && contacts.length > 0 && contacts[0].Id) {
                        var contact = contacts[0];
                        logger.log('The contact is found from map:' + JSON.stringify(contact));
                        callService.setCallSFContact(activeCall.id, contact.Id);
                        $("#active_call").find("#contact").html("<a id='contact_redirect' href='javascript:'>" + contact.Name + "</a>");
                        $('#contact_redirect').click(function () {
                            windowService.openContactPage(contact.Id);
                        });
                    } else {
                        logger.log('Contact not found from map. click for add');
                        $("#active_call").find("#contact").html("<a href='javascript:' id='contact_title' target='_parent'>" + number + " (Add Contact)</a>");
                        $('#popup').on('click', '#contact_title', function () {
                            if (!$("#add_contact_dialog").hasClass('ui-dialog-content')) {
                                contactService.openDialog(number);
                            }
                            $("#add_contact_dialog").dialog("open");
                        });
                    }
                });
            }
        }
    };

    // gets SF contact according to search param
    this.getSFContacts = function (callerId, onSuccess, onFailure) {
        SFService.loadSFData('getContacts', '<logAddresses>' + callerId + '</logAddresses>')
                .done(function (data) {
                    var result,
                            response = xmlToJson(data),
                            resultJson = response['soapenv:Envelope']['soapenv:Body']['getContactsResponse']['result']['#text'];
                    result = JSON.parse(resultJson);
                    onSuccess(result);
                })
                .fail(function () {
                    if (onFailure)
                        onFailure();
                });
    };

    // converts XML to JSON
    function xmlToJson(xml) {
        var obj = {};
        if (xml.nodeType === 1) {
            if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType === 3) {
            obj = xml.nodeValue;
        }
        if (xml.hasChildNodes()) {
            for (var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof (obj[nodeName]) === "undefined") {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if (typeof (obj[nodeName].push) === "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
        return obj;
    }

    // searches SF contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchSFContact = function (criteria, successCallback) {
        var callbacks = {
            onSuccess: function onSuccess(queryResult, source) {
                var contacts = null;
                if (queryResult.size > 0) {
                    contacts = queryResult.getArray('records');
                }
                if (successCallback)
                    successCallback(contacts);
            },
            onFailure: function (error) {
                logger.warn('searchContact: ' + error);
            }
        };
        var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone FROM Contact";
        if (criteria)
            query += " WHERE Name LIKE '%" + criteria + "%'";
        sforce.connection.query(query, callbacks);
    };

    // gets all SF contacts calls successCallback with contacts parameter
    this.getAllSFContacts = function (successCallback) {
        self.searchSFContact(null, successCallback);
    };

    // searches Spidr contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchSpidrContact = function (criteria, successCallback) {
        fcsService.searchDirectory(criteria, successCallback);
    };

    // checks whether user navigated to a contact page. If so, calls successCallback with contact parameter
    this.checkCurrentContact = function (successCallback, failureCallback) {
        var entityId = window.__currentEntityId;
        var entityType = window.__currentEntityType;

        var contactId = window.__currentContactId;
//        if (contactId && contactId.length > 0) {
//            var callbacks = {
        var query = '';
        logger.log('checkCurrent[Entity]: entityId ' + entityId + 'entityType ' + entityType);
//        logger.log('checkCurrentContact: contactId ' + contactId + 'leadId ' + leadId + 'opportunityId ' + opportunityId + 'personId ' + personId + 'accountId ' + accountId);
        if (entityType && entityId)
        {
           logger.log('checkCurrent[Entity]: passed type check  ' + entityType);
           
           switch (entityType)
           {
               case 'Lead':
                   query = "SELECT Id, Name, Email, Phone, MobilePhone FROM Lead " +
                    "WHERE Id = '" + entityId + "'";
                    break;
                case 'Opportunity':
                    query = "SELECT Id, Name, Email_Address__c, Telephone__c, Cell_Phone__c FROM Opportunity " +
                    "WHERE Id = '" + entityId + "'";
                    break;
                case 'Contact':
                    query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
                          "FROM Contact WHERE Id='" + entityId + "'";
                   break;
                
                default:
                    break;
           }
           var callbacks = {
                onSuccess: function onSuccess(queryResult, source) {
                    var contacts = null;
                    if (queryResult.size > 0) {
                        window.name = entityId;
                        contacts = queryResult.getArray('records');
                        $.each(contacts, function (index, contact) {
                            if (contact && successCallback) {
                                successCallback(contact);
                            }
                        });
                    }
                },
                onFailure: function (error) {
                    logger.warn('checkCurrentContact: ' + error);
                }
            };
//            var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
//                    "FROM Contact WHERE Id='" + contactId + "'";
            sforce.connection.query(query, callbacks);
        }
        // leaving in contactid query for backwards compatibility
        else if (contactId && contactId.length > 0) {
            var callbacks = {
                onSuccess: function onSuccess(queryResult, source) {
                    var contacts = null;
                    if (queryResult.size > 0) {
                        window.name = contactId;
                        contacts = queryResult.getArray('records');
                        $.each(contacts, function (index, contact) {
                            if (contact && successCallback) {
                                successCallback(contact);
                            }
                        });
                    }
                },
                onFailure: function (error) {
                    logger.warn('checkCurrentContact: ' + error);
                }
            };
            var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
                    "FROM Contact WHERE Id='" + contactId + "'";
            sforce.connection.query(query, callbacks);
        }

        else if (failureCallback) {
            failureCallback();
        }
    };

}();
/* global fcs, audioToneService, SFService, timerService, callService, rendererService, storageService, dtmfService, contactService, message, logger, phoneNumberService, windowService */

var fcsService = new function () {

    var calls = {},
            self = this,
            connected = false,
            subscriptionInProgress = false;

    window.config = {
        serverUrl: "https://localhost:7000/",
        pluginLink: "https://localhost:7000/plugin/gencomweb-plugin_3.1.502.0/GCFWEnabler.exe"
    };

    this.isConnected = function () {
        return connected;///fcs.isConnected();
    };

    this.setup = function (credentials) {
        fcs.setup({
            "restUrl": credentials.rest_server,
            "restPort": credentials.rest_port,
            "protocol": "https",
            "cors": "true",
            "notificationType": fcs.notification.NotificationTypes.WEBSOCKET,
            "websocketIP": credentials.ws_server,
            "websocketPort": credentials.ws_port,
            "websocketProtocol": "wss",
            "continuity": true,
            "services": ["call"]
        });
    };

    this.setUserAuth = function (credentials) {
        fcs.setUserAuth(credentials.username, credentials.password);
    };

    this.notificationStart = function (onSuccess, onFailure, isAnonymous) {
        fcs.notification.start(function () {
            logger.log("Notification start successful");
            onSuccess();
        }, function (error) {
            logger.warn("Notification start failed: ", error);
            onFailure(error);
        }, isAnonymous === true);
    };

    this.initMedia = function (onSuccess, onFailure) {

        SFService.loadCredentials(function (credentials) {
            if(credentials.webRtcDTLS == "false"){
                credentials.webRtcDTLS = false;
            }
            else {
                credentials.webRtcDTLS = true;
            }

            fcs.call.initMedia(onSuccess, onFailure, {
                iceserver: {
                    url: credentials.iceServerUrl
                },
                pluginMode: {
                    mode: "auto"
                },
                notificationType: "websocket",
                webrtcdtls: credentials.webRtcDTLS
            });
        });
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.setup, fcs.call.initMedia and fcs.notification.start functions
     * 
     * @param {type} onSuccess Success callback for fcs subscribtion
     * @param {type} onFailure Fail callback for fcs subscribtion
     * 
     * @example 
     * fcsService.subscribe(function(){
     *     window.console.log('Subscription successful');
     * },function(){
     *     window.console.log('Subscription failed');
     * });
     */
    this.subscribe = function (onSuccess, onFailure) {
        if (!subscriptionInProgress) {
            connected = false;
            SFService.loadCredentials(function (credentials) {
                self.setUserAuth(credentials);
                self.setup(credentials);
                self.initMedia(function () {
                    logger.log("Media initialized successfully");
                    subscriptionInProgress = true;
                    self.notificationStart(function () {
                        connected = true;
                        subscriptionInProgress = false;
                        if (onSuccess)
                            onSuccess();
                    }, function (error) {
                        subscriptionInProgress = false;
                        if (onFailure)
                            onFailure(error);
                    });
                }, function (error) {
                    logger.warn("Media initialization failed: ", error);
                    if (window.i_am_widget) {
                        if (error === fcs.call.MediaErrors.WRONG_VERSION || error === fcs.call.MediaErrors.NOT_FOUND) {
                            $("#plugin_install").show();
                            $('#download_plugin').on('click', function () {
                                window.open(window.config.pluginLink, "_blank");
                            });
                        }
                    }
                    onFailure();
                });
            });
        }
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.notification.stop function
     * 
     * @example 
     * fcsService.unsubscribe();
     */
    this.unsubscribe = function (successCallback) {
        jQuery.each(calls, function (callId, call) {
            self.endCall(callId);
        });
        if (self.isConnected()) {
            connected = false;
            if (successCallback)
                fcs.notification.stop(successCallback, function () {
                }, true);
            else
                fcs.notification.stop();
        }
    };

    this.onCallReceived = function (call) {
        var callId = call.getId();
        var callObj = {
            id: callId,
            contact: call.callerName,
            number: call.callerNumber,
            canReject: call.canReject(),
            canReceiveRemoteVideo: call.isVideoNegotationAvailable(callId)
        };
        callService.addIncomingCall(callObj);
        rendererService.renderIncomingCalls();
        if (window.i_am_widget) {
            windowService.openPopup();
        }
        contactService.getSFContacts(call.callerNumber, function (contactsMap) {
            var contacts = contactsMap[call.callerNumber];
            if (contacts && contacts.length > 0 && contacts[0].Id) {
                var contact = contacts[0];
                callService.setIncomingCallSFContact(callId, contact.Name, contact.Id);
                rendererService.renderIncomingCalls();
            }
        });
        contactService.searchSpidrContact(call.callerNumber, function (contacts) {
            if (contacts) {
                var index;
                for (index in contacts) {
                    if (call.callerName === contacts[index].firstName + ' ' + contacts[index].lastName && contacts[index].photoUrl) {
                        callService.setIncomingCallContactPhoto(callId, contacts[index].photoUrl);
                        rendererService.renderIncomingCalls();
                    }
                }
            }
        });
    };

    //just for testing
    this.addtoCalls = function (callId, call) {
        calls[callId] = call;
    };
    //just for testing
    this.deleteFromCalls = function (callId) {
        delete calls[callId];
    };

    this.startCall = function () {
        logger.log('Starting call...');
        rendererService.setStatus("connecting");
        timerService.stopCallTimer();

        var call = callService.getOutgoingCall();

        var callNumberForStorage = phoneNumberService.standardizePhoneNumber(call.number).forCompare;
        callNumberForStorage = phoneNumberService.appendDomain(callNumberForStorage);

        call.number = phoneNumberService.standardizePhoneNumber(call.number).forCall;
        call.number = phoneNumberService.appendDomain(call.number);

        logger.log(call.number + ' is getting called...');

        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            self.holdCall(activeCall.id);
        }

        fcs.call.startCall(fcs.getUser(), '', call.number, function (outgoingCall) {
            logger.log('Call start to ' + call.number + ' successful');
            callService.deleteOutgoingCall();
            var callId = outgoingCall.getId();
            //calls[callId] = outgoingCall;
            self.addtoCalls(callId, outgoingCall);

            var activeCall = {};
            activeCall.id = callId;
            activeCall.type = 'out';
            activeCall.contact = call.contact;
            activeCall.sf_contact_id = call.sf_contact_id;
            activeCall.number = callNumberForStorage;
            activeCall.status = 'active';
            activeCall.mute = 0;

            callService.addCall(activeCall);
            rendererService.renderCalls();

            contactService.searchSpidrContact(call.number, function (contacts) {
                if (contacts) {
                    var index;
                    for (index in contacts) {
                        if (contacts[index].photoUrl) {
                            callService.setCallContactPhoto(callId, contacts[index].photoUrl);
                            rendererService.renderCalls();
                        }
                    }
                }
            });

            outgoingCall._isOutgoing = true;
            setStateChange(outgoingCall);
            setStreamAdded(outgoingCall);

        }, function (error) {
            logger.warn('Call start to ' + call.number + ' failed: ' + error);
            callService.deleteOutgoingCall();
            rendererService.setStatus("call failed");
            rendererService.renderCalls();
            setTimeout(function () {
                audioToneService.stop(audioToneService.BUSY);
            }, 2000);
        }, false, false);
    };

    this.handleIncomingCall = function () {
        var incomingCalls = callService.getIncomingCalls();
        jQuery.each(incomingCalls, function (callId, incomingCall) {
            if (!calls[callId]) {
                logger.info('Handling incoming call from ' + incomingCall.number);
                var call = fcs.call.getIncomingCallById(callId);
                if (call) {
                    //calls[callId] = call;
                    self.addtoCalls(callId, call);

                    call._isIncoming = true;
                    setStateChange(call);
                    setStreamAdded(call);
                }
            }

            //removes storage item with callId composed by getIncomingCallById
            storageService.removeItem(callId);

            var command = incomingCall.command;
            if (command === 'answer') {
                self.answerAudioCall(callId);
            }
            else if (command === 'answer_video') {
                self.answerVideoCall(callId);
            }
            else if (command === 'decline') {
                self.declineCall(callId);
            }
            else if (command === 'ignore') {
                self.ignoreCall(callId);
            }
        });
        rendererService.renderIncomingCalls();
    };

    function setStateChange(call) {
        if (call) {
            var callId = call.getId();

            call.onStateChange = function (state, statusCode) {
                logger.info("callStateChange \tstate:" + state + " \tstatusCode:" + statusCode, fcs.call.States);
                switch (state) {
                    case fcs.call.States.RINGING:
                        audioToneService.play(audioToneService.RING_OUT);
                        rendererService.setStatus("ringing");
                        break;
                    case fcs.call.States.ENDED:
                        rendererService.setStatus("ended");
                        if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                            audioToneService.play(audioToneService.BUSY);

                        setTimeout(function () {
                            if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                                audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isIncoming) {
                                callService.deleteIncomingCall(callId);
                                rendererService.renderIncomingCalls();
                            }
                            rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.REJECTED:
                        rendererService.setStatus("rejected");
                        audioToneService.play(audioToneService.BUSY);
                        setTimeout(function () {
                            audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isOutgoing)
                                rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.RENEGOTIATION:
                    case fcs.call.States.TRANSFERRED:
                    case fcs.call.States.IN_CALL:
                        if (call._isIncoming)
                            audioToneService.stop(audioToneService.RING_IN);
                        else if (call._isOutgoing)
                            audioToneService.stop(audioToneService.RING_OUT);
                        rendererService.setStatus("in call");
                        callService.setCallStartTime(callId);
                        var activeCall = callService.getActiveCall();
                        timerService.setCallTimer(activeCall.start_time);
                        break;
                    case fcs.call.States.ON_REMOTE_HOLD:
                        rendererService.setStatus("call held remotely");
                        break;
                    case fcs.call.States.OUTGOING:
                        rendererService.setStatus("There is an outgoing call");
                        break;
                }
                call._state = state;
                rendererService.renderCalls();
                //rendererService.renderIncomingCalls();
            };
        }
    }

    function setStreamAdded(call) {
        if (call) {
            call.onStreamAdded = function (streamURL) {
                var activeCall = callService.getActiveCall();
                if (activeCall && call.getId() === activeCall.id) {
                    logger.info('Stream adding for ' + call.callerNumber + '...');
                    // Setting up source (src tag) of remote video container
                    if (streamURL) {
                        $("#remoteVideo").attr("src", streamURL);
                    }
                    if (call.canReceiveVideo()) {
                        logger.info('Call can receive video');
                        $("#remoteVideo").show();
                    }
                    else {
                        logger.info('Call cannot receive video');
                        $("#remoteVideo").hide();
                    }

                    if (!call.canSendVideo()) {
                        $("#localVideo").hide();
                    }
                    logger.info('Stream added for ' + call.callerNumber + ':' + streamURL);
                }
            };

            call.onLocalStreamAdded = function (streamURL) {
                logger.info('Local stream adding for ' + call.callerNumber + '...');
                // Setting up source (src tag) of remote video container
                if (call.canSendVideo()) {
                    logger.info('Call can send video');
                    $("#localVideo").attr("src", streamURL).attr('muted', true).show();
                }
                else {
                    logger.info('Call cannot send video');
                    $("#localVideo").attr("src", "");
                }
                logger.info('Local stream added for ' + call.callerNumber + ':' + streamURL);
            };
        }
    }

    function answerCall(call, video, resolution) {
        logger.log('Answering call from ' + call.callerNumber + '...');
        var callId = call.getId(),
                incomingCalls = callService.getIncomingCalls();
        var callObj = {};
        callObj.id = callId;
        callObj.type = 'in';
        callObj.contact = call.callerName;
        callObj.number = call.callerNumber;
        callObj.status = 'active';
        callObj.mute = 0;
        if (incomingCalls && incomingCalls[callId]) {
            callObj.contact = incomingCalls[callId].contact;
            if (incomingCalls[callId].sf_contact_id) {
                callObj.sf_contact_id = incomingCalls[callId].sf_contact_id;
            }
            if (incomingCalls[callId].contactPhoto) {
                callObj.contactPhoto = incomingCalls[callId].contactPhoto;
            }
        }
        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            if (activeCall.id !== callId) {
                self.holdCall(activeCall.id);
            }
        }
        callService.addCall(callObj);
        call.answer(function () {
            logger.log('Call answered successfully');
            callService.deleteIncomingCall(callId);
            rendererService.renderCalls();
            rendererService.renderIncomingCalls();
        }, function (e) {
            callService.deleteIncomingCall(callId);
            callService.endCall(callId);
            rendererService.renderCalls();
            message.error('Call answer failed', 2000);
            logger.warn('Call answer failed: ' + e);
        }, video, resolution);
    }

    this.answerAudioCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, false);
        }
    };

    this.answerVideoCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, true, '320x240');
        }
    };

    this.endCall = function (callId) {
        var call = calls[callId];
        if (call) {
            audioToneService.stop(audioToneService.RING_OUT);
            call.end(function () {
                logger.log('Call ended!');
                if (callService.isActiveCall(callId)) {
                    rendererService.setStatus("call ended");
                }
                callService.endCall(callId);
                //delete calls[callId];
                self.deleteFromCalls(callId);
                setTimeout(function () {
                    rendererService.renderCalls();
                }, 1000);
            }, function (error) {
                message.error('Call could not be ended', 2000);
                logger.warn('Call could not be ended! ' + error);
            });
        }
    };

    this.holdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Holding call ' + call.callerNumber + '...');
            call.hold(function () {
                logger.log('Call held ' + call.callerNumber);
                callService.holdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be held ' + call.callerNumber, 2000);
                logger.warn('Call could not be held! ' + error);
            });
        }
    };

    this.unholdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Unholding call ' + call.callerNumber + '...');
            var activeCall = callService.getActiveCall();
            if (activeCall) {
                logger.log('Active calls getting held...');
                this.holdCall(activeCall.id);
            }
            call.unhold(function () {
                logger.log('Call unheld ' + call.callerNumber);
                callService.unholdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be retrieved' + call.callerNumber, 2000);
                logger.warn('Call could not be retrieved! ' + error);
            });
        }
    };

    this.muteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.mute();
            callService.muteCall(callId);
            rendererService.renderCalls();
        }
    };
    this.unmuteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.unmute();
            callService.unmuteCall(callId);
            rendererService.renderCalls();
        }
    };

    this.ignoreCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.ignore(function () {
                logger.log('Ignore call successful');
            }, function () {
                message.error('Call could not be ignored', 2000);
                logger.warn('Ignore call failed');
            });
        }
        audioToneService.stop(audioToneService.RING_IN);
        callService.ignoreIncomingCall(callId);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.declineCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.reject(function () {
                logger.log('Decline call successful');
            }, function () {
                message.error('Call could not be rejected', 2000);
                logger.warn('Decline call failed');
            });
        }
        callService.declineIncomingCall(callId);
        audioToneService.stop(audioToneService.RING_IN);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.sendDTMF = function (callId, tone) {
        var call = calls[callId];
        if (call) {
            dtmfService.sendDTMF(call, tone);
        }
    };

    // searches SF contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchDirectory = function (criteria, successCallback) {
        fcs.addressbook.searchDirectory(criteria, fcs.addressbook.SearchType.USERNAME, function (contacts) {
            if (successCallback)
                successCallback(contacts);
        }, function (error) {
            message.error('Search contact failed', 2000);
            logger.warn('Search contact failed :' + error);
        });
    };

    this.retrieveCalllog = function (onSuccess, onFailure) {
        SFService.loadCredentials(function (credentials) {
            self.setUserAuth(credentials);
            self.setup(credentials);
            fcs.calllog.retrieve(onSuccess, onFailure);
        });
    };
}();

/* global fcsService, rendererService, storageService, widget, callLog, logService, contactService, windowService */

var listenerService = new function () {
    this.popupListener = function (event) {
        switch (event.key) {
            case 'outgoing_call':
                fcsService.startCall();
                break;
            case 'incoming_call':
                if(fcsService.isConnected()){
                    fcsService.handleIncomingCall();
                }
                break;
            case 'calls':
                rendererService.renderCalls();
                break;
            case 'end_call':
                fcsService.endCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'hold_call':
                fcsService.holdCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'unhold_call':
                fcsService.unholdCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'mute_call':
                fcsService.muteCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'unmute_call':
                fcsService.unmuteCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            default:
                if (event.key.indexOf(windowService.getPrefix()) !== 0 && fcsService.isConnected()) {
                    rendererService.renderCalls();
                }
                break;
        }
    };

    this.widgetListener = function (event) {
        switch (event.key) {
            case 'call_status':
                rendererService.setStatus(event.newValue);
                break;
            case 'incoming_call':
                rendererService.renderIncomingCalls();
                break;
            case 'calls':
                rendererService.renderCalls();
                break;
            case 'popup_opened':
                if (!event.newValue) {
                    fcsService.unsubscribe(function () {
                        widget.subscribe(true);
                    });
                }
                break;
            default:
                // if any tab closed, call subscribe (it will check master and connectivity inside)
                if (event.key.indexOf(windowService.getPrefix()) === 0 && !event.newValue) {
                    widget.subscribe(true);
                }
                break;
        }
    };

    this.callLogListener = function (event) {
        switch (event.key) {
            case 'popup_opened':
                if (!event.newValue) {
                    logService.refreshCallLogGrid();
                }
                break;
        }
    };
}();

/* global fcsService, contactService, SFService, sforce, logger */

var logService = new function () {
    this.updateActivityHistory = function () {
        logger.info('Updating activity history...');
        getCallLogs(function (calllogs) {

            // Format local times in calllogs
            for (var i = 0; i < calllogs.length; i++){
                calllogs[i].sfTime = moment(calllogs[i].startTime).format('h:mm a').toString().toUpperCase();
            }

            SFService.loadSFData('logActivities', '<userID>' + window.UserContext.userId + '</userID><json>' + JSON.stringify(calllogs) + '</json>')
                    .done(function () {
                        logger.log('Activity history successfully updated');
                    })
                    .fail(function (e) {
                        logger.warn('Activity history update failed', e);
                    });
        });
    };
    //isSet is return format, if true, repetitions eliminated
    function getCallLogs(successCallback, isSet) {
        fcsService.retrieveCalllog(function (calllogs) {
            var calllog, distinctCalllogs = [];
            for (var i = 0; i < calllogs.length; i++) {
                calllog = calllogs[i];
                calllog.address = calllog.address.trim();
                if (distinctCalllogs.indexOf(calllog.address) < 0) {
                    distinctCalllogs.push(calllog.address);
                }
            }

            contactService.getSFContacts(JSON.stringify(distinctCalllogs),
                    function (contactsMap) {
                        var calllogsNew = [];
                        for (var i = 0; i < calllogs.length; i++) {
                            var calllog = calllogs[i], filteredContacts = contactsMap[calllog.address];
                            calllog.startTime = new Date(calllog.startTime);
                            calllog.mili = calllog.startTime.getTime();
                            switch (calllog.type) {
                                case 0:
                                    calllog.type_label = "Incoming";
                                    break;
                                case 1:
                                    calllog.type_label = "Missed";
                                    break;
                                case 2:
                                    calllog.type_label = "Outgoing";
                                    break;
                            }

                            if (isSet && filteredContacts.length > 0) {
                                var newCallLog = jQuery.extend({}, calllog);
                                newCallLog.name = filteredContacts[0].Name;
                                var altName = '';
                                for (var j = 1; j < filteredContacts.length; j++) {
                                    altName += filteredContacts[j].Name;
                                    if (j < filteredContacts.length - 1) {
                                        altName += ', ';
                                    }
                                }
                                if (altName !== '') {
                                    newCallLog.name += ' (' + altName + ')';
                                }

                                newCallLog.contactId = filteredContacts[0].Id;
                                newCallLog.id = calllog.id + ':' + calllog.mili + ':' + newCallLog.contactId;
                                calllogsNew.push(newCallLog);
                            }
                            else {
                                for (var j = 0; j < filteredContacts.length; j++) {
                                    var newCallLog = jQuery.extend({}, calllog);
                                    newCallLog.name = filteredContacts[j].Name;
                                    newCallLog.contactId = filteredContacts[j].Id;
                                    newCallLog.id = calllog.id + ':' + calllog.mili + ':' + newCallLog.contactId;
                                    calllogsNew.push(newCallLog);
                                }
                            }
                        }
                                                    
                        calllogsNew = calllogsNew.sort(compare);
                        successCallback(calllogsNew);
                    },
                    function () {

                    });

        });
    }

    function compare(a, b) {
        if (a.mili > b.mili)
            return -1;
        if (a.mili < b.mili)
            return 1;
        return 0;
    }

    this.refreshCallLogGrid = function () {
        logger.info('Refreshing call log grid...');
        getCallLogs(function (calllogs) {
            $('#spidr_call_logs').jqGrid('setGridParam', {data: calllogs}).trigger('reloadGrid');
            logger.log('Call log grid successfully refreshed');
        }, true);
    };
}();
/* global fcsService, listenerService, storageService, contactService, SFService, logService */

var callLog = new function () {
    var sfdcOnload = window.onload,
            self = this;

    this.onload = function () {
        if (sfdcOnload) {
            sfdcOnload();
        }
        $("body").css("margin", 0).css("padding", 0);

        $("#spidr_call_logs").jqGrid && $("#spidr_call_logs").jqGrid({
            datatype: "local",
            autowidth: true,
            height: 230,
            rowNum: 10,
            rowList: [10, 20, 30],
            colNames: ['Type', 'Name', 'Address', 'Start Time'],
            colModel: [
                {name: 'type_label', width: 40, index: 'type_label', align: "center"},
                {name: 'name', index: 'name'},
                {name: 'address', index: 'address'},
                {name: 'startTime', index: 'startTime', sorttype: "date", formatter: 'date',
                    formatoptions: {srcformat: 'D M d Y H:i:s', newformat: 'M d g:i A'}}
            ],
            pager: "#spidr_call_logs_pager",
            viewrecords: true,
            caption: "Call Logs",
            hidegrid: false
        });

        storageService.listen(listenerService.callLogListener);
        logService.refreshCallLogGrid();
    };
    if (!window._testonly) {
        window.i_am_call_logs = true;
        window.onload = self.onload;
    }
}();

})( );