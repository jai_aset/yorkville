global class GenbandController {
    
    private String username, password, server, webSocketServer, iceServerUrl, pluginMode;
    private Boolean webRtcDTLS;
    private static Integer soslQueries = 0;
    
    public static final Integer SOSL_QUERY_LIMIT = 20;
    public static final Integer SOQL_QUERY_LIMIT = 100;
    
    
    public GenbandController () {
        try {
            GenbandCredentials__c entry = [Select username__c, password__c, server__c, webSocketServer__c, iceServerUrl__c, webRtcDTLS__c From GenbandCredentials__c WHERE name=:getName()];
            username = entry.username__c;
            password = entry.password__c;
            server = entry.server__c;
            webSocketServer = entry.webSocketServer__c;
            iceServerUrl = entry.iceServerUrl__c;
            webRtcDTLS = entry.webRtcDTLS__c;
        } catch (Exception e) {  }
    }
    
    /* Getters and Setters */
    
    public static String getName() {
        return 'credentials_' + UserInfo.getUserId();
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String value) {
        this.username = value;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String value) {
        this.password = value;
    }
    
    public String getServer() {
        return this.server;
    }
    
    public void setServer(String value) {
        this.server = value;
    }
    
    public String getIceServerUrl() {
        return this.iceServerUrl;
    }
    
    public void setIceServerUrl(String value) {
        this.iceServerUrl = value;
    }
    
    public String getWebSocketServer() {
        return this.webSocketServer;
    }
    
    public void setWebSocketServer(String value) {
        this.WebSocketServer= value;
    }
    
    public String getPluginMode() {
        return this.pluginMode;
    }
    
    public void setPluginMode(String value) {
        this.pluginMode= value;
    }
    
    public Boolean getWebRtcDTLS() {
        return this.webRtcDTLS;
    }
    
    public void setWebRtcDTLS(Boolean value) {
        this.webRtcDTLS = value;
    }
    
    webService static String loadUsername() {
        String username = '';
        
        try {
            GenbandCredentials__c entry = [Select username__c From GenbandCredentials__c WHERE name=:getName()];
            username =  entry.username__c;  } catch (Exception e) {  }
        
        return username;
    }
    
    webService static String loadPassword() {
        String password = '';
        
        try {
            GenbandCredentials__c entry = [Select password__c From GenbandCredentials__c WHERE name=:getName()];
            password = entry.password__c;  } catch (Exception e) {  }
        
        return password;
    }
    
    webService static String loadServer() {
        String server = '';
        
        try {
            GenbandCredentials__c entry = [Select server__c From GenbandCredentials__c WHERE name=:getName()];
            server =  entry.server__c;  } catch (Exception e) {  }
        
        return server;
    }
    
    webService static String loadWebSocketServer() {
        String webSocketServer = '';
        
        try {
            GenbandCredentials__c entry = [Select webSocketServer__c From GenbandCredentials__c WHERE name=:getName()];
            webSocketServer =  entry.webSocketServer__c;  } catch (Exception e) {  }
        
        return webSocketServer ;
    }
    
    webService static String loadIceServerUrl() {
        String iceServerUrl = '';
        
        try {
            GenbandCredentials__c entry = [Select iceServerUrl__c From GenbandCredentials__c WHERE name=:getName()];
            iceServerUrl =  entry.iceServerUrl__c;  } catch (Exception e) {  }
        
        return iceServerUrl ;
    }
    
    webService static Boolean loadWebRtcDTLS() {
        Boolean webRtcDTLS = false;
        
        try {
            GenbandCredentials__c entry = [Select webRtcDTLS__c From GenbandCredentials__c WHERE name=:getName()];
            webRtcDTLS =  entry.webRtcDTLS__c;  } catch (Exception e) {  }
        
        return webRtcDTLS ;
    }
    
    webService static String loadGenbandCredentials() {
        // Consolidating creds load into one call to minimize API calls
        List<String> creds = new List<String>();
        System.debug('loadGenbandCredentials');
        try {
            GenbandCredentials__c entry = [Select username__c, password__c, server__c, webSocketServer__c, iceServerUrl__c, webRtcDTLS__c 
                                            From GenbandCredentials__c WHERE name=:getName()];
            creds.add(entry.username__c);  
            creds.Add(entry.password__c);  
            creds.Add(entry.server__c);  
            creds.Add(entry.webSocketServer__c);  
            creds.Add(entry.iceServerUrl__c); 
            if (entry.webRtcDTLS__c)
            {
                creds.Add('true');
                }
            else
            {
                creds.Add('false');
            }
        } catch (Exception e) {  
            System.debug('oops '+ e.getMessage());
            creds.add('Query error: ' + e.getMessage());
        }
        
        return JSON.serialize(creds);
    }
    
    webService static void setLoginState(boolean loginState) {
        GenbandLogin__c prev;
        boolean exists = true;
        
        try {
            prev = [SELECT loginState__c FROM GenbandLogin__c WHERE name=:getName()]; } catch (Exception e) { exists = false; }
        
        if (exists) {
            prev.loginState__c = loginState;
            update prev;
        } else {
            GenbandLogin__c entry = new GenbandLogin__c (Name=getName(), loginState__c = loginState);
            insert entry;
        }
    }
    
    webService static void setAgreement(boolean agreed) {
        GenbandAgreements__c prev;
        boolean exists = true;
        
        try {
            prev = [SELECT agreed__c FROM GenbandAgreements__c WHERE name=:getName()]; } catch (Exception e) { exists = false; }
        
        if (exists) {
            prev.agreed__c = agreed;
            update prev;
        } else {
            GenbandAgreements__c entry = new GenbandAgreements__c(Name=getName(), agreed__c = agreed);
            insert entry;
        }
    }
    
    webService static boolean loadAgreement() {
        boolean agreed = false;
        
        try {
            GenbandAgreements__c entry = [Select agreed__c From GenbandAgreements__c WHERE name=:getName()];
            agreed = entry.agreed__c; } catch (Exception e) { }
        
        return agreed;
    }
    
    webService static boolean checkLoginState() {
        boolean loginState = false;
        
        try {
            GenbandLogin__c entry = [Select loginState__c From GenbandLogin__c WHERE name=:getName()];
            loginState = entry.loginState__c; } catch (Exception e) { }
        
        return loginState;
    }
    
    public void save () {
        boolean exists = true;
        boolean hasError = false;
        
        if (getUsername().trim().length() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide username'));
        }
        
        if (getPassword().trim().length() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide password'));
        }
        
        if (getServer().trim().length() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide server address'));
        }
        
        if (getWebSocketServer().trim().length() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide websocket server address'));
        }
        
        if (getIceServerUrl().trim().length() == 0) {
            hasError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide Ice Server URL'));
        }
        
        
        if (hasError) {
            return;
        }
        
        try {
            GenbandCredentials__c prev = [Select username__c From GenbandCredentials__c 
                                          WHERE name=:getName()]; 
        } 
        catch (Exception e) { exists = false; }
        
        try {
            if (exists) {
                GenbandCredentials__c entry = [SELECT password__c, username__c, server__c, webSocketServer__c, iceServerUrl__c FROM GenbandCredentials__c WHERE name=:getName()];
                entry.username__c = getUsername();
                entry.password__c = getPassword();
                entry.server__c = getServer();
                entry.webSocketServer__c = getWebSocketServer();
                entry.iceServerUrl__c = getIceServerUrl();
                entry.webRtcDTLS__c = getWebRtcDTLS();
                update entry;
            } else {
                GenbandCredentials__c entry = new GenbandCredentials__c(Name=getName(), username__c= getUsername(), password__c=getPassword(), server__c=getServer(), webSocketServer__c=getWebSocketServer(), iceServerUrl__c=getIceServerUrl(), webRtcDTLS__c=getWebRtcDTLS());
                insert entry;
            }   } catch (Exception e) {  }
    }
    
    webService static void logActivities(String userID, String json) {
        List<SpidrLog> logs = (List<SpidrLog>)System.JSON.deserialize(json, List<SpidrLog>.class);
        Integer numberOfLogs = logs.size();
        Task task;
        
        String logIds = '';
        Integer logIndex = 0;
        for (SpidrLog log : logs) {
            logIndex++;
            logIds += '\'' + log.id + '\'';
            if(logIndex < numberOfLogs){
                logIds +=',';
            }
        }
        
        List<Task> alreayAddedTaskList = new List<Task>();
        
        if(logIds != ''){
            String logIdsQuery = 'SELECT SpidrID__c from Task WHERE SpidrID__c IN ('+logIds+')';
            
            System.debug('logIdsQuery: ' + logIdsQuery);
            
            alreayAddedTaskList = Database.query(logIdsQuery);
        }
        
        Map<String,Task> alreayAddedTaskMap = new Map<String,Task>();
        for (Task t : alreayAddedTaskList) {
            alreayAddedTaskMap.put(t.SpidrID__c, t);
        }
        
        System.debug('alreayAddedTaskMap: ' + alreayAddedTaskMap);
        
        for (SpidrLog log : logs) {
            
            try {
                Integer index = log.address.indexOf('@');
                if (log.contactId !=null && log.contactId !='0' && !alreayAddedTaskMap.containsKey(log.id)) {
                    task = new Task();
                    datetime logTime = datetime.newInstance(log.mili);
                    task.OwnerId = userID;
                    task.Type = 'Call';
                    task.Status = 'Completed';
                    task.Subject = 'Call - ' + log.type_label + ' ' + log.sfTime;
                    task.ActivityDate = log.getDateValue();
                    task.SpidrID__c = log.id;
                    task.WhoId = log.contactId;
                    
                    System.debug('newly added: ' + log.id);
                    
                    upsert task;
                }
                else {
                    System.debug('already added: ' + log.id);
                }
            } catch (Exception e) {
                System.debug('Log activity failed: ' + e);
            }
        }
    }
    
    webService static List<Contact> checkContactExistence(String firstName, String lastName) {
        List<Contact> contacts;
        String queryMsg;
        if(firstName != '') {
            queryMsg = 'SELECT Id, Name From Contact WHERE (FirstName LIKE ' + '\'' + firstName + '\' AND LastName LIKE ' + '\'' + lastName + '\') OR (LastName LIKE ' + '\'' + firstName + '\' AND FirstName LIKE ' + '\'' + lastName + '\') OR (LastName LIKE ' + '\'' + lastName + '\')';
        } else {
            queryMsg = 'SELECT Id, Name From Contact WHERE LastName LIKE ' + '\'' + lastName + '\'';
        }
        contacts = Database.query(queryMsg);
        if (contacts.size() > 0) {
            return contacts;
        } else {
            return null;
        }
    }
    
    //webService static String describeSFObj(String object) {
    //}
    
    //webService static String getPhoneFields(String object) {
    //}
    
    //webService static String getObjsWithPhone(String object) {
    private static List<String> getAllSObjsWithPhoneFields()
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        List<Schema.SObjectType> gd2 = Schema.getGlobalDescribe().Values();     
        
        List<String> objList = new List<String>();
        System.debug('#####');
        System.debug('#####Size of the GenbandController map: ' + gd.size());
        System.debug('#####');
        for(String sObj : Schema.getGlobalDescribe().keySet())
        {
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
            String objectName = r.getName();
            //System.debug('sObject name: ' + objectName);
            if (objectName.equalsIgnoreCase('Contact'))
            {
                System.debug('********Contact found: ' + objectName);
            }
            if (objectName.equalsIgnoreCase('User'))
            {
                System.debug('********User found: ' + objectName);
            }
            
            objList.add(objectName);
            Map<String, Schema.SObjectField> fieldMap = gd.get(sObj).getDescribe().fields.getMap();
            //           Map<String, Schema.FieldSet> fieldMap = r.fieldSets.getMap();
            //            Field[] fields = desObj.getFields();
            //        for(int j=0;j < fields.length; j++)
            for(Schema.SObjectField sfield : fieldMap.Values())
            {                        
                // /           Field field = fields[j];
                schema.describefieldresult dfield = sfield.getDescribe();
                //System.debug('Field name: ' + dfield.getName() + '\t\tType: ' + dfield.getType());
                //String fType = dfield.getType();
                if(dfield.getType() == DisplayType.Phone)
                {
                    //                    System.debug('Object ' + objectName + ' has a phone field named ' + dfield.getName());
                }      
            }
        }
        
        System.debug('#####Length of the list is: ' + objList.size());
        System.debug('#####Length of the map is: ' + gd.size());
        return objList;
        
        
    }
    
    webService static String getContacts(String logAddresses) {
        Map<String, List<ContactUser>> matchedContacts = new Map<String, List<ContactUser>>();
        List<String> loggedAddresses = new List<String>();
        List<String> phoneObjs = new List<String>();
        phoneObjs = getAllSObjsWithPhoneFields();
        List<ContactUser> finalizedContacts = new List<ContactUser>();
        
        System.debug('Log addresses: ' + logAddresses);
        try {
            loggedAddresses = (List<String>)System.JSON.deserialize(logAddresses, List<String>.class);
        }catch(Exception e){
            System.debug(logAddresses + ' is not a valid json. So single-item list getting formed');
            loggedAddresses.add(logAddresses);
        }
        //loggedAddresses = (List<String>)System.JSON.deserialize(logAddresses, List<String>.class);
        
        Integer loggedAddressCount = loggedAddresses.size();
        System.debug('Starting to match ' + loggedAddressCount + ' addresses.');
        System.debug('At start, finalizedContacts is ' + finalizedContacts.size() + ' entries.');
        System.debug('At start, matchedContacts map is  ' + matchedContacts.size() + ' entries.');
        
        for(Integer i = 0; i < loggedAddressCount; i++){
            String address = loggedAddresses[i];
            List<Contact> queriedContacts = new List<Contact>();
            
            //			for (Id key : matchedContacts.keySet()) {
            //            }
            
            if(isNumeric(address)){
                
                String numericAddress = getNumber(address);
                System.debug('Given number is numeric,create query for filtering');
                // let's try this the easy way first
                if (soslQueries < SOSL_QUERY_LIMIT)
                {
                    System.debug('Try SOSL find query with ' + numericAddress);
                    List<List<SObject>> searchList = new List<List<SObject>>();
                    searchList = search.query('find \'' + numericAddress  
                                              + '\' in phone fields returning ' 
                                              + 'Account(Name, Id, Phone, Fax)'
                                              + ', Contact(Name, Id, Phone, Fax, HomePhone, MobilePhone)'
                                              + ', Opportunity(Name, Id)'
                                              + ', Lead(Name, Id, Phone, MobilePhone, Fax)');
                    soslQueries++;
                    System.debug('Executed ' + soslQueries + ' SOSL queries.');
                    Integer foundTotal = searchList[0].size() + searchList[1].size() + searchList[2].size() + searchList[3].size();
                    if (foundTotal > 0)
                    {
                        List<ContactUser> result = parseSOSLResult(searchList);
                        matchedContacts.put(address, result);
                        System.debug('Putting matched contacts for entry ' + address + ' of length ' + result.size());
                        continue;
                    }
                    System.debug('No SOSL exact match found for ' + numericAddress);
                }
                // no match found, could maybe be email
                
                
                
                String addressForQuery = numericAddress;
                
                if(numericAddress.length() >= 7){
                    //String addressForQuery = getAddressForQuery(numericAddress);
                    //*******************************************************//
                    finalizedContacts.clear();
                    System.debug('Length of given address [' + address + '] is greater than 7. Looking for exact matching.');
                    //length of 10 is special to SF
                    //10 digits are saved with formatting
                    if (numericAddress.length() == 10)
                    {
                        addressForQuery = '(' + numericAddress.substring(0,3) + ') ' + numericAddress.substring(3,6) + '-' + numericAddress.substring(6,10);
                        System.debug('Ten-digit number encountered, formatting applied: ' + addressForQuery);
                    }
                    
                    String condition = 'Email LIKE \'' + addressForQuery + '\' OR '
                        + 'Phone = \'' + addressForQuery + '\' OR '
                        + 'HomePhone = \'' + addressForQuery + '\' OR '
                        + 'MobilePhone = \'' + addressForQuery + '\' OR '
                        + 'OtherPhone = \'' + addressForQuery + '\' OR '
                        + 'Fax = \'' + addressForQuery + '\' OR '
                        + 'AssistantPhone = \'' + addressForQuery + '\'';
                    
                    System.debug(address + ' address getting searched with condition: ' + condition);
                    
                    queriedContacts = Database.query('SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, Fax, AssistantPhone FROM Contact WHERE ' + condition);
                    String matchingAddress = getAddressForMatching(address);
                    System.debug('Address for matching: ' + matchingAddress);
                    List<Contact> contactsToBeCompared = new List<Contact>();
                    
                    Integer queriedContactsCount = queriedContacts.size();
                    System.debug('[Contacts Count: ' + queriedContactsCount + ']');
                    
                    if(queriedContactsCount > 1){
                        for(Integer j = 0; j < queriedContactsCount; j++){
                            if(queriedContacts[j].Phone != null && getNumber(queriedContacts[j].Phone).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                            else if(queriedContacts[j].HomePhone != null && getNumber(queriedContacts[j].HomePhone).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                            else if(queriedContacts[j].MobilePhone != null && getNumber(queriedContacts[j].MobilePhone).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                            else if(queriedContacts[j].OtherPhone != null && getNumber(queriedContacts[j].OtherPhone).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                            else if(queriedContacts[j].Fax != null && getNumber(queriedContacts[j].Fax).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                            else if(queriedContacts[j].AssistantPhone != null && getNumber(queriedContacts[j].AssistantPhone).endsWith(matchingAddress)){
                                contactsToBeCompared.add(queriedContacts[j]);
                            }
                        }
                        
                        
                        if(contactsToBeCompared.size() > 0){
                            finalizedContacts = findBestMatch(contactsToBeCompared);
                            System.debug('[Result for record (' + address + ') => ' + contactsToBeCompared[0].Name + ']');
                        }
                        
                        
                    } //qcc > 0
                    matchedContacts.put(address, finalizedContacts);
                    System.debug('Putting matched contacts for entry ' + address + ' of length ' + finalizedContacts.size());
                } //numeric length > 7
                else {  //numeric < 7
                    System.debug('Length of given address [' + address + '] is less than 7. Looking for exact matching.');
                    String condition = 'Phone =\'' + address + '\' OR '
                        + 'HomePhone =\'' + address + '\' OR '
                        + 'MobilePhone =\'' + address + '\' OR '
                        + 'OtherPhone =\'' + address + '\' OR '
                        + 'Fax =\'' + address + '\' OR '
                        + 'AssistantPhone =\'' + address + '\'';
                    
                    System.debug(address + ' address getting searched with condition: ' + condition);
                    
                    queriedContacts = Database.query('SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, Fax, AssistantPhone FROM Contact WHERE ' + condition);
                    
                    finalizedContacts = buildList(queriedContacts);
                    
                    matchedContacts.put(address, finalizedContacts);
                } //numeric < 7
                
            }  // is numeric
            else {
                System.debug('Non-numeric, querying email');
                // let's try this the easy way first
                if (soslQueries < SOSL_QUERY_LIMIT){
                    System.debug('Try SOSL find query with ' + address);
                    List<List<SObject>> searchList = new List<List<SObject>>();
                    searchList = search.query('find \'' + address  
                                              + '\' in email fields returning ' 
                                              + 'Account(Name, Id, Phone, Fax)'
                                              + ', Contact(Name, Id, Phone, Fax, HomePhone, MobilePhone)'
                                              + ', Opportunity(Name, Id)'
                                              + ', Lead(Name, Id, Phone, MobilePhone, Fax)');
                    soslQueries++;
                    System.debug('Executed ' + soslQueries + ' SOSL queries.');
                    Integer foundTotal = searchList[0].size() + searchList[1].size() + searchList[2].size() + searchList[3].size();
                    if (foundTotal > 0)
                    {
                        List<ContactUser> result = parseSOSLResult(searchList);
                        matchedContacts.put(address, result);
                        System.debug('Putting matched contacts for entry ' + address + ' of length ' + result.size());
                        continue;
                    }
                }
                
                System.debug('Given address [' + address + '] is not numeric.');
                String condition = 'Phone =\'' + address + '\' OR '
                    + 'HomePhone =\'' + address + '\' OR '
                    + 'MobilePhone =\'' + address + '\' OR '
                    + 'OtherPhone =\'' + address + '\' OR '
                    + 'Fax =\'' + address + '\' OR '
                    + 'AssistantPhone =\'' + address + '\' OR '
                    + 'Email =\'' + address + '\'';
                
                queriedContacts = Database.query('SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, Fax, AssistantPhone FROM Contact WHERE ' + condition);
                System.debug('queriedContacts is ' + queriedContacts.size());
                
                finalizedContacts = buildList(queriedContacts);
                
                matchedContacts.put(address, finalizedContacts);
                System.debug('Putting matched contacts for entry ' + address + ' of length ' + finalizedContacts.size());
            }
        }
        return JSON.serialize(matchedContacts);
    }
    
    @TestVisible
    private static String getNumber(String value) {
        if (value.indexOf('@') > -1) {
            Integer index = value.indexOf('@');
            String address = value.substring(0, index);
            
            return address;
        }
        
        return value;
    }
    
    private static Boolean isNumeric (String value) {
        Integer index = value.indexOf('@');
        String address = value.substring(0, (index > -1) ? index : value.length() );
        
        Pattern isnumbers = Pattern.Compile('^[0-9]+$');
        Matcher match = isnumbers.matcher(address);
        
        return match.Matches();
    }
    
    private static String getNumeric(String value) {
        return getNumber(value).replaceAll('[^a-zA-Z0-9]', '');
    }
    
    
    @TestVisible
    private static List<Contact> checkBestMatch(String logAddress, List<Contact> contacts){
        Integer len = contacts.size();
        
        List<Contact> newContacts = new List<Contact>();
        
        Contact c;
        String contactAddress;
        for(Integer i = 0; i < len; i++){
            c = contacts[i];
            
            if(equals(logAddress, c.Email) ||
               equals(logAddress, c.Phone) ||
               equals(logAddress, c.HomePhone) ||
               equals(logAddress, c.MobilePhone) ||
               equals(logAddress, c.OtherPhone) ||
               equals(logAddress, c.Fax) ||
               equals(logAddress, c.AssistantPhone)){
                   newContacts.add(c);
               }
        }
        
        return newContacts;
    }
    
    @TestVisible
    private static Boolean equals (String spidrLogAddress, String contactAddress) {
        System.debug('equality check forr '+ spidrLogAddress+ ' and ' + contactAddress);
        Integer length;
        
        if (spidrLogAddress == null || contactAddress == null) {
            System.debug('one is null so they are not equal');
            return false;
        }
        
        contactAddress = getNumeric(contactAddress);
        System.debug('contact address numeric value: ' + contactAddress);
        
        if (contactAddress.length() == 0) {
            System.debug('contact address is empty so they are not equal');
            return false;
        }
        
        if (spidrLogAddress.length() > contactAddress.length()) {
            System.debug('logaddress longer than contact address');
            spidrLogAddress = spidrLogAddress.substring(spidrLogAddress.length() - contactAddress.length(), spidrLogAddress.length());
        } else {
            System.debug('contact address longer than or equal logaddress');
            contactAddress  = contactAddress.substring(contactAddress.length() - spidrLogAddress.length(), contactAddress.length());
        }
        
        System.debug('last equality check... ' + contactAddress + ' & ' +spidrLogAddress);
        return contactAddress.equals(spidrLogAddress);
    }
    
    //    private static String getAddressForQuery(String address){
    //        String trimmed = '';
    //        String[] splitted= address.split('');
    //        Integer len = splitted.size();
    
    //        for (Integer i = 0; i < len; i++) {
    //           if( splitted[i] != ''){
    //                trimmed = trimmed + '%' + splitted[i] ;
    //            }
    //        }
    //        if(trimmed != ''){
    //            trimmed = trimmed + '%';
    //        }
    //        return trimmed;
    //    }
    
    private static String getAddressForMatching(String address){
        address = address.trim();
        address = address.replace(' ', '');
        address = address.replace('+', '');
        
        if(address.startsWith('0')){
            address = address.substring(1,address.length());
        }
        
        return getNumber(address);
    }
    
    @TestVisible private static List<ContactUser> findBestMatch(List<Contact> contacts){
        
        List<ContactUser> results = new List<ContactUser>();
        
        if(contacts.size() > 0){
            Contact tmpContact = contacts[0];
            Integer contactsLength = contacts.size();
            
            for(Integer i = 0; i < contactsLength; i++){
                if(contacts[i].Phone != null && (contacts[i].Phone.length() > tmpContact.Phone.length())){
                    tmpContact = contacts[i];
                }
                else if(contacts[i].HomePhone != null && (contacts[i].HomePhone.length() > tmpContact.HomePhone.length())){
                    tmpContact = contacts[i];
                }
                else if(contacts[i].MobilePhone != null && (contacts[i].MobilePhone.length() > tmpContact.MobilePhone.length())){
                    tmpContact = contacts[i];
                }
                else if(contacts[i].OtherPhone != null && (contacts[i].OtherPhone.length() > tmpContact.OtherPhone.length())){
                    tmpContact = contacts[i];
                }
                else if(contacts[i].Fax != null && (contacts[i].Fax.length() > tmpContact.Fax.length())){
                    tmpContact = contacts[i];
                }
                else if(contacts[i].AssistantPhone != null && (contacts[i].AssistantPhone.length() > tmpContact.AssistantPhone.length())){
                    tmpContact = contacts[i];
                }
            }
            
            // Create result contact
            ContactUser user = new ContactUser();
            user.Id = tmpContact.Id;
            user.Name = tmpContact.Name;
            user.Phone = tmpContact.Phone;
            user.HomePhone = tmpContact.HomePhone;
            user.MobilePhone = tmpContact.MobilePhone;
            user.Fax = tmpContact.Fax;
            
            results.add(user);
        }
        else {
            ContactUser user = new ContactUser();
            user.Id = '0';
            user.Name = 'Unknown User';
            user.Phone = 'Unknown';
            user.HomePhone = 'Unknown';
            user.MobilePhone = 'Unknown';
            user.Fax = 'Unknown';
            
            results.add(user);
        }
        
        return results;
    }
    
    @TestVisible private static List<ContactUser> parseSOSLResult(List<List<SObject>> SOSLResult)
    {
        List<ContactUser> resList = new List<ContactUser>();
        
        Account [] accounts = ((List<Account>)SOSLResult[0]);
        Contact [] contacts = ((List<Contact>)SOSLResult[1]);
        Opportunity [] opportunities = ((List<Opportunity>)SOSLResult[2]);
        Lead [] leads = ((List<Lead>)SOSLResult[3]);
        System.debug('Size of accounts is ' + accounts.size());
        System.debug('Size of contacts is ' + contacts.size());
        System.debug('Size of opportunities is ' + opportunities.size());
        System.debug('Size of leads is ' + leads.size());
        //we've found one or more exact match to the phone number
        //execute these in reverse order of priority
        ContactUser foundUser = new ContactUser();
        if (opportunities.size() > 0)
        {  
            foundUser.Id = opportunities[0].Id;
            foundUser.Name = opportunities[0].Name;
        }
        if (leads.size() > 0)
        {  
            foundUser = new ContactUser();
            foundUser.Id = leads[0].Id;
            foundUser.Name = leads[0].Name;
            foundUser.Phone = leads[0].Phone;
            foundUser.MobilePhone = leads[0].MobilePhone;
            foundUser.Fax = leads[0].Fax;
        }
        if (contacts.size() > 0)
        {  
            foundUser = new ContactUser();
            foundUser.Id = contacts[0].Id;
            foundUser.Name = contacts[0].Name;
            foundUser.Phone = contacts[0].Phone;
            foundUser.HomePhone = contacts[0].HomePhone;
            foundUser.MobilePhone = contacts[0].MobilePhone;
            foundUser.Fax = contacts[0].Fax;
        }
        if (accounts.size() > 0)
        {  
            foundUser = new ContactUser();
            foundUser.Id = accounts[0].Id;
            foundUser.Name = accounts[0].Name;
            foundUser.Phone = accounts[0].Phone;
            foundUser.Fax = accounts[0].Fax;
        }
        resList.add(foundUser);
        
        return resList;
    }
    
    private static List<ContactUser> buildList(List<Contact> contacts){
        List<ContactUser> resultList = new List<ContactUser>();
        Integer listSize = contacts.size();
        
        if(listSize > 0){
            for(Integer i = 0; i < listSize; i++){
                ContactUser user = new ContactUser();
                user.Id = contacts[i].Id;
                user.Name = contacts[i].Name;
                user.Phone = contacts[i].Phone;
                user.HomePhone = contacts[i].HomePhone;
                user.MobilePhone = contacts[i].MobilePhone;
                user.Fax = contacts[i].Fax;
                
                resultList.add(user);
            }
        }
        else {
            ContactUser user = new ContactUser();
            user.Id = '0';
            user.Name = 'Unknown User';
            user.Phone = 'Unknown';
            user.HomePhone = 'Unknown';
            user.MobilePhone = 'Unknown';
            user.Fax = 'Unknown';
            
            resultList.add(user);
        }
        
        return resultList;
    }
    
    private class SpidrLog {
        public String id;
        public String address;
        public String name;
        public long mili;
        public String type_label;
        public String contactId;
        public String sfTime;

        public Date getDateValue() {
            Date ret = null;
            datetime value = null;
            
            value = datetime.newInstance(this.mili);
            
            ret = date.newinstance(value.year(), value.month(), value.day());
            return ret;
        }
    }
    
    @TestVisible private class ContactUser {
        public String Id;
        public String Name;
        public String Phone;
        public String HomePhone;
        public String MobilePhone;
        public String Fax;
    }
}