public class CurrentContactController {
    private String eID='';
    private String eType='';
	private String cId='';
    public Map<String, String> keyPrefixMap = new Map<String, String>();
	
	public CurrentContactController() {
        initSalesforceIdPrefix(keyPrefixMap);
        
        try{
            Contact contact = [SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone FROM Contact 
                               WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
            this.cId = contact.Id;
        } catch(Exception e) {
            this.cId= '';
        }
        
        if (ApexPages.currentPage().getParameters().get('id') != null)
        {
        eID = ApexPages.currentPage().getParameters().get('id');
        eType = keyPrefixMap.get(eID.substring(0, 3));
     }  
              
    }
    
    	private static void initSalesforceIdPrefix(Map<String, String> keyPrefixMap)
        {
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        System.debug('********Size of the CurrentContactController map: ' + gd.size());
		for(String sObj : Schema.getGlobalDescribe().keySet())
                {
		   Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
		   String tempName = r.getName();
		   String tempPrefix = r.getKeyPrefix();
		   System.debug('Processing Object['+tempName + '] with Prefix ['+ tempPrefix+']');
		   keyPrefixMap.put(tempPrefix,tempName);
		}
	}

	public String entityId {
		get {
			return this.eId;
		}
		private set;
	}
    
	public String entityType {
		get {
			return this.eType;
		}
		private set;
	}
    
	public String contactId {
		get {
			return this.cId;
		}
		private set;
	}
    
}