/* global videoInvitationService, expect, dummy, spyOn, sforce, rendererService, message */

describe('https-server.test.videoInvitationServiceTest', function () {
    beforeEach(function () {
        resetDummy();
        $('body').append('<div id="send_invitation_dialog"></div>');
        $('#send_invitation_dialog').append('<input type="text" id="invitation_email"/>');
        $('#send_invitation_dialog').append('<input type="text" id="valid_from"/>');
        $('#send_invitation_dialog').append('<input type="text" id="valid_to"/>');
        videoInvitationService.openDialog(dummy.email);
    });

    afterEach(function () {
        $('#send_invitation_dialog').remove();
    });

    it('check openDialog', function () {
        expect($('#invitation_email').val()).toEqual(dummy.email);
    });

    it('check cancel', function () {
        var buttons = $('#send_invitation_dialog').dialog('option', 'buttons');
        buttons['Cancel']();
        expect($('#send_invitation_dialog').dialog("isOpen")).toBeFalsy();
    });

    describe('check send', function () {
        beforeEach(function () {
            spyOn(sforce, 'SingleEmailMessage');
            spyOn(rendererService, 'templateEngine');
            spyOn(message, 'error');
            spyOn(message, 'warning');
            spyOn(message, 'info');
            spyOn(console, 'log');
            $('#send_invitation_dialog').find('#invitation_email').val('');
        });
        it('without email address', function () {
            var buttons = $('#send_invitation_dialog').dialog('option', 'buttons');
            buttons['Send']();
            expect(message.warning).toHaveBeenCalledWith('Please enter an email address', 2000);
        });
        it('success', function () {
            spyOn(sforce.connection, 'sendEmail').and.returnValue([{success: true}]);
            $('#send_invitation_dialog').find('#invitation_email').val(dummy.email);
            $('#send_invitation_dialog').find('#valid_from').val('01/27/2016 11:45');
            $('#send_invitation_dialog').find('#valid_to').val('01/27/2016 11:46');

            var buttons = $('#send_invitation_dialog').dialog('option', 'buttons');
            buttons['Send']();
            expect(sforce.SingleEmailMessage).toHaveBeenCalled();
            expect(message.info).toHaveBeenCalledWith('Invitation email sent successfully', 2000);
        });

        it('sendEmail failed', function () {
            spyOn(sforce.connection, 'sendEmail').and.returnValue([{success: false}]);
            $('#send_invitation_dialog').find('#invitation_email').val(dummy.email);

            var buttons = $('#send_invitation_dialog').dialog('option', 'buttons');
            buttons['Send']();

            expect(message.error).toHaveBeenCalledWith('Error in sending invitation email', 2000);
        });
    });

});
