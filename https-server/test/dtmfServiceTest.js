/* global dtmfService, expect, dummy, spyOn */

describe('https-server.test.dtmfServiceTest', function () {
    var tone,
            oscillatorObj,
            gainObj,
            audioContext;
    beforeEach(function () {
        resetDummy();
        window.AudioContext = dummy.functionVoid;
        tone = '1';
        oscillatorObj = {
            frequency: {
                value: ''
            },
            connect: dummy.functionVoid,
            disconnect: dummy.functionVoid,
            start: dummy.functionVoid
        };
        gainObj = {
            gain: {
                value: ''
            },
            connect: dummy.functionVoid
        };
        audioContext = {
            createOscillator: function () {
                return oscillatorObj;
            },
            createGain: function () {
                return gainObj;
            },
            destination: 'destination'
        };

        spyOn(dummy.call, 'sendDTMF');
        spyOn(window, 'AudioContext').and.returnValue(audioContext);
        spyOn(oscillatorObj, 'connect');
        spyOn(oscillatorObj, 'disconnect');
        spyOn(oscillatorObj, 'start');
        spyOn(gainObj, 'connect');
        jasmine.clock().install();
    });
    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it('check sendDTMF', function () {
        dtmfService.sendDTMF(dummy.call, tone);
        expect(dummy.call.sendDTMF).toHaveBeenCalledWith(tone);
        expect(window.AudioContext).toHaveBeenCalled();
        expect(oscillatorObj.connect).toHaveBeenCalledWith(gainObj, 0, 0);
        expect(gainObj.connect).toHaveBeenCalledWith(audioContext.destination);
        expect(oscillatorObj.start).toHaveBeenCalledWith(0);

        jasmine.clock().tick(301);
        expect(oscillatorObj.disconnect).toHaveBeenCalled();
    });
});
