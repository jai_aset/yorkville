/* global SFService, expect, spyOn, sforce, logger */

describe('https-server.test.SFServiceTest', function () {

    beforeEach(function () {
        spyOn(logger, 'log');
        spyOn(logger, 'warn');
    });
    afterEach(function () {
        localStorage.clear();
    });

    describe('check initSFApi', function () {
        it('with window.__sfdcSessionId', function () {
            window.__sfdcSessionId = 1;
            SFService.initSFApi();
            expect(sforce.sessionId).toEqual(window.__sfdcSessionId);
            expect(logger.log).toHaveBeenCalledWith('sforce api initialized successfully');
        });
        it('without window.__sfdcSessionId', function () {
            delete window.__sfdcSessionId;
            spyOn(window, "getCookie").and.returnValue(null);
            SFService.initSFApi();
            expect(window.getCookie).toHaveBeenCalledWith('sid');
            expect(logger.warn).toHaveBeenCalledWith('sforce api initialization error');
        });
    });

    it('check loadSFData', function () {
        spyOn($, "ajax");
        SFService.loadSFData('method');
        expect($.ajax).toHaveBeenCalled();
    });

    it('check parseSFResult', function () {
        expect(SFService.parseSFResult('<data><result>a</result></data>')).toEqual('a');
    });

    it('check loadCredentials ', function () {
        var credentials = {username: 'u', password: 'p', server: 'https://server:10', wsServer: 'http://server:10', iceServerUrl: 'test', pluginMode: 'auto', webRtcDTLS: true},
        dummyResult,
                dummyFunction = function (a) {
                    dummyResult = a;
                },
                deferred = {
                    'loadUsername': $.Deferred(),
                    'loadPassword': $.Deferred(),
                    'loadServer': $.Deferred(),
                    'loadWebSocketServer': $.Deferred(),
                    'loadIceServerUrl': $.Deferred(),
                    'loadPluginMode': $.Deferred(),
                    'loadWebRtcDTLS': $.Deferred()
                };

        spyOn(SFService, 'loadSFData').and.callFake(function (a) {
            return deferred[a].promise();
        });
        spyOn(SFService, 'parseSFResult').and.callFake(function (a) {
            return a;
        });

        SFService.loadCredentials(dummyFunction);
        deferred.loadUsername.resolve(credentials.username);
        deferred.loadPassword.resolve(credentials.password);
        deferred.loadServer.resolve(credentials.server);
        deferred.loadWebSocketServer.resolve(credentials.wsServer);
        deferred.loadIceServerUrl.resolve(credentials.iceServerUrl);
        deferred.loadPluginMode.resolve(credentials.pluginMode);
        deferred.loadWebRtcDTLS.resolve(credentials.webRtcDTLS);
        expect(dummyResult.username).toEqual(credentials.username);
    });
});
