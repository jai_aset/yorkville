/* global callLog, expect, storageService, logService, listenerService */

describe('https-server.test.call_logsTest', function () {
    beforeEach(function () {
        resetDummy();
    });
    afterEach(function () {
        localStorage.clear();
        window._testonly = true;
    });

    it('check onload', function () {
        $('body').append('<div id="spidr_call_logs"></div>');
        spyOn($.fn,'jqGrid');
        spyOn(storageService,'listen');
        spyOn(logService,'refreshCallLogGrid');
        spyOn(listenerService,'callLogListener');
        
        callLog.onload();
        
        expect($("#spidr_call_logs").jqGrid).toHaveBeenCalled();
        expect(storageService.listen).toHaveBeenCalled();
        expect(logService.refreshCallLogGrid).toHaveBeenCalled();
        
        $('#spidr_call_logs').remove();
    });
});
