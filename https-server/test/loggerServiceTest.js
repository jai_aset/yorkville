
/* global spyOn, expect, logger, dummy, fcs */

describe('https-server.test.loggerServiceTest', function () {

    var logHandler;
    beforeEach(function () {
        resetDummy();
        spyOn(console, 'warn');
        spyOn(console, 'info');
        spyOn(console, 'debug');
        spyOn(console, 'error');
        logHandler = fcs.logManager.getLogger();
    });

    it('check log', function () {
        logger.log(dummy.text);
        expect(console.info).toHaveBeenCalled();
    });

    it('check info', function () {
        logger.info(dummy.text);
        expect(console.info).toHaveBeenCalled();
    });

    it('check debug', function () {
        logger.debug(dummy.text);
        expect(console.debug).toHaveBeenCalled();
    });

    it('check warn', function () {
        logger.warn(dummy.text);
        expect(console.warn).toHaveBeenCalled();
    });

    it('check error', function () {
        logger.error({message: dummy.text});
        expect(console.error).toHaveBeenCalled();
    });

    describe('check jslLogHandler', function () {
        it('check info', function () {
            logHandler.info(dummy.text);
            expect(console.info).toHaveBeenCalled();
        });

        it('check debug', function () {
            logHandler.debug(dummy.text);
            expect(console.debug).toHaveBeenCalled();
        });

        it('check warn', function () {
            logHandler.warn(dummy.text);
            expect(console.warn).toHaveBeenCalled();
        });

        it('check error', function () {
            logHandler.error({message: dummy.text});
            expect(console.error).toHaveBeenCalled();
        });
    });

});
