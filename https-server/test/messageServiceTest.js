/* global message, expect, dummy, spyOn */

describe('https-server.test.messageServiceTest', function () {

    var text = 'text',
            timeout = 100,
            transparent = false,
            pushdown = true;

    beforeEach(function () {
        resetDummy();
        $('body').append('<div id="alert"></div>');
    });

    afterEach(function () {
        $('#alert').remove();
    });


    it('check alert', function () {
        expect(message.alert()).toEqual($('#alert'));
    });

    describe('check', function () {
        beforeEach(function () {
            spyOn(message, 'alert').and.returnValue($('#alert'));
            spyOn($.fn, 'fadeOut').and.callFake(function (callback) {
                callback();
            });
            jasmine.clock().install();
        });

        afterEach(function () {
            jasmine.clock().uninstall();
        });

        it('error', function () {
            message.error(text, timeout, transparent, pushdown);

            expect(message.alert).toHaveBeenCalled();
            expect($('#alert').hasClass('error')).toBeTruthy();
            expect($('#alert').text()).toEqual(text);
            expect($('#alert').hasClass('transparent')).toBeFalsy();
            expect($('#alert').hasClass('pushdown')).toBeTruthy();

            jasmine.clock().tick(timeout + 1);
            expect($('#alert').attr('class')).toEqual('');
        });

        it('warning', function () {
            message.warning(text, timeout, transparent, pushdown);

            expect(message.alert).toHaveBeenCalled();
            expect($('#alert').hasClass('warning')).toBeTruthy();
            expect($('#alert').text()).toEqual(text);
            expect($('#alert').hasClass('transparent')).toBeFalsy();
            expect($('#alert').hasClass('pushdown')).toBeTruthy();
        });

        it('info', function () {
            message.info(text, timeout, transparent, pushdown);

            expect(message.alert).toHaveBeenCalled();
            expect($('#alert').hasClass('info')).toBeTruthy();
            expect($('#alert').text()).toEqual(text);
            expect($('#alert').hasClass('transparent')).toBeFalsy();
            expect($('#alert').hasClass('pushdown')).toBeTruthy();
        });
    });
});
