
/* global timerService, expect */

describe('https-server.test.timerServiceTest', function () {
    var dateTime = (new Date()).getTime();

    it('check setCallTimer', function () {
        $('body').append('<div id="timer"></div>');
        jasmine.clock().install();
        spyOn(timerService, 'stopCallTimer');

        timerService.setCallTimer(dateTime);
        expect(timerService.stopCallTimer).not.toHaveBeenCalled();
        expect($("#timer").text().length).toEqual(0);

        jasmine.clock().tick(60001);
        expect($("#timer").text()).toEqual('01:00');

        timerService.setCallTimer();
        expect(timerService.stopCallTimer).toHaveBeenCalled();

        jasmine.clock().uninstall();
        $("#timer").remove();
    });

    it('check stopCallTimer', function () {
        $('body').append('<div id="timer"></div>');
        timerService.setCallTimer(dateTime);
        timerService.stopCallTimer();
        expect($("#timer").text()).toEqual('00:00');
        $("#timer").remove();
    });

});
