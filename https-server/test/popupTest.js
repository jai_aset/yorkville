
/* global popup, expect, audioToneService, storageService, rendererService, listenerService, spyOn, fcsService, logger, dummy, noteService, callService */

describe('https-server.test.popupTest', function () {
    beforeEach(function () {
        resetDummy();
        spyOn(storageService, 'setItem');
        spyOn(storageService, 'listen');
        spyOn(storageService, 'clear');
        spyOn(listenerService, 'popupListener');
        spyOn(audioToneService, 'init');
        spyOn(rendererService, 'showModalMessage');
        spyOn(rendererService, 'hideModal');
        spyOn(rendererService, 'renderCalls');
        spyOn(logger, 'log');
        spyOn(logger, 'warn');
        spyOn(fcsService, 'subscribe').and.callFake(dummy.callBacks);
        spyOn(fcsService, 'unsubscribe');
        spyOn(callService, 'hasOutgoingCall');
        spyOn(callService, 'hasIncomingCall');
        spyOn(window, 'resizeTo');
        spyOn(window, 'close');
        jasmine.clock().install();
    });
    afterEach(function () {
        localStorage.clear();
        jasmine.clock().uninstall();
    });

    it('check onload', function () {
        popup.onload();

        expect(audioToneService.init).toHaveBeenCalled();
        expect(storageService.listen).toHaveBeenCalled();
        expect(rendererService.showModalMessage).toHaveBeenCalledWith('connecting');
        expect(fcsService.subscribe).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Subscription successful');
        expect(rendererService.hideModal).toHaveBeenCalled();
        expect(logger.warn).toHaveBeenCalledWith('no call found in initial check');
        expect(rendererService.renderCalls).toHaveBeenCalled();

        jasmine.clock().tick(1001);
        expect(storageService.setItem).toHaveBeenCalled();
    });
    
    it('check onload subscription failed', function () {
        dummy.callSuccessCallback = false;
        popup.onload();
        
        expect(logger.warn).toHaveBeenCalledWith('Subscription failed');
        expect(rendererService.showModalMessage).toHaveBeenCalledWith('Connection failed');

        jasmine.clock().tick(3001);
        expect(window.close).toHaveBeenCalled();
    });

    it('check onresize', function () {
        window.onresize();
        expect(window.resizeTo).toHaveBeenCalled();
    });

    it('check onbeforeunload', function () {
        window.onbeforeunload();
        expect(fcsService.unsubscribe).toHaveBeenCalled();
        expect(storageService.clear).toHaveBeenCalled();
    });

    describe('check clicks', function () {
        beforeEach(function () {
            $('body').append('<div id="popup" class="container"></div>');
            $('#popup').append('<div class="end_call"></div>');
            $('#popup').append('<div class="hold_call"></div>');
            $('#popup').append('<div class="unhold_call"></div>');
            $('#popup').append('<div class="mute_call"></div>');
            $('#popup').append('<div class="mute_call"></div>');
            $('#popup').append('<div class="add_note"></div>');
            $('#popup').append('<div class="keypad"></div>');
            $('#popup').append('<div id="audio_answer"></div>');
            $('#popup').append('<div id="video_answer"></div>');
            $('#popup').append('<div id="decline_call"></div>');
            $('#popup').append('<div id="ignore_call"></div>');
            $('#popup').append('<div id="incoming_call"></div>');
            $('#popup').attr('call_id', dummy.call.id);
            $('#incoming_call').attr('call_id', dummy.call.id);

            popup.onload();
        });
        afterEach(function () {
            $('#popup').remove();
        });

        it('end_call', function () {
            spyOn(fcsService, 'endCall');

            $('#popup').find('.end_call').click();
            expect(fcsService.endCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('hold_call', function () {
            spyOn(fcsService, 'holdCall');

            $('#popup').find('.hold_call').click();
            expect(fcsService.holdCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('unhold_call', function () {
            spyOn(fcsService, 'unholdCall');

            $('#popup').find('.unhold_call').click();
            expect(fcsService.unholdCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('mute_call', function () {
            spyOn(fcsService, 'muteCall');

            $('#popup').find('.mute_call').click();
            expect(fcsService.muteCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('unmute_call', function () {
            spyOn(fcsService, 'unmuteCall');

            $('#popup').find('.mute_call').addClass('mute_call_active').click();
            expect(fcsService.unmuteCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('add_note', function () {
            spyOn(noteService, 'openDialog');

            $('#popup').find('.add_note').click();
            expect(noteService.openDialog).toHaveBeenCalled();
        });

        it('audio_answer', function () {
            spyOn(fcsService, 'answerAudioCall');

            $('#popup').find('#audio_answer').click();
            expect(fcsService.answerAudioCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('video_answer', function () {
            spyOn(fcsService, 'answerVideoCall');

            $('#popup').find('#video_answer').click();
            expect(fcsService.answerVideoCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('decline_call', function () {
            spyOn(fcsService, 'declineCall');

            $('#popup').find('#decline_call').click();
            expect(fcsService.declineCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('ignore_call', function () {
            spyOn(fcsService, 'ignoreCall');

            $('#popup').find('#ignore_call').click();
            expect(fcsService.ignoreCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('keypad', function () {
            $('#popup').find('.keypad').click();
            expect($('#popup').find('.keypad').hasClass('keypad_active')).toBeTruthy();
        });
    });
});
