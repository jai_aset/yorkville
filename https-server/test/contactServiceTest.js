/* global contactService, expect, spyOn, sforce, logger, dummy, fcsService, SFService, callService, windowService, message */

describe('https-server.test.contactServiceTest', function () {
    var queryContactResult;
    beforeEach(function () {
        resetDummy();
        queryContactResult = {
            size: 1,
            getArray: function () {
                return [dummy.callerName];
            }
        };
        spyOn(dummy, 'functionVoid');
        spyOn(dummy, 'functionVoid2');
        spyOn(logger, 'log');
        spyOn(logger, 'warn');
        spyOn(message, 'warning');
        spyOn(message, 'error');
    });
    afterEach(function () {
        localStorage.clear();
        delete window.__currentContactId;
    });

    describe('check openDialog&addContact', function () {

        beforeEach(function () {
            $('body').append('<div id="add_contact_dialog"></div>');
            $('#add_contact_dialog').append('<input type="text" id="add_contact_phone"/>');
            $('#add_contact_dialog').append('<input type="text" id="add_contact_firstname"/>');
            $('#add_contact_dialog').append('<input type="text" id="add_contact_lastname"/>');
            $('#add_contact_dialog').append('<div class="existed-contacts"></div>');
            $('#add_contact_dialog').append('<span id="add-contact-anyway">here</span>');


            contactService.openDialog(dummy.number);
        });

        afterEach(function () {
            $('#add_contact_dialog').remove();
        });

        it('check openDialog', function () {
            expect($('#add_contact_phone').val()).toEqual(dummy.number);
        });

        describe('check addContact', function () {

            it('without lastName', function () {
                var buttons = $('#add_contact_dialog').dialog('option', 'buttons');
                buttons['Add']();

                expect(message.warning).toHaveBeenCalledWith('Please Enter a Last Name', 2000);
            });

            describe('with lastName', function () {
                beforeEach(function () {
                    $('#add_contact_firstname').val(dummy.callerName);
                    $('#add_contact_lastname').val(dummy.callerName);
                    spyOn(contactService, 'setContactTitle');
                    spyOn(sforce.connection, 'create').and.returnValue([{Id: dummy.sf_contact_id}]);
                });
                it('without existing contact', function () {
                    spyOn(sforce.apex, 'execute').and.returnValue([]);

                    var buttons = $('#add_contact_dialog').dialog('option', 'buttons');
                    buttons['Add']();

                    expect(sforce.apex.execute).toHaveBeenCalled();
                    expect(contactService.setContactTitle).toHaveBeenCalled();
                    expect(sforce.connection.create).toHaveBeenCalled();
                });
                it('with existing contact', function () {
                    var update = true;
                    spyOn(sforce.apex, 'execute').and.returnValue([{Id: dummy.sf_contact_id, Name: dummy.callerName}]);
                    spyOn(sforce.connection, 'update').and.returnValue(
                            [
                                {
                                    getBoolean: function () {
                                        return update;
                                    }
                                }
                            ]
                            );

                    var buttons = $('#add_contact_dialog').dialog('option', 'buttons');
                    buttons['Add']();

                    expect(sforce.apex.execute).toHaveBeenCalled();

                    $('.existed-contact').click();
                    expect(sforce.connection.update).toHaveBeenCalled();
                    expect(contactService.setContactTitle).toHaveBeenCalled();

                    $('#add-contact-anyway').click();
                    expect(sforce.connection.create).toHaveBeenCalled();
                    expect(contactService.setContactTitle).toHaveBeenCalled();

                    update = false;
                    buttons['Add']();
                    $('.existed-contact').click();
                    expect(sforce.connection.update).toHaveBeenCalled();
                    expect(message.error).toHaveBeenCalledWith('Contact update failed', 2000);
                });

            });

        });
    });

    describe('check setContactTitle', function () {
        var contactsMap;
        beforeEach(function () {
            contactsMap = {
                'number': [
                    {
                        Id: dummy.sf_contact_id,
                        Name: dummy.number
                    }
                ]
            };
            $('body').append('<div id="popup"><div id="active_call"><div id="contact"></div></div></div>');

            spyOn(callService, 'getActiveCall').and.returnValue(dummy.call);
            spyOn(callService, 'setCallSFContact');
            spyOn(windowService, 'openContactPage');
            spyOn(contactService, 'getSFContacts').and.callFake(function (callerNumber, callBack) {
                callBack(contactsMap);
            });
        });
        afterEach(function () {
            $('#popup').remove();
        });
        it('if there is sf_contact_id on call', function () {
            dummy.call.sf_contact_id = dummy.sf_contact_id;
            contactService.setContactTitle(dummy.number, dummy.callerName);

            expect(logger.log).toHaveBeenCalledWith('There is an active call with sf contact id: ' + dummy.sf_contact_id);
            expect($("#active_call").find("#contact").find('a').length).toEqual(1);

            $('#contact_redirect').click();
            expect(windowService.openContactPage).toHaveBeenCalledWith(dummy.sf_contact_id);
        });
        describe('if there is no sf_contact_id on call', function () {

            it(' with matched contacts', function () {
                var contact = contactsMap['number'][0];
                contactService.setContactTitle(dummy.number);

                expect(logger.log).toHaveBeenCalledWith('There is no active call to set title');
                expect($("#active_call").find("#contact").text()).toEqual(dummy.number);
                expect($("#active_call").find("#contact").attr('title')).toEqual(dummy.number);
                expect(logger.log).toHaveBeenCalledWith('Getting SF contacts with number: ' + dummy.number);
                expect(contactService.getSFContacts).toHaveBeenCalled();
                expect(logger.log).toHaveBeenCalledWith('The contact is found from map:' + JSON.stringify(contact));
                expect(callService.setCallSFContact).toHaveBeenCalledWith(dummy.call.id, contact.Id);
                expect($("#active_call").find("#contact").find('a').length).toEqual(1);

                $('#contact_redirect').click();
                expect(windowService.openContactPage).toHaveBeenCalledWith(contact.Id);
            });

            it(' with no contact matching', function () {
                contactsMap = {};
                spyOn(contactService, 'openDialog');
                contactService.setContactTitle(dummy.number);

                expect(logger.log).toHaveBeenCalledWith('Contact not found from map. click for add');
                expect($("#active_call").find("#contact").find('a').text()).toEqual(dummy.number + " (Add Contact)");

                $('#contact_title').click();
                expect(contactService.openDialog).toHaveBeenCalledWith(dummy.number);
            });
        });
    });

    describe('check getSFContacts', function () {
        var deferred;
        beforeEach(function () {
            deferred = $.Deferred();
            spyOn(SFService, 'loadSFData').and.returnValue(deferred.promise());
        });
        it('success', function () {
            contactService.getSFContacts(dummy.sf_contact_id, dummy.functionVoid, dummy.functionVoid2);
            var resultText = '{"meetme@genband.com":[{"attributes":{"type":"Contact","url":"/services/data/v35.0/sobjects/Contact/0032400000MJbhrAAD"},"Id":"0032400000MJbhrAAD","Name":"meet me"}]}';
            var responseText = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://soap.sforce.com/schemas/package/GenbandController"><soapenv:Body><getContactsResponse><result>' + resultText + '</result></getContactsResponse></soapenv:Body></soapenv:Envelope>';
            var responseXML = (new DOMParser()).parseFromString(responseText, "text/xml");
            deferred.resolve(responseXML);
            expect(dummy.functionVoid).toHaveBeenCalledWith(JSON.parse(resultText));
        });
        it('failed', function () {
            contactService.getSFContacts(dummy.sf_contact_id, dummy.functionVoid, dummy.functionVoid2);
            deferred.reject();
            expect(dummy.functionVoid2).toHaveBeenCalled();
        });
    });

    describe('check searchSFContact', function () {
        beforeEach(function () {
            spyOn(sforce.connection, 'query').and.callFake(function (query, callbacks) {
                if (dummy.callSuccessCallback) {
                    callbacks.onSuccess(queryContactResult);
                } else
                    callbacks.onFailure(dummy.error);
            });
        });
        it('query success', function () {
            contactService.searchSFContact(dummy.text, dummy.functionVoid);
            expect(dummy.functionVoid).toHaveBeenCalledWith([dummy.callerName]);
        });
        it('query failed', function () {
            dummy.callSuccessCallback = false;
            contactService.searchSFContact(dummy.text, dummy.functionVoid);
            expect(logger.warn).toHaveBeenCalledWith('searchContact: ' + dummy.error);
        });
    });

    it('check getAllSFContacts', function () {
        spyOn(contactService, 'searchSFContact');
        contactService.getAllSFContacts(dummy.functionVoid);
        expect(contactService.searchSFContact).toHaveBeenCalled();
    });

    it('check searchSpidrContact', function () {
        spyOn(fcsService, 'searchDirectory');
        contactService.searchSpidrContact(dummy.email, dummy.functionVoid);
        expect(fcsService.searchDirectory).toHaveBeenCalled();
    });

    describe('check checkCurrentContact', function () {

        it('without contact', function () {
            contactService.checkCurrentContact(dummy.functionVoid, dummy.functionVoid2);
            expect(dummy.functionVoid2).toHaveBeenCalled();
        });

        describe('with contact ', function () {

            beforeEach(function () {
                window.__currentContactId = dummy.sf_contact_id;
                spyOn(sforce.connection, 'query').and.callFake(function (query, callbacks) {
                    if (dummy.callSuccessCallback) {
                        callbacks.onSuccess(queryContactResult);
                    } else
                        callbacks.onFailure(dummy.error);
                });
            });

            it('query success', function () {
                contactService.checkCurrentContact(dummy.functionVoid, dummy.functionVoid2);
                expect(dummy.functionVoid).toHaveBeenCalledWith(dummy.callerName);
                expect(window.name).toEqual(dummy.sf_contact_id);
            });

            it('query failed', function () {
                dummy.callSuccessCallback = false;
                contactService.checkCurrentContact(dummy.functionVoid, dummy.functionVoid2);
                expect(logger.warn).toHaveBeenCalledWith('checkCurrentContact: ' + dummy.error);
            });
        });
    });
});
