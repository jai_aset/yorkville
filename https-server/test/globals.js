var getCookie = function () {
},
        dummy,
        sforce = {
            sessionId: 1,
            connection: {
                sessionId: 1,
                upsert: function () {
                    return [];
                },
                sendEmail: getCookie,
                query: getCookie,
                create: getCookie,
                update: getCookie
            },
            apex: {
                execute: getCookie
            },
            SObject: function () {
                return {};
            },
            SingleEmailMessage: getCookie
        };
window.UserContext = {userId: 'address'};
window._testonly = true;
window.onload = function(){};
$('body').append('<div id="for_video_call" style="display:none;"><input id="first_name"><input id="last_name"><div id="start_button"></div><div id="end_call"></div></div>');

function resetDummy() {
    dummy = {
        id: '1',
        text: 'text',
        error: 'error',
        url: 'url',
        number: 'number',
        email: 'a@b.com',
        callerName: 'caller name',
        sf_contact_id: '0032400000MJbhrAAD',
        callSuccessCallback: true,
        username: 'username',
        password: 'pass'
    };
    dummy.functionVoid = function () {
    };
    dummy.functionVoid2 = function () {
    };
    dummy.callBacks = function (success, error) {
        if (dummy.callSuccessCallback)
            success();
        else
            error(dummy.error);
    };
    dummy.call = {
        id: dummy.id,
        callerNumber: dummy.number,
        callerName: dummy.callerName,
        getId: dummy.functionVoid,
        canReject: dummy.functionVoid,
        isVideoNegotationAvailable: dummy.functionVoid,
        mute: dummy.functionVoid,
        unmute: dummy.functionVoid,
        ignore: dummy.functionVoid,
        reject: dummy.functionVoid,
        hold: dummy.functionVoid,
        unhold: dummy.functionVoid,
        end: dummy.functionVoid,
        answer: dummy.functionVoid,
        canSendVideo: dummy.functionVoid,
        canReceiveVideo: dummy.functionVoid,
        sendDTMF: dummy.functionVoid
    };
    dummy.incomingCall = {
        '1': {
            id: dummy.id,
            contact: dummy.callerName,
            sf_contact_id: dummy.sf_contact_id,
            contactPhoto: dummy.url,
            command: 'answer'
        }
    };
    dummy.outgoingCall = {
        '1': {
            id: dummy.id,
            contact: dummy.callerName,
            sf_contact_id: dummy.sf_contact_id,
            contactPhoto: dummy.url
        }
    };
    dummy.sf_contact = {
        Id: dummy.sf_contact_id,
        Name: dummy.callerName,
        Email: dummy.email,
        Phone: dummy.number
    };
}