
/* global spyOn, logService, fcsService, moment, SFService, contactService, logger, expect */

describe('https-server.test.logServiceTest', function () {
    var deferred,
            userAddress = window.UserContext.userId,
            dateTime = (new Date()).getTime(),
            calllogs = [
                {address: userAddress, type: 0, startTime: dateTime + 1},
                {address: userAddress, type: 1, startTime: dateTime},
                {address: userAddress, type: 2, startTime: dateTime + 1}
            ],
            contactsMap = {
                address: [
                    {Name: 'userName'},
                    {Name: 'userName2'},
                    {Name: 'userName3'}
                ]
            };


    beforeEach(function () {
        deferred = $.Deferred();
        spyOn(SFService, 'loadSFData').and.returnValue(deferred.promise());
        spyOn(fcsService, 'retrieveCalllog').and.callFake(function (successCallback, isSet) {
            successCallback(calllogs);
        });
        spyOn(contactService, 'getSFContacts').and.callFake(function (distinctCalllogs, successCallback) {
            successCallback(contactsMap);
        });
        spyOn(logger, 'log');
        spyOn(logger, 'warn');
        spyOn(logger, 'info');
    });
    describe('check updateActivityHistory', function () {

        it(' with SFService.loadSFData success', function () {
            logService.updateActivityHistory();
            expect(SFService.loadSFData).toHaveBeenCalled();
            expect(fcsService.retrieveCalllog).toHaveBeenCalled();
            expect(contactService.getSFContacts).toHaveBeenCalled();

            deferred.resolve();
            expect(logger.log).toHaveBeenCalledWith('Activity history successfully updated');
        });
        it(' with SFService.loadSFData fail', function () {
            logService.updateActivityHistory();
            deferred.reject('error');
            expect(logger.warn).toHaveBeenCalledWith('Activity history update failed', 'error');
        });
    });

    it('check refreshCallLogGrid', function () {
        spyOn($('#spidr_call_logs'), 'jqGrid');
        logService.refreshCallLogGrid();
        expect(logger.info).toHaveBeenCalledWith('Refreshing call log grid...');
        expect(fcsService.retrieveCalllog).toHaveBeenCalled();
        expect(contactService.getSFContacts).toHaveBeenCalled();
    });

});
