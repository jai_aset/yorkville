/* global widget, expect, spyOn, sforce, logger, windowService, fcsService, storageService, rendererService, dummy, SFService, logService, callService, contactService, videoInvitationService */

describe('https-server.test.widgetTest', function () {
    var deferred, deferredLoadAgreement, deferredCheckLoginState;
    beforeEach(function () {
        resetDummy();
        deferred = $.Deferred();
        deferredLoadAgreement = $.Deferred();
        deferredCheckLoginState = $.Deferred();
        spyOn(SFService, 'loadSFData').and.callFake(function (methodName) {
            if (methodName === 'loadAgreement')
                return deferredLoadAgreement.promise();
            else if (methodName === 'checkLoginState')
                return deferredCheckLoginState.promise();
            return deferred.promise();
        });
        spyOn(fcsService, 'subscribe').and.callFake(dummy.callBacks);
        spyOn(SFService, 'parseSFResult').and.callFake(function (a) {
            return a;
        });
        spyOn(sforce.apex, 'execute').and.callFake(function (controller, method, param, callbacks) {
            callbacks.onSuccess();
        });
        spyOn(windowService, 'isPopupAlive').and.returnValue(false);
        spyOn(widget, 'reload');
        spyOn(logger, 'log');
        spyOn(storageService, 'listen');
        spyOn(logService, 'updateActivityHistory');
        spyOn(rendererService, 'setNiceScroll');
        window.i_am_widget = true;
    });
    afterEach(function () {
        localStorage.clear();
        delete window.i_am_widget;
    });

    it('check onload', function () {
        widget.onload();
        deferredLoadAgreement.resolve('true');
        expect(windowService.isPopupAlive).toHaveBeenCalled();
        expect(SFService.loadSFData).toHaveBeenCalledWith('loadAgreement');
        expect(SFService.loadSFData).toHaveBeenCalledWith('checkLoginState');
        expect(storageService.listen).toHaveBeenCalled();
    });

    it('check onload agreed=false ', function () {
        $('body').append('<div id="agree"></div>');

        widget.onload();
        deferredLoadAgreement.resolve('false');
        expect(SFService.loadSFData).toHaveBeenCalledWith('loadAgreement');

        $('#agree').click();
        expect(sforce.apex.execute).toHaveBeenCalled();

        $('#agree').remove();
    });

    it('check onload loginState=false ', function () {
        $('body').append('<div id="login_container"><div id="login"></div></div>');

        widget.onload();
        deferredLoadAgreement.resolve('true');
        deferredCheckLoginState.resolve('false');
        expect(SFService.loadSFData).toHaveBeenCalledWith('loadAgreement');

        $('#login').click();
        expect(sforce.apex.execute).toHaveBeenCalled();
        expect(widget.reload).toHaveBeenCalled();

        $('#app_container').remove();
    });

    it('check onbeforeunload', function () {
        spyOn(widget, 'unsubscribe');
        window.onbeforeunload();
        expect(widget.unsubscribe).toHaveBeenCalled();
    });

    it('check unsubscribe', function () {
        spyOn(windowService, 'getNumberOfTabs').and.returnValue(1);
        spyOn(windowService, 'removeTabId');
        spyOn(fcsService, 'unsubscribe');
        spyOn(storageService, 'clear');

        widget.unsubscribe();

        expect(windowService.getNumberOfTabs).toHaveBeenCalled();
        expect(windowService.removeTabId).toHaveBeenCalled();
        expect(windowService.isPopupAlive).toHaveBeenCalled();
        expect(fcsService.unsubscribe).toHaveBeenCalled();
        expect(storageService.clear).toHaveBeenCalled();
    });

    it('check subscribe', function () {
        $('body').append('<div id="app_container"><div id="logout"></div><div id="connection_state"></div></div>');
        spyOn(windowService, 'createTabId');
        spyOn(rendererService, 'renderCalls');
        spyOn(rendererService, 'renderIncomingCalls');
        spyOn(windowService, 'amIMasterTab').and.returnValue(true);
        spyOn(fcsService, 'isConnected').and.returnValue(false);
        spyOn(callService, 'hasCall').and.returnValue(false);
        spyOn(widget, 'unsubscribe');

        widget.onload();
        deferredLoadAgreement.resolve('true');
        deferredCheckLoginState.resolve('true');

        widget.subscribe(false);

        expect(windowService.createTabId).toHaveBeenCalled();
        expect(windowService.isPopupAlive).toHaveBeenCalled();
        expect(windowService.amIMasterTab).toHaveBeenCalled();
        expect(fcsService.isConnected).toHaveBeenCalled();

        expect(logger.log).toHaveBeenCalledWith('Subscription successful');

        expect($('#connection_state').hasClass('connected')).toBeTruthy();
        expect($('#connection_state').text()).toEqual('Connected');
        expect(rendererService.renderCalls).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();

        $('#logout').click();
        expect(sforce.apex.execute).toHaveBeenCalled();
        expect(widget.unsubscribe).toHaveBeenCalled();
        expect(widget.reload).toHaveBeenCalled();

        $('#app_container').remove();
    });

    it('check enableTab', function () {
        var tab = 'addressbook';
        $('body').append('<div id="tab_' + tab + '"></div>');
        spyOn(widget, 'setTab');

        widget.enableTab(tab, true);

        expect($("#tab_" + tab).css("display")).toEqual("inline-block");
        expect(widget.setTab).toHaveBeenCalledWith(tab);

        $('#tab_' + tab).remove();
    });

    it('check disableTab', function () {
        var tab1 = 'addressbook', tab2 = 'contact';
        $('body').append('<div id="tab_' + tab1 + '"></div>');
        $('body').append('<div id="tab_' + tab2 + '"></div>');
        widget.setTab(tab1);
        widget.setTab(tab2);

        spyOn(widget, 'setTab');

        widget.disableTab(tab2);

        expect($("#tab_" + tab2).css("display")).toEqual("none");
        expect(widget.setTab).toHaveBeenCalledWith(tab1);

        $('#tab_' + tab1).remove();
        $('#tab_' + tab2).remove();
    });

    describe('check clicks', function () {
        beforeEach(function () {
            $('body').append('<div id="widget"><div id="tabs"></div></div>');
            $('#tabs').append('<div class="tab" id="tab_tabM"></div>');
            $('#tabs').append('<input type="text" id="addressbook_search"></div>');
            $('#tabs').append('<div id="contacts"></div>');
            $('#widget').append('<div id="tab_content_active_call" class="container"></div>');
            $('#tab_content_active_call').append('<div class="end_call"></div>');
            $('#tab_content_active_call').append('<div class="hold_call"></div>');
            $('#tab_content_active_call').append('<div class="unhold_call"></div>');
            $('#tab_content_active_call').append('<div class="mute_call"></div>');
            $('#tab_content_active_call').append('<div class="mute_call"></div>');
            $('#tab_content_active_call').append('<div class="held_call_text"></div>');
            $('#widget').append('<div id="active_call"><div id="contact"></div></div>');

            $('#tab_content_active_call').attr('call_id', dummy.call.id);

            spyOn(windowService, 'openPopup');
            spyOn(contactService, 'searchSFContact').and.callFake(function (text, callback) {
                callback([dummy.sf_contact]);
            });

            jasmine.clock().install();

            widget.onload();
        });
        afterEach(function () {
            $('#widget').remove();
            jasmine.clock().uninstall();
        });

        it('tab', function () {
            spyOn(widget, 'setTab');

            $('#widget').find('.tab').click();
            expect(widget.setTab).toHaveBeenCalledWith('tabM');
        });

        it('addressbook_search', function () {
            $('#addressbook_search').val(dummy.text);
            $('#addressbook_search').keyup();
            jasmine.clock().tick(701);

            expect(contactService.searchSFContact).toHaveBeenCalled();
            expect($('#contacts').html().length).not.toEqual(0);
            expect($('#contacts').find('.name').text()).toEqual(dummy.callerName);
            expect(rendererService.setNiceScroll).toHaveBeenCalled();
        });

        it('contacts name', function () {
            $('#addressbook_search').val(dummy.text);
            $('#addressbook_search').keyup();
            jasmine.clock().tick(701);

            $('#widget').find('.name').click();
            expect(rendererService.setNiceScroll).toHaveBeenCalled();
        });

        it('contacts phone_button', function () {
            spyOn(callService, 'addOutgoingCall');

            $('#addressbook_search').val(dummy.text);
            $('#addressbook_search').keyup();
            jasmine.clock().tick(701);

            $('#widget').find('.phone_button').click();
            // fix this later
//            expect(callService.addOutgoingCall).toHaveBeenCalledWith(dummy.callerName, dummy.number, dummy.sf_contact_id);
            expect(windowService.openPopup).toHaveBeenCalled();
        });

        it('contacts email_button', function () {
            spyOn(videoInvitationService, 'openDialog');

            $('#addressbook_search').val(dummy.text);
            $('#addressbook_search').keyup();
            jasmine.clock().tick(701);

            $('#widget').find('.email_button').click();
            expect(videoInvitationService.openDialog).toHaveBeenCalledWith(dummy.email);
        });

        it('active_call end_call', function () {
            spyOn(callService.trigger, 'endCall');

            $('#tab_content_active_call').find('.end_call').click();
            expect(callService.trigger.endCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('active_call hold_call', function () {
            spyOn(callService.trigger, 'holdCall');

            $('#tab_content_active_call').find('.hold_call').click();
            expect(callService.trigger.holdCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('active_call unhold_call', function () {
            spyOn(callService.trigger, 'unholdCall');

            $('#tab_content_active_call').find('.unhold_call').click();
            expect(callService.trigger.unholdCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('active_call mute_call', function () {
            spyOn(callService.trigger, 'muteCall');

            $('#tab_content_active_call').find('.mute_call').click();
            expect(callService.trigger.muteCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('active_call unmute_call', function () {
            spyOn(callService.trigger, 'unmuteCall');

            $('#tab_content_active_call').find('.mute_call').addClass('mute_call_active').click();
            expect(callService.trigger.unmuteCall).toHaveBeenCalledWith(dummy.call.id);
        });

        it('active_call held_call_text', function () {
            $('#tab_content_active_call').find('.held_call_text').click();
            expect(windowService.openPopup).toHaveBeenCalled();
        });

        it('active_call contact', function () {
            $('#active_call').find('#contact').click();
            expect(windowService.openPopup).toHaveBeenCalled();
        });
    });



    it('check checkCurrentContact', function () {
        spyOn(windowService, 'amIMasterTab').and.returnValue(false);
        spyOn(widget, 'setTab');
        spyOn(widget, 'enableTab');
        spyOn(rendererService, 'renderCalls');
        spyOn(rendererService, 'renderIncomingCalls');
        spyOn(contactService, 'checkCurrentContact').and.callFake(function (callback) {
            callback(dummy.sf_contact);
        });
        spyOn(callService, 'hasCall').and.returnValue(false);

        widget.subscribe(false);

        expect(windowService.amIMasterTab).toHaveBeenCalled();
        expect(widget.setTab).toHaveBeenCalledWith('addressbook');
        expect(rendererService.renderCalls).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();

        expect(contactService.checkCurrentContact).toHaveBeenCalled();
        expect(widget.enableTab).toHaveBeenCalledWith("contact_card", true);
        expect(rendererService.setNiceScroll).toHaveBeenCalled();
    });
});
