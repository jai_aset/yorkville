
/* global audioToneService, fcsService, dummy, fcs, expect, spyOn */

describe('https-server.test.videoCallTest', function () {
    beforeEach(function () {
        resetDummy();
        spyOn(audioToneService, 'init');
        spyOn(audioToneService, 'play');
        spyOn(audioToneService, 'stop');
        spyOn(window, 'open');
        spyOn(fcsService, 'setUserAuth');
        spyOn(fcsService, 'setup');
        spyOn(fcsService, 'notificationStart').and.callFake(dummy.callBacks);
        spyOn(fcsService, 'initMedia').and.callFake(dummy.callBacks);
        spyOn(window.localStorage, 'removeItem');
        spyOn(window.console, 'log');
        spyOn(fcs.notification, 'stop');
        spyOn(fcs.call, 'startCall').and.callFake(function (user, name, number, successCallBack, errorCallBack) {
            if (dummy.callSuccessCallback)
                successCallBack(dummy.call);
            else
                errorCallBack(dummy.error);
        });
        spyOn(fcs.call, 'createStreamRenderer');
        spyOn(fcs.call, 'disposeStreamRenderer');
    });
    afterEach(function () {
        localStorage.clear();
    });

    it('check start_button', function () {
        $('#for_video_call').find('#last_name').val(dummy.callerName);
        var dateTime = (new Date()).getTime();
        window.setHashParams('#call?to=' + dummy.username + '&rest_server=&rest_port=443&ws_server=&ws_port=443&valid_from=' + dateTime + '&valid_to=' + (dateTime + 1000));

        $('#start_button').click();

        expect(fcsService.setUserAuth).toHaveBeenCalled();
        expect(fcsService.setup).toHaveBeenCalled();
        expect(fcsService.notificationStart).toHaveBeenCalled();
        expect(fcsService.initMedia).toHaveBeenCalled();
        expect(fcs.call.startCall).toHaveBeenCalled();

        dummy.call.onStreamAdded(dummy.url);
        expect(fcs.call.createStreamRenderer).toHaveBeenCalled();

        dummy.call.onStateChange(fcs.call.States.RINGING, '1');
        expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.RING_OUT);
        
        dummy.call.onStateChange(fcs.call.States.IN_CALL, '1');
        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
        
        dummy.call.onStateChange(fcs.call.States.ENDED, '1');
        expect(fcs.call.disposeStreamRenderer).toHaveBeenCalled();
        
        dummy.call.onStateChange(fcs.call.States.ON_HOLD, '1');
        expect(fcs.call.disposeStreamRenderer).toHaveBeenCalled();
    });

    it('check end_call', function () {
        var currentCall = {
            end: function (callback) {
                callback();
            }
        };
        window.setCurrentCall(currentCall);
        $('#end_call').click();

        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
        expect(fcs.notification.stop).toHaveBeenCalled();
        expect(window.localStorage.removeItem).toHaveBeenCalledWith("USERNAME");
        expect(window.localStorage.removeItem).toHaveBeenCalledWith("PASSWORD");


        $('#for_video_call').remove();

    });
});
