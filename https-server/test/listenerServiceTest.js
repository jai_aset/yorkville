/* global spyOn, fcsService, rendererService, storageService, logService, widget, expect, listenerService, windowService, dummy */

describe('https-server.test.listenerServiceTest', function () {

    var new_value = 'newValue';

    beforeEach(function () {
        resetDummy();
        spyOn(fcsService, 'startCall');
        spyOn(fcsService, 'isConnected').and.returnValue(true);
        spyOn(fcsService, 'handleIncomingCall');
        spyOn(fcsService, 'endCall');
        spyOn(fcsService, 'holdCall');
        spyOn(fcsService, 'unholdCall');
        spyOn(fcsService, 'muteCall');
        spyOn(fcsService, 'unmuteCall');
        spyOn(fcsService, 'unsubscribe').and.callFake(function (callback) {
            callback();
        });
        spyOn(rendererService, 'renderCalls');
        spyOn(rendererService, 'renderIncomingCalls');
        spyOn(rendererService, 'setStatus');
        spyOn(storageService, 'removeItem');
        spyOn(widget, 'subscribe');
        spyOn(logService, 'refreshCallLogGrid');
    });
    afterEach(function () {
        localStorage.clear();
    });

    describe('check popupListener for key', function () {
        it('outgoing_call', function () {
            var event = {key: 'outgoing_call'};
            listenerService.popupListener(event);
            expect(fcsService.startCall).toHaveBeenCalled();
        });

        it('incoming_call', function () {
            var event = {key: 'incoming_call'};
            listenerService.popupListener(event);
            expect(fcsService.handleIncomingCall).toHaveBeenCalled();
        });

        it('calls', function () {
            var event = {key: 'calls'};
            listenerService.popupListener(event);
            expect(rendererService.renderCalls).toHaveBeenCalled();
        });

        it('end_call', function () {
            var event = {key: 'end_call'};
            listenerService.popupListener(event);
            expect(fcsService.endCall).toHaveBeenCalled();
        });

        it('hold_call', function () {
            var event = {key: 'hold_call', newValue: new_value};
            listenerService.popupListener(event);
            expect(fcsService.holdCall).toHaveBeenCalledWith(new_value);
            expect(storageService.removeItem).toHaveBeenCalledWith(event.key);
        });

        it('unhold_call', function () {
            var event = {key: 'unhold_call', newValue: new_value};
            listenerService.popupListener(event);
            expect(fcsService.unholdCall).toHaveBeenCalledWith(new_value);
            expect(storageService.removeItem).toHaveBeenCalledWith(event.key);
        });

        it('mute_call', function () {
            var event = {key: 'mute_call', newValue: new_value};
            listenerService.popupListener(event);
            expect(fcsService.muteCall).toHaveBeenCalledWith(new_value);
            expect(storageService.removeItem).toHaveBeenCalledWith(event.key);
        });

        it('unmute_call', function () {
            var event = {key: 'unmute_call', newValue: new_value};
            listenerService.popupListener(event);
            expect(fcsService.unmuteCall).toHaveBeenCalledWith(new_value);
            expect(storageService.removeItem).toHaveBeenCalledWith(event.key);
        });

        it('default', function () {
            var event = {key: 'default'};
            listenerService.popupListener(event);
            expect(rendererService.renderCalls).toHaveBeenCalled();
        });
    });


    describe('check widgetListener  for key', function () {
        it('call_status', function () {
            var event = {key: 'call_status', newValue: new_value};
            listenerService.widgetListener(event);
            expect(rendererService.setStatus).toHaveBeenCalledWith(new_value);
        });

        it('incoming_call', function () {
            var event = {key: 'incoming_call'};
            listenerService.widgetListener(event);
            expect(rendererService.renderIncomingCalls).toHaveBeenCalled();
        });

        it('calls', function () {
            var event = {key: 'calls'};
            listenerService.widgetListener(event);
            expect(rendererService.renderCalls).toHaveBeenCalled();
        });

        it('popup_opened', function () {
            var event = {key: 'popup_opened'};
            listenerService.widgetListener(event);
            expect(fcsService.unsubscribe).toHaveBeenCalled();
            expect(widget.subscribe).toHaveBeenCalled();
        });

        it('default', function () {
            var event = {key: windowService.getPrefix()};
            listenerService.widgetListener(event);
            expect(widget.subscribe).toHaveBeenCalled();
        });
    });

    describe('check callLogListener  for key', function () {
        it('popup_opened', function () {
            var event = {key: 'popup_opened'};
            listenerService.callLogListener(event);
            expect(logService.refreshCallLogGrid).toHaveBeenCalled();
        });
    });

});
