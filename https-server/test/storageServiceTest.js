/* global storageService, expect, dummy */

describe('https-server.test.storageServiceTest', function () {
    var key = "calls",
            value = "dummy_value",
            valueJSON = {'key': value};

    beforeEach(function () {
        resetDummy();
    });
    afterEach(function () {
        localStorage.clear();
    });

    it('check setting item', function () {
        storageService.setItem(key, value);
        expect(localStorage.getItem(key)).toBe(value);
    });

    it('check getting item', function () {
        storageService.setItem(key, value);
        expect(storageService.getItem(key)).toBe(value);
    });

    it('check setting item from JSON', function () {
        storageService.setItemFromJSON(key, valueJSON);
        expect(localStorage.getItem(key)).toBe(JSON.stringify(valueJSON));
    });

    it('check getting item as JSON', function () {
        storageService.setItemFromJSON(key, valueJSON);
        expect(JSON.stringify(storageService.getItemAsJSON(key))).toBe(JSON.stringify(valueJSON));
    });

    it('check clearing all items', function () {
        storageService.setItem(key, value);
        expect(storageService.getItem(key)).not.toBeNull();
        storageService.clear();
        expect(storageService.getItem(key)).toBeNull();
    });

    it('check setting listener', function () {
        spyOn(window, 'addEventListener');
        storageService.listen(dummy.functionVoid());
        expect(window.addEventListener).toHaveBeenCalled();
    });

});
