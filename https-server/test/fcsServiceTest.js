/* global spyOn, fcs, SFService, fcsService, expect, logger, message, rendererService, dtmfService, callService, audioToneService, windowService, contactService, phoneNumberService, timerService, dummy, storageService */

describe('https-server.test.fcsServiceTest', function () {

    var credentials, contactsMap;

    beforeEach(function () {
        resetDummy();
        credentials = {username: dummy.username, password: dummy.password},
        contactsMap = {
            number: [
                {
                    Id: dummy.username,
                    Name: dummy.password
                }
            ]
        };
        window.i_am_widget = false;
        spyOn(fcs, 'setup');
        spyOn(fcs, 'setUserAuth');
        spyOn(fcs.notification, 'start').and.callFake(dummy.callBacks);
        spyOn(fcs.notification, 'stop');
        spyOn(fcs.call, 'initMedia');
        spyOn(SFService, 'loadCredentials').and.callFake(function (callBack) {
            callBack(credentials);
        });
        spyOn(logger, 'log');
        spyOn(logger, 'warn');
        spyOn(logger, 'info');
        spyOn(message, dummy.error);
        spyOn(rendererService, 'renderCalls');
        spyOn(rendererService, 'renderIncomingCalls');
        spyOn(rendererService, 'setStatus');
        spyOn(audioToneService, 'stop');
        spyOn(dummy, 'functionVoid');
        spyOn(dummy, 'functionVoid2');
        spyOn(contactService, 'getSFContacts').and.callFake(function (callerNumber, callBack) {
            callBack(contactsMap);
        });
        spyOn(contactService, 'searchSpidrContact').and.callFake(function (callerNumber, callBack) {
            callBack([{firstName: 'caller', lastName: 'name', photoUrl: dummy.url}]);
        });
        spyOn(windowService, 'openPopup');
        spyOn(dummy.call, 'getId').and.returnValue(dummy.call.id);
        spyOn(dummy.call, 'canReject');
        spyOn(dummy.call, 'isVideoNegotationAvailable');
        spyOn(dummy.call, 'canReceiveVideo').and.returnValue(true);
        spyOn(dummy.call, 'canSendVideo').and.returnValue(true);
        spyOn(callService, 'getActiveCall').and.returnValue(dummy.call);
        spyOn(timerService, 'stopCallTimer');
        spyOn(timerService, 'setCallTimer');
        jasmine.clock().install();
        fcsService.deleteFromCalls(dummy.call.id);
    });

    afterEach(function () {
        delete window.i_am_widget;
        localStorage.clear();
        fcsService.deleteFromCalls(dummy.call.id);
        jasmine.clock().uninstall();
    });

    it('check isConnected', function () {
        expect(fcsService.isConnected()).toBeFalsy();
    });

    it('check setup', function () {
        fcsService.setup(credentials);
        expect(fcs.setup).toHaveBeenCalled();
    });

    it('check setUserAuth', function () {
        fcsService.setUserAuth(credentials);
        expect(fcs.setUserAuth).toHaveBeenCalledWith(credentials.username, credentials.password);
    });

    describe('check notificationStart:', function () {
        it('notification.start successfull', function () {
            fcsService.notificationStart(dummy.functionVoid, dummy.functionVoid2);
            expect(fcs.notification.start).toHaveBeenCalled();
            expect(logger.log).toHaveBeenCalledWith('Notification start successful');
        });
        it('notification.start failed', function () {
            dummy.callSuccessCallback = false;
            fcsService.notificationStart(dummy.functionVoid, dummy.functionVoid2);
            expect(fcs.notification.start).toHaveBeenCalled();
            expect(logger.warn).toHaveBeenCalledWith('Notification start failed: ', dummy.error);
        });
    });

    it('check initMedia', function () {
        fcsService.initMedia();
        expect(fcs.call.initMedia).toHaveBeenCalled();
    });

    describe('check subscribe ', function () {
        var initMediaSuccessCallback = true;
        beforeEach(function () {
            spyOn(fcsService, 'setUserAuth');
            spyOn(fcsService, 'setup');
            spyOn(fcsService, 'initMedia').and.callFake(function (success, error) {
                if (initMediaSuccessCallback)
                    success();
                else
                    error(fcs.call.MediaErrors.WRONG_VERSION);
            });
            spyOn(fcsService, 'notificationStart').and.callFake(dummy.callBacks);
            spyOn(window, 'open');
        });
        describe('initMedia:', function () {
            describe('success ', function () {
                it('notificationStart:success', function () {
                    fcsService.subscribe(dummy.functionVoid, dummy.functionVoid2);
                    expect(SFService.loadCredentials).toHaveBeenCalled();
                    expect(fcsService.setUserAuth).toHaveBeenCalled();
                    expect(fcsService.setup).toHaveBeenCalled();
                    expect(fcsService.initMedia).toHaveBeenCalled();
                    expect(logger.log).toHaveBeenCalledWith('Media initialized successfully');
                    expect(fcsService.notificationStart).toHaveBeenCalled();
                    expect(dummy.functionVoid).toHaveBeenCalled();
                    expect(dummy.functionVoid2).not.toHaveBeenCalled();
                });
                it('notificationStart:failed', function () {
                    dummy.callSuccessCallback = false;
                    fcsService.subscribe(dummy.functionVoid, dummy.functionVoid2);
                    expect(SFService.loadCredentials).toHaveBeenCalled();
                    expect(fcsService.setUserAuth).toHaveBeenCalled();
                    expect(fcsService.setup).toHaveBeenCalled();
                    expect(fcsService.initMedia).toHaveBeenCalled();
                    expect(logger.log).toHaveBeenCalledWith('Media initialized successfully');
                    expect(fcsService.notificationStart).toHaveBeenCalled();
                    expect(dummy.functionVoid2).toHaveBeenCalled();
                    expect(dummy.functionVoid).not.toHaveBeenCalled();
                });
            });
            it('failed', function () {
                $('body').append('<div id="plugin_install" style="display:none;"></div><div id="download_plugin"></div>');
                initMediaSuccessCallback = false;
                window.i_am_widget = true;
                expect($("#plugin_install").is(":visible")).toBeFalsy();
                fcsService.subscribe(dummy.functionVoid, dummy.functionVoid2);
                expect(SFService.loadCredentials).toHaveBeenCalled();
                expect(fcsService.setUserAuth).toHaveBeenCalled();
                expect(fcsService.setup).toHaveBeenCalled();
                expect(fcsService.initMedia).toHaveBeenCalled();
                expect(logger.warn).toHaveBeenCalledWith("Media initialization failed: ", fcs.call.MediaErrors.WRONG_VERSION);
                expect(fcsService.notificationStart).not.toHaveBeenCalled();
                expect(dummy.functionVoid2).toHaveBeenCalled();
                expect(dummy.functionVoid).not.toHaveBeenCalled();
                expect($("#plugin_install").is(":visible")).toBeTruthy();

                $("#download_plugin").click();
                expect(window.open).toHaveBeenCalledWith(window.config.pluginLink, "_blank");
            });
        });
    });

    it('check unsubscribe', function () {
        spyOn(fcsService, 'endCall');
        spyOn(fcsService, 'isConnected').and.returnValue(true);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.unsubscribe(dummy.functionVoid);
        expect(fcsService.endCall).toHaveBeenCalledWith(dummy.call.id);
        expect(fcs.notification.stop).toHaveBeenCalled();

        fcsService.unsubscribe();
        expect(fcs.notification.stop).toHaveBeenCalled();
    });

    it('check onCallReceived', function () {
        window.i_am_widget = true;
        spyOn(callService, 'addIncomingCall');
        spyOn(callService, 'setIncomingCallSFContact');
        spyOn(callService, 'setIncomingCallContactPhoto');
        fcsService.onCallReceived(dummy.call);
        expect(dummy.call.getId).toHaveBeenCalled();
        expect(dummy.call.canReject).toHaveBeenCalled();
        expect(dummy.call.isVideoNegotationAvailable).toHaveBeenCalledWith(dummy.call.id);
        expect(callService.addIncomingCall).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();
        expect(windowService.openPopup).toHaveBeenCalled();
        expect(contactService.getSFContacts).toHaveBeenCalled();
        expect(callService.setIncomingCallSFContact).toHaveBeenCalledWith(dummy.call.id, credentials.password, credentials.username);
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();
        expect(callService.setIncomingCallContactPhoto).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();
    });

    describe('check startCall', function () {

        beforeEach(function () {
            spyOn(callService, 'deleteOutgoingCall');
            spyOn(callService, 'addCall');
            spyOn(callService, 'endCall');
            spyOn(callService, 'setCallContactPhoto');
            spyOn(callService, 'getOutgoingCall').and.returnValue({id: dummy.call.id, number: dummy.call.callerNumber});
            spyOn(phoneNumberService, 'standardizePhoneNumber').and.callFake(function (number) {
                return {forCall: number};
            });
            spyOn(phoneNumberService, 'appendDomain').and.callFake(function (number) {
                return number;
            });
            spyOn(fcsService, 'holdCall');
            spyOn(fcsService, 'deleteFromCalls');
            spyOn(fcs.call, 'startCall').and.callFake(function (user, name, number, successCallBack, errorCallBack) {
                if (dummy.callSuccessCallback)
                    successCallBack(dummy.call);
                else
                    errorCallBack(dummy.error);
            });
            spyOn(audioToneService, 'play');

            fcsService.startCall();
        });

        it('success scenario', function () {
            expect(logger.log).toHaveBeenCalledWith('Starting call...');
            expect(rendererService.setStatus).toHaveBeenCalledWith('connecting');
            expect(timerService.stopCallTimer).toHaveBeenCalled();
            expect(phoneNumberService.standardizePhoneNumber).toHaveBeenCalledWith(dummy.call.callerNumber);
            expect(phoneNumberService.appendDomain).toHaveBeenCalledWith(dummy.call.callerNumber);
            expect(logger.log).toHaveBeenCalledWith(dummy.call.callerNumber + ' is getting called...');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(logger.log).toHaveBeenCalledWith('Active calls getting held...');
            expect(fcsService.holdCall).toHaveBeenCalledWith(dummy.call.id);

            expect(logger.log).toHaveBeenCalledWith('Call start to ' + dummy.call.callerNumber + ' successful');
            expect(callService.deleteOutgoingCall).toHaveBeenCalled();
            expect(dummy.call.getId).toHaveBeenCalled();
            expect(callService.addCall).toHaveBeenCalled();
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(contactService.searchSpidrContact).toHaveBeenCalled();
            expect(callService.setCallContactPhoto).toHaveBeenCalledWith(dummy.call.id, dummy.url);
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(dummy.call.onStateChange).toBeDefined();
            expect(dummy.call.onStreamAdded).toBeDefined();
            expect(dummy.call.onLocalStreamAdded).toBeDefined();
        });


        it('call onstatechange state:RINGING', function () {
            dummy.call.onStateChange(fcs.call.States.RINGING, '1');
            expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.RING_OUT);
            expect(rendererService.setStatus).toHaveBeenCalledWith('ringing');
        });

        it('call onstatechange state:ENDED', function () {
            dummy.call.onStateChange(fcs.call.States.ENDED, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('ended');
            jasmine.clock().tick(2001);
            expect(callService.endCall).toHaveBeenCalledWith(dummy.call.id);
            expect(fcsService.deleteFromCalls).toHaveBeenCalledWith(dummy.call.id);
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:REJECTED', function () {
            dummy.call.onStateChange(fcs.call.States.REJECTED, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('rejected');
            expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.BUSY);
            jasmine.clock().tick(2001);
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.BUSY);
            expect(callService.endCall).toHaveBeenCalledWith(dummy.call.id);
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:RENEGOTIATION', function () {
            dummy.call.onStateChange(fcs.call.States.RENEGOTIATION, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });
        it('call onstatechange state:TRANSFERRED', function () {
            dummy.call.onStateChange(fcs.call.States.TRANSFERRED, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:IN_CALL', function () {
            dummy.call.onStateChange(fcs.call.States.IN_CALL, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:ON_REMOTE_HOLD', function () {
            dummy.call.onStateChange(fcs.call.States.ON_REMOTE_HOLD, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('call held remotely');
        });

        it('call onstatechange state:OUTGOING', function () {
            dummy.call.onStateChange(fcs.call.States.OUTGOING, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('There is an outgoing call');
        });



        it('call onStreamAdded', function () {
            // onStreamAdded test
            expect(dummy.call.onStreamAdded).toBeDefined();
            $("body").append("<div id='remoteVideo' style='display:none;'></div><div id='localVideo'></div>");
            dummy.call.onStreamAdded(dummy.url);
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(dummy.call.getId).toHaveBeenCalled();
            expect($("#remoteVideo").attr("src")).toEqual(dummy.url);
            expect($("#remoteVideo").is(":visible")).toBeTruthy();
            expect($("#localVideo").is(":visible")).toBeTruthy();
            expect(logger.info).toHaveBeenCalled();
            $("#remoteVideo").remove();
            $("#localVideo").remove();
        });

        it('call onStreamAdded', function () {

            // onStreamAdded test
            expect(dummy.call.onLocalStreamAdded).toBeDefined();
            $("body").append("<div id='localVideo' style='display:none;'></div>");
            dummy.call.onLocalStreamAdded(dummy.url);
            expect($("#localVideo").attr("src")).toEqual(dummy.url);
            expect($("#localVideo").attr("muted")).toBeTruthy();
            expect($("#localVideo").is(":visible")).toBeTruthy();
            expect(logger.info).toHaveBeenCalled();
            $("#localVideo").remove();
        });

        it('startCall failed', function () {
            dummy.callSuccessCallback = false;
            fcsService.startCall();
            expect(logger.warn).toHaveBeenCalledWith('Call start to ' + dummy.call.callerNumber + ' failed: ' + dummy.error);
            expect(callService.deleteOutgoingCall).toHaveBeenCalled();
            expect(rendererService.setStatus).toHaveBeenCalledWith('call failed');
            expect(rendererService.renderCalls).toHaveBeenCalled();
            jasmine.clock().tick(2001);
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.BUSY);
        });
    });

    describe('check handleIncomingCall', function () {
        beforeEach(function () {
            spyOn(storageService, 'removeItem');
            spyOn(callService, 'endCall');
            spyOn(callService, 'getIncomingCalls').and.callFake(function () {
                return dummy.incomingCall;
            });
            spyOn(fcsService, 'answerAudioCall');
            spyOn(fcsService, 'answerVideoCall');
            spyOn(fcsService, 'declineCall');
            spyOn(fcsService, 'ignoreCall');
            spyOn(fcsService, 'deleteFromCalls');
            spyOn(fcs.call, 'getIncomingCallById').and.returnValue(dummy.call);
            spyOn(audioToneService, 'play');
            fcsService.handleIncomingCall();
        });

        it('success scenario', function () {
            expect(callService.getIncomingCalls).toHaveBeenCalled();
            expect(logger.info).toHaveBeenCalled();
            expect(fcs.call.getIncomingCallById).toHaveBeenCalledWith(dummy.call.id);
            expect(dummy.call.onStateChange).toBeDefined();
            expect(dummy.call.onStreamAdded).toBeDefined();
            expect(dummy.call.onLocalStreamAdded).toBeDefined();
            expect(storageService.removeItem).toHaveBeenCalledWith(dummy.call.id);
            expect(fcsService.answerAudioCall).toHaveBeenCalledWith(dummy.call.id);
            expect(rendererService.renderIncomingCalls).toHaveBeenCalled();
        });

        it('call onstatechange state:RINGING', function () {
            dummy.call.onStateChange(fcs.call.States.RINGING, '1');
            expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.RING_OUT);
            expect(rendererService.setStatus).toHaveBeenCalledWith('ringing');
        });

        it('call onstatechange state:ENDED', function () {
            dummy.call.onStateChange(fcs.call.States.ENDED, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('ended');
            jasmine.clock().tick(2001);
            expect(callService.endCall).toHaveBeenCalledWith(dummy.call.id);
            expect(fcsService.deleteFromCalls).toHaveBeenCalledWith(dummy.call.id);
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:REJECTED', function () {
            dummy.call.onStateChange(fcs.call.States.REJECTED, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('rejected');
            expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.BUSY);
            jasmine.clock().tick(2001);
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.BUSY);
            expect(callService.endCall).toHaveBeenCalledWith(dummy.call.id);
            expect(rendererService.renderCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:RENEGOTIATION', function () {
            dummy.call.onStateChange(fcs.call.States.RENEGOTIATION, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });
        it('call onstatechange state:TRANSFERRED', function () {
            dummy.call.onStateChange(fcs.call.States.TRANSFERRED, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:IN_CALL', function () {
            dummy.call.onStateChange(fcs.call.States.IN_CALL, '1');
            expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
            expect(rendererService.setStatus).toHaveBeenCalledWith('in call');
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(timerService.setCallTimer).toHaveBeenCalled();
        });

        it('call onstatechange state:ON_REMOTE_HOLD', function () {
            dummy.call.onStateChange(fcs.call.States.ON_REMOTE_HOLD, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('call held remotely');
        });

        it('call onstatechange state:OUTGOING', function () {
            dummy.call.onStateChange(fcs.call.States.OUTGOING, '1');
            expect(rendererService.setStatus).toHaveBeenCalledWith('There is an outgoing call');
        });

        it('call onStreamAdded', function () {
            expect(dummy.call.onStreamAdded).toBeDefined();
            $("body").append("<div id='remoteVideo' style='display:none;'></div><div id='localVideo'></div>");
            dummy.call.onStreamAdded(dummy.url);
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(dummy.call.getId).toHaveBeenCalled();
            expect($("#remoteVideo").attr("src")).toEqual(dummy.url);
            expect($("#remoteVideo").is(":visible")).toBeTruthy();
            expect($("#localVideo").is(":visible")).toBeTruthy();
            expect(logger.info).toHaveBeenCalled();
            $("#remoteVideo").remove();
            $("#localVideo").remove();
        });

        it('call onLocalStreamAdded', function () {
            expect(dummy.call.onLocalStreamAdded).toBeDefined();
            $("body").append("<div id='localVideo' style='display:none;'></div>");
            dummy.call.onLocalStreamAdded(dummy.url);
            expect($("#localVideo").attr("src")).toEqual(dummy.url);
            expect($("#localVideo").attr("muted")).toBeTruthy();
            expect($("#localVideo").is(":visible")).toBeTruthy();
            expect(logger.info).toHaveBeenCalled();
            $("#localVideo").remove();
        });


        it('command:answer_video', function () {
            resetDummy();
            dummy.incomingCall[dummy.call.id].command = 'answer_video';
            fcsService.handleIncomingCall();
            expect(fcsService.answerVideoCall).toHaveBeenCalledWith(dummy.call.id);
        });
        it('command:decline', function () {
            resetDummy();
            dummy.incomingCall[dummy.call.id].command = 'decline';
            fcsService.handleIncomingCall();
            expect(fcsService.declineCall).toHaveBeenCalledWith(dummy.call.id);
        });
        it('command:ignore', function () {
            resetDummy();
            dummy.incomingCall[dummy.call.id].command = 'ignore';
            fcsService.handleIncomingCall();
            expect(fcsService.ignoreCall).toHaveBeenCalledWith(dummy.call.id);
        });
    });

    it('check answerAudioCall', function () {
        spyOn(callService, 'getIncomingCalls').and.callFake(function () {
            return dummy.incomingCall;
        });
        spyOn(callService, 'deleteIncomingCall');
        spyOn(callService, 'addCall');
        spyOn(dummy.call, 'answer').and.callFake(dummy.callBacks);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.answerAudioCall(dummy.call.id);
        expect(logger.log).toHaveBeenCalledWith('Answering call from ' + dummy.call.callerNumber + '...');
        expect(dummy.call.getId).toHaveBeenCalled();
        expect(callService.getIncomingCalls).toHaveBeenCalled();

        expect(callService.getActiveCall).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Active calls getting held...');
        expect(callService.addCall).toHaveBeenCalled();
        expect(dummy.call.answer).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Call answered successfully');
        expect(callService.deleteIncomingCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.answerAudioCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call answer failed', 2000);
        expect(logger.warn).toHaveBeenCalledWith('Call answer failed: ' + dummy.error);
    });

    it('check answerVideoCall', function () {
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.answerVideoCall(dummy.call.id);
    });

    it('check endCall', function () {
        spyOn(callService, 'endCall');
        spyOn(callService, 'isActiveCall').and.returnValue(true);
        spyOn(dummy.call, 'end').and.callFake(dummy.callBacks);
        spyOn(fcsService, 'deleteFromCalls');
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.endCall(dummy.call.id);
        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_OUT);
        expect(dummy.call.end).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Call ended!');
        expect(callService.isActiveCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.setStatus).toHaveBeenCalledWith('call ended');
        expect(callService.endCall).toHaveBeenCalledWith(dummy.call.id);
        expect(fcsService.deleteFromCalls).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).not.toHaveBeenCalled();
        jasmine.clock().tick(1001);
        expect(rendererService.renderCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.endCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call could not be ended', 2000);
        expect(logger.warn).toHaveBeenCalledWith('Call could not be ended! ' + dummy.error);
    });

    it('check holdCall', function () {
        spyOn(callService, 'holdCall');
        spyOn(dummy.call, 'hold').and.callFake(dummy.callBacks);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.holdCall(dummy.call.id);
        expect(logger.log).toHaveBeenCalledWith('Holding call ' + dummy.call.callerNumber + '...');
        expect(dummy.call.hold).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Call held ' + dummy.call.callerNumber);
        expect(callService.holdCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.holdCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call could not be held ' + dummy.call.callerNumber, 2000);
        expect(logger.warn).toHaveBeenCalledWith('Call could not be held! ' + dummy.error);
    });

    it('check unholdCall', function () {
        spyOn(fcsService, 'holdCall');
        spyOn(callService, 'unholdCall');
        spyOn(dummy.call, 'unhold').and.callFake(dummy.callBacks);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.unholdCall(dummy.call.id);
        expect(logger.log).toHaveBeenCalledWith('Unholding call ' + dummy.call.callerNumber + '...');
        expect(logger.log).toHaveBeenCalledWith('Active calls getting held...');
        expect(fcsService.holdCall).toHaveBeenCalledWith(dummy.call.id);
        expect(dummy.call.unhold).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Call unheld ' + dummy.call.callerNumber);
        expect(callService.unholdCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.unholdCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call could not be retrieved' + dummy.call.callerNumber, 2000);
        expect(logger.warn).toHaveBeenCalledWith('Call could not be retrieved! ' + dummy.error);
    });

    it('check muteCall', function () {
        spyOn(callService, 'muteCall');
        spyOn(dummy.call, 'mute');
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.muteCall(dummy.call.id);
        expect(dummy.call.mute).toHaveBeenCalled();
        expect(callService.muteCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();
    });

    it('check unmuteCall', function () {
        spyOn(callService, 'unmuteCall');
        spyOn(dummy.call, 'unmute');
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.unmuteCall(dummy.call.id);
        expect(dummy.call.unmute).toHaveBeenCalled();
        expect(callService.unmuteCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();
    });

    it('check ignoreCall', function () {
        spyOn(callService, 'ignoreIncomingCall');
        spyOn(callService, 'deleteIncomingCall');
        spyOn(dummy.call, 'ignore').and.callFake(dummy.callBacks);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.ignoreCall(dummy.call.id);
        expect(dummy.call.ignore).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Ignore call successful');
        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
        expect(callService.ignoreIncomingCall).toHaveBeenCalledWith(dummy.call.id);
        expect(callService.deleteIncomingCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.ignoreCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call could not be ignored', 2000);
        expect(logger.warn).toHaveBeenCalledWith('Ignore call failed');
    });

    it('check declineCall', function () {
        spyOn(callService, 'declineIncomingCall');
        spyOn(callService, 'deleteIncomingCall');
        spyOn(dummy.call, 'reject').and.callFake(dummy.callBacks);
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.declineCall(dummy.call.id);
        expect(dummy.call.reject).toHaveBeenCalled();
        expect(logger.log).toHaveBeenCalledWith('Decline call successful');
        expect(callService.declineIncomingCall).toHaveBeenCalledWith(dummy.call.id);
        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
        expect(callService.deleteIncomingCall).toHaveBeenCalledWith(dummy.call.id);
        expect(rendererService.renderCalls).toHaveBeenCalled();
        expect(rendererService.renderIncomingCalls).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.declineCall(dummy.call.id);
        expect(message.error).toHaveBeenCalledWith('Call could not be rejected', 2000);
        expect(logger.warn).toHaveBeenCalledWith('Decline call failed');
    });

    it('check sendDTMF', function () {
        spyOn(dtmfService, 'sendDTMF');
        fcsService.addtoCalls(dummy.call.id, dummy.call);
        fcsService.sendDTMF(dummy.call.id, 'tone');
        expect(dtmfService.sendDTMF).toHaveBeenCalled();
    });

    it('check searchDirectory', function () {
        spyOn(fcs.addressbook, 'searchDirectory').and.callFake(function (criteria, username, success, error) {
            if (dummy.callSuccessCallback)
                success();
            else
                error(dummy.error);
        });
        fcsService.searchDirectory('criteria', dummy.functionVoid());
        expect(fcs.addressbook.searchDirectory).toHaveBeenCalled();
        expect(dummy.functionVoid).toHaveBeenCalled();

        dummy.callSuccessCallback = false;
        fcsService.searchDirectory('criteria', dummy.functionVoid);
        expect(fcs.addressbook.searchDirectory).toHaveBeenCalled();
        expect(message.error).toHaveBeenCalledWith('Search contact failed', 2000);
        expect(logger.warn).toHaveBeenCalledWith('Search contact failed :' + dummy.error);
    });

    it('check retrieveCalllog', function () {
        spyOn(fcs.calllog, 'retrieve');
        spyOn(fcsService, 'setUserAuth');
        spyOn(fcsService, 'setup');
        fcsService.retrieveCalllog();
        expect(fcsService.setUserAuth).toHaveBeenCalled();
        expect(fcsService.setup).toHaveBeenCalled();
        expect(fcs.calllog.retrieve).toHaveBeenCalled();
    });
});
