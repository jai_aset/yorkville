/* global audioToneService, expect, dummy */

describe('https-server.test.audioToneServiceTest', function () {
    var dummyTone;
    beforeEach(function () {
        resetDummy();
    });

    describe('check play', function () {
        beforeEach(function () {
            dummyTone = {
                readyState: 1,
                play: dummy.functionVoid,
                Play: dummy.functionVoid
            };
            audioToneService.useAudioTag = false;
            spyOn(dummyTone, 'play');
            spyOn(dummyTone, 'Play');
        });
        it('if no tone', function () {
            audioToneService.play();
            expect(dummyTone.play).not.toHaveBeenCalled();
            expect(dummyTone.Play).not.toHaveBeenCalled();
        });
        it('if tone with readyState is 0', function () {
            dummyTone.readyState = 0;
            audioToneService.play(dummyTone);
            expect(dummyTone.play).not.toHaveBeenCalled();
            expect(dummyTone.Play).not.toHaveBeenCalled();
        });
        it('if tone without useAudioTag', function () {
            audioToneService.play(dummyTone);
            expect(dummyTone.play).not.toHaveBeenCalled();
            expect(dummyTone.Play).toHaveBeenCalled();
        });
        it('if tone with useAudioTag', function () {
            audioToneService.useAudioTag = true;
            audioToneService.play(dummyTone);
            expect(dummyTone.play).toHaveBeenCalled();
            expect(dummyTone.Play).not.toHaveBeenCalled();
        });
    });

    describe('check stop', function () {
        beforeEach(function () {
            dummyTone = {
                readyState: 1,
                pause: dummy.functionVoid,
                StopPlay: dummy.functionVoid
            };
            audioToneService.useAudioTag = false;
            spyOn(dummyTone, 'pause');
            spyOn(dummyTone, 'StopPlay');
        });
        it('if no tone', function () {
            audioToneService.stop();
            expect(dummyTone.pause).not.toHaveBeenCalled();
            expect(dummyTone.StopPlay).not.toHaveBeenCalled();
        });
        it('if tone with readyState is 0', function () {
            dummyTone.readyState = 0;
            audioToneService.stop(dummyTone);
            expect(dummyTone.pause).not.toHaveBeenCalled();
            expect(dummyTone.StopPlay).not.toHaveBeenCalled();
        });
        it('if tone without useAudioTag', function () {
            audioToneService.stop(dummyTone);
            expect(dummyTone.pause).not.toHaveBeenCalled();
            expect(dummyTone.StopPlay).toHaveBeenCalled();
        });
        it('if tone with useAudioTag', function () {
            audioToneService.useAudioTag = true;
            audioToneService.stop(dummyTone);
            expect(dummyTone.pause).toHaveBeenCalled();
            expect(dummyTone.StopPlay).not.toHaveBeenCalled();
        });
    });

    describe('check init', function () {
        it('if no window.HTMLAudioElement', function () {
            window.HTMLAudioElement = undefined;
            audioToneService.init();
            expect(audioToneService.useAudioTag).toBeFalsy();
            expect($(audioToneService.RING_IN).is('object')).toBeTruthy();
            expect($(audioToneService.RING_OUT).is('object')).toBeTruthy();
            expect($(audioToneService.BUSY).is('object')).toBeTruthy();
        });
        it('with window.HTMLAudioElement', function () {
            window.HTMLAudioElement = 1;
            audioToneService.init();
            expect(audioToneService.useAudioTag).toBeTruthy();
            expect($(audioToneService.RING_IN).is('audio')).toBeTruthy();
            expect($(audioToneService.RING_OUT).is('audio')).toBeTruthy();
            expect($(audioToneService.BUSY).is('audio')).toBeTruthy();
        });
    });

});
