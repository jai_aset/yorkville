/* global noteService, expect, dummy, spyOn, callService, sforce, message */

describe('https-server.test.noteServiceTest', function () {
    beforeEach(function () {
        resetDummy();
        $('body').append('<div id="popup"><div class="add_note"></div></div>');
        $('body').append('<div id="add_note_dialog"><textarea id="note"></textarea></div>');
        jasmine.clock().install();
        noteService.openDialog();
    });

    afterEach(function () {
        $('#add_note_dialog').remove();
        $('#popup').remove();
        jasmine.clock().uninstall();
    });

    it('check openDialog', function () {
        expect($('textarea#note').val()).toEqual('');
        expect($('#popup').find('.add_note').hasClass('add_note_active')).toBeTruthy();
    });

    it('check closeDialog', function () {
        $('#add_note_dialog').dialog("close");
        expect($('#add_note_dialog').dialog("isOpen")).toBeFalsy();
    });

    describe('check addNote', function () {
        var upsert = true;
        beforeEach(function () {
            spyOn(callService, 'getActiveCall').and.returnValue(dummy.call);
            spyOn(sforce, 'SObject').and.returnValue({});
            spyOn(message, 'info');
            spyOn(message, 'warning');
            spyOn(sforce.connection, 'upsert').and.returnValue(
                    [
                        {
                            getBoolean: function () {
                                return upsert;
                            }
                        }
                    ]
                    );
        });
        it('cancel', function () {
            $('#popup').find('.add_note').addClass('add_note_active');
            var buttons = $('#add_note_dialog').dialog('option', 'buttons');
            buttons['Cancel']();

            expect($('#add_note_dialog').dialog("isOpen")).toBeFalsy();
        });
        it('without text', function () {
            var buttons = $('#add_note_dialog').dialog('option', 'buttons');
            buttons['Add']();
            expect(message.warning).toHaveBeenCalledWith('Please add a new note', 1500);
        });
        it('success', function () {
            $('#note').val(dummy.text);

            var buttons = $('#add_note_dialog').dialog('option', 'buttons');
            buttons['Add']();

            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(sforce.SObject).toHaveBeenCalledWith('Note');
            expect(message.info).toHaveBeenCalledWith('Note is successfully saved', 1500);

            jasmine.clock().tick(1501);
            expect($('#popup').find('.add_note').hasClass('add_note_active')).toBeFalsy();
        });

        it('upsert failed', function () {
            $('#note').val(dummy.text);
            dummy.call.type = 'in';
            upsert = false;

            var buttons = $('#add_note_dialog').dialog('option', 'buttons');
            buttons['Add']();

            expect(message.warning).toHaveBeenCalledWith('Please add this user in contacts', 1500);
        });
    });

});
