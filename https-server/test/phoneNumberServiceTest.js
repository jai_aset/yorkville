/* global phoneNumberService, expect, dummy, spyOn, fcs */

describe('https-server.test.phoneNumberServiceTest', function () {
    beforeEach(function () {
        resetDummy();
    });


    describe('check standardizePhoneNumber for', function () {
        it('empty string', function () {
            var number = phoneNumberService.standardizePhoneNumber('');
            expect(number.forCall).toEqual('');
            expect(number.forDisplay).toEqual('');
            expect(number.forCompare).toEqual('');
        });
        it('email', function () {
            var number = phoneNumberService.standardizePhoneNumber(dummy.email);
            expect(number.forCall).toEqual(dummy.email);
            expect(number.forDisplay).toEqual(dummy.email);
            expect(number.forCompare).toEqual(dummy.email);
        });
        it('+0123456789', function () {
            var number = phoneNumberService.standardizePhoneNumber('+0123456789');
            expect(number.forCall).toEqual('+0123456789');
            expect(number.forDisplay).toEqual('+0123456789');
            expect(number.forCompare).toEqual('+0123456789');
        });
        it('+0-123-456-789', function () {
            var number = phoneNumberService.standardizePhoneNumber('+0123456789');
            expect(number.forCall).toEqual('+0123456789');
            expect(number.forDisplay).toEqual('+0123456789');
            expect(number.forCompare).toEqual('+0123456789');
        });
        it('00123456789', function () {
            var number = phoneNumberService.standardizePhoneNumber('00123456789');
            expect(number.forCall).toEqual('00123456789');
            expect(number.forDisplay).toEqual('+123456789');
            expect(number.forCompare).toEqual('00123456789');
        });
        it('service number *25#', function () {
            var number = phoneNumberService.standardizePhoneNumber('*25#');
            expect(number.forCall).toEqual('*25#');
            expect(number.forDisplay).toEqual('*25#');
            expect(number.forCompare).toEqual('*25#');
        });
    });
    it('check appendDomain', function () {
        spyOn(fcs,'getDomain').and.returnValue('domain');
        expect(phoneNumberService.appendDomain('')).toEqual('@domain');
        expect(phoneNumberService.appendDomain(dummy.email)).toEqual(dummy.email);
    });



});
