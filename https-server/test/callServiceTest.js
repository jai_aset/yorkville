/* global spyOn, expect, callService, storageService, dummy */

describe('https-server.test.callServiceTest', function () {
    beforeEach(function () {
        resetDummy();
    });
    afterEach(function () {
        localStorage.clear();
    });

    it('check hasOutgoingCall', function () {
        expect(callService.hasOutgoingCall()).toBeFalsy();
        localStorage.setItem('outgoing_call', JSON.stringify(dummy.outgoingCall));
        expect(callService.hasOutgoingCall()).toBeTruthy();
    });

    it('check getOutgoingCall', function () {
        spyOn(storageService, 'getItemAsJSON');
        callService.getOutgoingCall();
        expect(storageService.getItemAsJSON).toHaveBeenCalledWith('outgoing_call');
    });

    it('check addOutgoingCall', function () {
        spyOn(storageService, 'setItemFromJSON');
        callService.addOutgoingCall(dummy.callerName, dummy.number, dummy.sf_contact_id);
        expect(storageService.setItemFromJSON).toHaveBeenCalled(); 
    });

    it('check deleteOutgoingCall', function () {
        spyOn(storageService, 'removeItem');
        callService.deleteOutgoingCall();
        expect(storageService.removeItem).toHaveBeenCalledWith('outgoing_call');
    });

    it('check hasIncomingCall', function () {
        expect(callService.hasIncomingCall()).toBeFalsy();
        localStorage.setItem('incoming_call', JSON.stringify(dummy.incomingCall));
        expect(callService.hasIncomingCall()).toBeTruthy();
    });

    it('check getIncomingCalls', function () {
        spyOn(storageService, 'getItemAsJSON');
        callService.getIncomingCalls();
        expect(storageService.getItemAsJSON).toHaveBeenCalledWith('incoming_call');
    });

    it('check addIncomingCall', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getIncomingCalls').and.returnValue({});
        callService.addIncomingCall({"id": 1});
        expect(callService.getIncomingCalls).toHaveBeenCalled();
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('incoming_call', {"1": {"id": 1}});
    });

    it('check deleteIncomingCall', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getIncomingCalls').and.returnValue({"1": {}});
        callService.deleteIncomingCall(1);
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('incoming_call', {});
    });

    it('check setIncomingCallCommand', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getIncomingCalls').and.returnValue({"1": {}});
        callService.setIncomingCallCommand(1, 'command');
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('incoming_call', {1: {command: 'command'}});
    });

    it('check setIncomingCallSFContact', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getIncomingCalls').and.returnValue({"1": {}});
        callService.setIncomingCallSFContact(1, 'name', 2);
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('incoming_call', {1: {contact: 'name', sf_contact_id: 2}});
    });

    it('check setIncomingCallContactPhoto', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getIncomingCalls').and.returnValue({"1": {}});
        callService.setIncomingCallContactPhoto(1, dummy.url);
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('incoming_call', {1: {contactPhoto: dummy.url}});
    });

    it('check answerIncomingCall', function () {
        spyOn(callService, 'setIncomingCallCommand');
        callService.answerIncomingCall(1, false);
        expect(callService.setIncomingCallCommand).toHaveBeenCalledWith(1, 'answer');
        callService.answerIncomingCall(1, true);
        expect(callService.setIncomingCallCommand).toHaveBeenCalledWith(1, 'answer_video');
    });

    it('check declineIncomingCall', function () {
        spyOn(callService, 'setIncomingCallCommand');
        callService.declineIncomingCall(1);
        expect(callService.setIncomingCallCommand).toHaveBeenCalledWith(1, 'decline');
    });

    it('check ignoreIncomingCall', function () {
        spyOn(callService, 'setIncomingCallCommand');
        callService.ignoreIncomingCall(1);
        expect(callService.setIncomingCallCommand).toHaveBeenCalledWith(1, 'ignore');
    });

    it('check setCallParam', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getCalls').and.returnValue({"1": {}});
        callService.setCallParam(1, 'param', 'value');
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('calls', {1: {'param': 'value'}});
    });

    it('check hasCall', function () {
        expect(callService.hasCall()).toBeFalsy();
        localStorage.setItem('calls', '{"1":{}}');
        expect(callService.hasCall()).toBeTruthy();
    });

    it('check getCalls', function () {
        spyOn(storageService, 'getItemAsJSON');
        callService.getCalls();
        expect(storageService.getItemAsJSON).toHaveBeenCalledWith('calls');
    });

    it('check addCall', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getCalls').and.returnValue({});
        callService.addCall({"id": 1});
        expect(callService.getCalls).toHaveBeenCalled();
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('calls', {"1": {"id": 1}});
    });

    it('check endCall', function () {
        spyOn(storageService, 'setItemFromJSON');
        spyOn(callService, 'getCalls').and.returnValue({"1": {}});
        callService.endCall(1);
        expect(storageService.setItemFromJSON).toHaveBeenCalledWith('calls', {});
    });

    it('check holdCall', function () {
        spyOn(callService, 'setCallParam');
        callService.holdCall(1);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'status', 'hold');
    });

    it('check unholdCall', function () {
        spyOn(callService, 'setCallParam');
        callService.unholdCall(1);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'status', 'active');
    });

    it('check muteCall', function () {
        spyOn(callService, 'setCallParam');
        callService.muteCall(1);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'mute', 1);
    });

    it('check unmuteCall', function () {
        spyOn(callService, 'setCallParam');
        callService.unmuteCall(1);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'mute', 0);
    });

    it('check setCallStartTime', function () {
        spyOn(callService, 'setCallParam');
        spyOn(callService, 'getCalls').and.returnValue({"1": {}});
        callService.setCallStartTime(1);
        expect(callService.setCallParam).toHaveBeenCalled();
    });

    it('check setCallSFContact', function () {
        spyOn(callService, 'setCallParam');
        callService.setCallSFContact(1, 2);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'sf_contact_id', 2);
    });

    it('check setCallContactPhoto', function () {
        spyOn(callService, 'setCallParam');
        callService.setCallContactPhoto(1, dummy.url);
        expect(callService.setCallParam).toHaveBeenCalledWith(1, 'contactPhoto', dummy.url);
    });

    it('check getActiveCall', function () {
        spyOn(callService, 'getCalls').and.returnValue({"1": {id: 1, status: 'active'}, "2": {id: 2, status: 'hold'}});
        var call = callService.getActiveCall();
        expect(call.id).toEqual(1);
    });

    it('check isActiveCall', function () {
        spyOn(callService, 'getCalls').and.returnValue({1: {id: 1, status: 'active'}, 2: {id: 2, status: 'hold'}});
        expect(callService.isActiveCall("1")).toBeTruthy();
        expect(callService.isActiveCall("2")).toBeFalsy();
    });

    it('check getInactiveCalls', function () {
        spyOn(callService, 'getCalls').and.returnValue({"1": {id: 1, status: 'active'}, "2": {id: 2, status: 'hold'}});
        var calls = callService.getInactiveCalls();
        expect(calls[1]).toBeUndefined();
        expect(calls[2]).toBeDefined();
    });

    it('check getInactiveCalls without any inactive', function () {
        spyOn(callService, 'getCalls').and.returnValue({"1": {id: 1, status: 'active'}});
        var calls = callService.getInactiveCalls();
        expect(calls).toBeUndefined();
    });

    it('check trigger.endCall', function () {
        spyOn(storageService, 'setItem');
        callService.trigger.endCall(1);
        expect(storageService.setItem).toHaveBeenCalledWith('end_call', 1);
    });

    it('check trigger.holdCall', function () {
        spyOn(storageService, 'setItem');
        callService.trigger.holdCall(1);
        expect(storageService.setItem).toHaveBeenCalledWith('hold_call', 1);
    });

    it('check trigger.unholdCall', function () {
        spyOn(storageService, 'setItem');
        callService.trigger.unholdCall(1);
        expect(storageService.setItem).toHaveBeenCalledWith('unhold_call', 1);
    });

    it('check trigger.muteCall', function () {
        spyOn(storageService, 'setItem');
        callService.trigger.muteCall(1);
        expect(storageService.setItem).toHaveBeenCalledWith('mute_call', 1);
    });

    it('check trigger.unmuteCall', function () {
        spyOn(storageService, 'setItem');
        callService.trigger.unmuteCall(1);
        expect(storageService.setItem).toHaveBeenCalledWith('unmute_call', 1);
    });


});
