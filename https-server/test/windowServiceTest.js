/* global spyOn, storageService, windowService, expect, dummy, widget */

describe('https-server.test.windowServiceTest', function () {

    beforeEach(function () {
        resetDummy();
        spyOn(storageService, 'removeItem');
        spyOn(widget, 'subscribe');
        spyOn(window, 'open').and.returnValue({focus: dummy.functionVoid});
        delete window.id;
        clearInterval(window.checkMasterAliveInterval);
    });

    afterEach(function () {
        localStorage.clear();
    });

    it('check createTabId', function () {
        spyOn(storageService, 'setItem');
        jasmine.clock().install();
        window.__currentContactId = 1;
        delete window.id;

        windowService.createTabId();
        expect(window.id).not.toBeNull();
        expect(storageService.setItem).toHaveBeenCalled();

        jasmine.clock().tick(2001);
        expect(storageService.setItem).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('check removeTabId', function () {
        window.id = 1;
        windowService.removeTabId();
        expect(storageService.removeItem).toHaveBeenCalledWith(windowService.getPrefix() + window.id);
    });

    it('check getNumberOfTabs', function () {
        window.id = 1;
        expect(windowService.getNumberOfTabs()).toEqual(0);
        storageService.setItem(windowService.getPrefix() + window.id, (new Date()).getTime());
        expect(windowService.getNumberOfTabs()).toEqual(1);
    });

    it('check amIMasterTab', function () {
        window.id = 3;
        storageService.setItem(windowService.getPrefix() + window.id, '{"id":' + window.id + '}');
        expect(windowService.amIMasterTab()).toBeTruthy();
        window.id = 5;
        expect(windowService.amIMasterTab()).toBeFalsy();
    });

    it('check openPopup and getPopup', function () {
        expect(windowService.getPopup()).toBe(undefined);
        expect(window.popupWin).toBeNull;
        windowService.openPopup();
        expect(window.popupWin).not.toBeNull();
        windowService.openPopup();
        expect(windowService.getPopup()).not.toBe(undefined);
    });

    it('check openContactPage', function () {
        windowService.openContactPage(1);
        expect(window.open).toHaveBeenCalledWith('/1', '_blank');
    });

    it('check checkPopupBlocker', function () {
        expect(windowService.checkPopupBlocker()).toBeTruthy();
        expect(window.open).toHaveBeenCalled();
    });

    it('check isPopupAlive', function () {
        storageService.setItem("popup_opened", (new Date()).getTime());
        windowService.isPopupAlive();
        expect(windowService.isPopupAlive()).toBeTruthy();
    });

});
