/* global spyOn, timerService, storageService, fcsService, windowService, contactService, widget, rendererService, callService, expect, audioToneService, dummy */

describe('https-server.test.rendererServiceTest', function () {

    var dummyCall;

    beforeEach(function () {
        resetDummy();
        dummyCall = {
            id: "1",
            type: "out",
            contact: "user1",
            number: "num1",
            status: "active",
            mute: 1,
            start_time: 1453445670296,
            sf_contact_id: dummy.sf_contact_id,
            contactPhoto: dummy.url
        };
        window.i_am_widget = false;
        window.i_am_popup = false;
        spyOn(fcsService, 'isConnected').and.returnValue(true);
        spyOn(timerService, 'setCallTimer');
        spyOn(timerService, 'stopCallTimer');
        spyOn(storageService, 'getItem');
        spyOn(storageService, 'setItem');
        spyOn(windowService, 'openContactPage');
        spyOn(windowService, 'openPopup');
        spyOn(contactService, 'setContactTitle');
        spyOn(widget, 'enableTab');
        spyOn(widget, 'disableTab');
        spyOn(window, 'resizeTo');
        spyOn(window, 'setTimeout');
        spyOn($.fn, "getNiceScroll");
        spyOn($.fn, "niceScroll");
    });
    afterEach(function () {
        localStorage.clear();
        $("#active_call").remove();
        $("#modal").remove();
        delete window.i_am_widget;
        delete window.i_am_popup;
    });

    describe('check renderCalls if window is', function () {
        beforeEach(function () {
            window.i_am_widget = false;
            window.i_am_popup = false;
            $("body").append('<div class="container" id="active_call">' +
                    '<div><span id="timer">00:00</span></div>' +
                    '<div id="contact"></div>' +
                    '<div id="status"></div>' +
                    '<div id="buttons">' +
                    '<table style="width: 100%">' +
                    '<tr>' +
                    '<td width="34%"><span class="end_call" title="End call"></span></td>' +
                    '<td width="33%"><span class="bordered mute_call" title="Mute"></span></td>' +
                    '<td width="33%"><span class="bordered hold_call" title="Hold"></span></td>' +
                    '</tr>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '<div id="inactive_calls"></div>');

            spyOn(rendererService, 'setStatus');
            spyOn(callService, 'getActiveCall').and.returnValue(dummyCall);
            spyOn(callService, 'getInactiveCalls').and.returnValue([dummyCall]);

        });
        it('widget', function () {
            window.i_am_widget = true;
            rendererService.renderCalls();
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(callService.getInactiveCalls).toHaveBeenCalled();
            expect($("#active_call").is(":visible")).toBeTruthy();
            expect(timerService.setCallTimer).toHaveBeenCalledWith(dummyCall.start_time);
            expect($("#active_call").find("#contact").html()).toEqual('<a id="contact_redirect" href="javascript:">' + dummyCall.contact + '</a>');
            expect($("#active_call").find("#contact").attr('title')).toEqual(dummyCall.number);
            expect(storageService.getItem).toHaveBeenCalledWith('call_status');
            expect(rendererService.setStatus).toHaveBeenCalled();
            expect($("#active_call").find(".mute_call").hasClass('mute_call_active')).toBeTruthy();
            expect($("#active_call").find(".mute_call").attr('title')).toEqual('Unmute');
            expect($("#inactive_calls").is(":visible")).toBeTruthy();
            expect($("#inactive_calls").find(".container").length).toEqual(1);
            expect(widget.enableTab).toHaveBeenCalledWith('active_call', true);

            $("#active_call").find("#contact a").click();
            expect(windowService.openContactPage).toHaveBeenCalledWith(dummyCall.sf_contact_id);
        });

        it('popup', function () {
            window.i_am_popup = true;
            dummyCall.mute = 0;
            rendererService.renderCalls();
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(callService.getInactiveCalls).toHaveBeenCalled();
            expect($("#active_call").is(":visible")).toBeTruthy();
            expect(timerService.setCallTimer).toHaveBeenCalledWith(dummyCall.start_time);
            expect(contactService.setContactTitle).toHaveBeenCalledWith(dummyCall.number, dummyCall.contact);
            expect($("#active_call").find(".mute_call").hasClass('mute_call_active')).toBeFalsy();
            expect($("#active_call").find(".mute_call").attr('title')).toEqual('Mute');
            expect($("#inactive_calls").is(":visible")).toBeTruthy();
            expect($("#inactive_calls").find(".container").length).toEqual(1);
            expect(window.resizeTo).toHaveBeenCalled();
        });

    });

    describe('check renderCalls if there is no active call and window is', function () {
        beforeEach(function () {
            window.i_am_widget = false;
            window.i_am_popup = false;
            spyOn(callService, 'getActiveCall').and.returnValue(null);
            spyOn(callService, 'getInactiveCalls').and.returnValue(null);

        });
        it('widget', function () {
            window.i_am_widget = true;
            rendererService.renderCalls();
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(callService.getInactiveCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
        });
        it('popup', function () {
            window.i_am_popup = true;
            rendererService.renderCalls();
            expect(callService.getActiveCall).toHaveBeenCalled();
            expect(callService.getInactiveCalls).toHaveBeenCalled();
            expect(timerService.stopCallTimer).toHaveBeenCalled();
            expect(window.setTimeout).toHaveBeenCalled();
        });
    });


    describe('check renderIncomingcalls if window is', function () {
        beforeEach(function () {
            window.i_am_widget = false;
            window.i_am_popup = false;
            spyOn(callService, 'hasIncomingCall').and.returnValue(true);
            spyOn(callService, 'getIncomingCalls').and.returnValue(
                    {
                        id: '1',
                        contact: 'user1',
                        number: 'number1',
                        canReject: true,
                        canReceiveRemoteVideo: true
                    });
            spyOn(audioToneService, 'play');
            spyOn(rendererService, 'templateEngine');
            spyOn(callService, 'answerIncomingCall');
            spyOn(callService, 'ignoreIncomingCall');
            spyOn(callService, 'declineIncomingCall');
            spyOn(fcsService, 'handleIncomingCall');
        });

        afterEach(function () {
            $("#modal").remove();
        });

        it('widget', function () {
            window.i_am_widget = true;

            rendererService.renderIncomingCalls();
            expect(callService.hasIncomingCall).toHaveBeenCalled();
            expect(callService.getIncomingCalls).toHaveBeenCalled();
            expect($("#modal").length).toEqual(1);

            $("#modal").append("<div class='incoming_call' call_id='" + dummyCall.id + "'>" +
                    "<div class='audio_answer'></div>" +
                    "<div class='video_answer'></div>" +
                    "<div class='ignore_call'></div>" +
                    "<div class='decline_call'></div>" +
                    "</div>");

            $("#modal").find(".audio_answer").click();
            expect(callService.answerIncomingCall).toHaveBeenCalledWith(dummyCall.id, false);
            expect(windowService.openPopup).toHaveBeenCalled();

            $("#modal").find(".video_answer").click();
            expect(callService.answerIncomingCall).toHaveBeenCalledWith(dummyCall.id, true);
            expect(windowService.openPopup).toHaveBeenCalled();

            $("#modal").find(".ignore_call").click();
            expect(callService.ignoreIncomingCall).toHaveBeenCalledWith(dummyCall.id);
            expect(windowService.openPopup).toHaveBeenCalled();

            $("#modal").find(".decline_call").click();
            expect(callService.declineIncomingCall).toHaveBeenCalledWith(dummyCall.id);
            expect(windowService.openPopup).toHaveBeenCalled();
        });

        it('popup', function () {
            window.i_am_popup = true;

            rendererService.renderIncomingCalls();
            expect(callService.hasIncomingCall).toHaveBeenCalled();
            expect(callService.getIncomingCalls).toHaveBeenCalled();
            expect(audioToneService.play).toHaveBeenCalledWith(audioToneService.RING_IN);
            expect($("#modal").length).toEqual(1);

            $("#modal").append("<div class='incoming_call' call_id='" + dummyCall.id + "'>" +
                    "<div class='audio_answer'></div>" +
                    "<div class='video_answer'></div>" +
                    "<div class='ignore_call'></div>" +
                    "<div class='decline_call'></div>" +
                    "</div>");

            $("#modal").find(".audio_answer").click();
            expect(callService.answerIncomingCall).toHaveBeenCalledWith(dummyCall.id, false);
            expect(fcsService.handleIncomingCall).toHaveBeenCalled();

            $("#modal").find(".video_answer").click();
            expect(callService.answerIncomingCall).toHaveBeenCalledWith(dummyCall.id, true);
            expect(fcsService.handleIncomingCall).toHaveBeenCalled();

            $("#modal").find(".ignore_call").click();
            expect(callService.ignoreIncomingCall).toHaveBeenCalledWith(dummyCall.id);
            expect(fcsService.handleIncomingCall).toHaveBeenCalled();

            $("#modal").find(".decline_call").click();
            expect(callService.declineIncomingCall).toHaveBeenCalledWith(dummyCall.id);
            expect(fcsService.handleIncomingCall).toHaveBeenCalled();
        });

    });

    it('check renderIncomingCalls if there is no incoming call and window is popup', function () {
        window.i_am_popup = true;
        spyOn(audioToneService, 'stop');
        spyOn(callService, 'hasIncomingCall').and.returnValue(false);
        rendererService.renderIncomingCalls();
        rendererService.renderIncomingCalls();
        expect(callService.hasIncomingCall).toHaveBeenCalled();
        expect(audioToneService.stop).toHaveBeenCalledWith(audioToneService.RING_IN);
    });

    it('check setStatus', function () {
        $("body").append('<div id="active_call"><div id="status"></div></div>');
        window.i_am_popup = true;
        rendererService.setStatus('state');
        expect($("#status").text()).toEqual('state');
        expect(storageService.setItem).toHaveBeenCalledWith('call_status', 'state');
    });

    it('check showModalMessage', function () {
        spyOn(rendererService, 'showModal');
        rendererService.showModalMessage('message');
        expect(rendererService.showModal).toHaveBeenCalledWith('<div id="modal_text">message</div>');
    });

    it('check templateEngine', function () {
        var template = '<a><%this.b%></a>';
        var result = rendererService.templateEngine(template, {b: 2});
        expect(result).toEqual('<a>2</a>');
    });

    it('check showModal', function () {
        spyOn(rendererService, 'hideModal');
        spyOn(rendererService, 'setNiceScroll');
        rendererService.showModal('html');
        expect(rendererService.hideModal).toHaveBeenCalled();
        expect($("#modal").length).toEqual(1);
        expect($("#modal").html()).toEqual('html');
        expect(rendererService.setNiceScroll).toHaveBeenCalled();
    });

    it('check hideModal', function () {
        $("body").append('<div id="modal"></div>');
        rendererService.hideModal();
        expect($("#modal").length).toEqual(0);
    });

    it('check setNiceScroll', function () {
        $("body").append('<div id="modal"></div>');
        rendererService.setNiceScroll($('#modal'));
    });

});
