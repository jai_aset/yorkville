/* global fcs */

var phoneNumberService = new function () {
    var self = this;

    //Convert a valid phone number to a standard format:
    //No non-numeric character except initial plus (+)
    //E.g. +1234567890
    this.standardizePhoneNumber = function (number) {
        var forCall = number.trim().replace(/\s+/g, ""),
                forDisplay = forCall, forCompare = forCall, checkedPhoneNumber;
        if (isPhoneNumberForService(forCall)) {
            forDisplay = forCall;
            forCompare = forCall;
        }
        else if (isPhoneNumberValid(forCall)) {
            forDisplay = forCall.replace(/\D+/g, "");
            checkedPhoneNumber = checkPlusAndDoubleZero({forCall: forCall, forDisplay: forDisplay, forCompare: forDisplay});
            forCall = checkedPhoneNumber.forCall;
            forDisplay = checkedPhoneNumber.forDisplay;
            forCompare = checkedPhoneNumber.forCompare;
        }
        return {forCall: forCall, forDisplay: forDisplay, forCompare: forCompare};
    };

    function checkPlusAndDoubleZero(phoneNumber) {
        var forCall = phoneNumber.forCall,
                forDisplay = phoneNumber.forDisplay,
                forCompare = phoneNumber.forCompare;
        if (forCall.substring(0, 2) === '00') {
            forCall = forDisplay;
            forDisplay = '+' + forDisplay.replace(/00/, "");
        }
        else if (forCall.substring(0, 1) === '+' || forCall.substring(0, 2) === '(+') {
            forDisplay = forCall.replace(/\D+/g, "");
            forCall = '+' + forDisplay;
            forDisplay = forCall;
        }
        else {
            forDisplay = forCall.replace(/\D+/g, "");
            forCall = forDisplay;
        }

        return {forCall: forCall, forDisplay: forDisplay, forCompare: forCompare};
    }

    //Check whether the number is possibly a phone number
    //Followings are some valid examples:
    //123456789
    //+123456789
    //+1 234-56-789
    //+1 (234) 56-789
    //(+1) 234 56-789
    //*25#
    function isPhoneNumberValid(number) {
        var newNumber = number.replace(/\s+/g, ""), isValid,
                phoneNumberRegex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
        isValid = phoneNumberRegex.test(newNumber) || isPhoneNumberForService(number);
        return isValid;
    }

    //Check whether the number is a service number
    //E.g. *25#, *123# etc.
    function isPhoneNumberForService(number) {
        var newNumber = number.trim(), isServiceNumber,
                serviceNumberRegex = /^\*\d+#$/;
        isServiceNumber = serviceNumberRegex.test(newNumber);
        return isServiceNumber;
    }

    this.appendDomain = function (number) {
        var userDomain,
                emailRegex = new RegExp('\\S+@\\S+\\.\\S+');

        if (!emailRegex.test(number)) {
            userDomain = fcs.getDomain();
            if (userDomain) {
                return number + '@' + userDomain;
            }
        }
        return number;
    };
}();