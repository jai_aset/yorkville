/* global SFService, listenerService, storageService, fcsService, sforce, callService, fcs, videoInvitationService, rendererService, contactService, logService, message, windowService, logger */

var widget = new function () {
    var sfdcOnload = window.onload,
            currentTab, prevTab = 'addressbook',
            self = this,
            searchContactTimer,
            appState;

    this.reload = function () {
        window.location.reload();
    };

    this.onload = function () {
        if (sfdcOnload) {
            sfdcOnload();
        }
        $("body").css("margin", 0).css("padding", 0);

        if (!windowService.isPopupAlive()) {
            storageService.clear();
        }

        //agreement check
        SFService.loadSFData('loadAgreement').done(function (agreement) {
            var agreed = SFService.parseSFResult(agreement);

            if (agreed === 'false') {
                setAppState('AGREEMENT');
            } else {
                SFService.loadSFData('checkLoginState').done(function (login) {
                    var loginState = SFService.parseSFResult(login);
                    if (loginState === 'false') {
                        setAppState('LOGIN');
                    } else {
                        if (windowService.checkPopupBlocker() === true) {
                            logger.warn('Popups blocked. You need unblock to receive calls!');
                            message.warning('Popups blocked. You need unblock to receive calls!');
                        }else{
                            setAppState('APP');                            
                        }
                    }
                });
            }
        });

        $('.tab').on('click', function () {
            self.setTab($(this).attr('id').substring(4));
        });

        $("#addressbook_search").keyup(function (e) {
            clearTimeout(searchContactTimer);
            var text = $(this).val();
            if (text.length > 0) {
                if (e.keyCode === 13) {
                    searchContact(text);
                } else {
                    searchContactTimer = setTimeout(function () {
                        searchContact(text);
                    }, 700);
                }
            } else {
                $("#contacts").empty();
            }
        });

        $('#contacts').on("click", ".name", function () {
            if ($(this).parents('.contact').hasClass("contact_expanded")) {
                $("#contacts").find('.contact_expanded').removeClass('contact_expanded');
            } else {
                $("#contacts").find('.contact_expanded').removeClass('contact_expanded');
                $(this).parents('.contact').addClass("contact_expanded");
                rendererService.setNiceScroll($("#contacts"));
            }
        });

        $('#tabs').on("click", ".phone_button", function () {
            var callNumber = $(this).find('.text').text(),
                    callContact = $(this).parents('.callable').find('.name').text();
             var entityId =  $(this).parents('.callable').find('.id').text();

            callService.addOutgoingCall(callContact, callNumber, entityId);
            windowService.openPopup();
        });

        $('#tabs').on("click", ".email_button", function () {
            var mailTo = $(this).find('.text').text();
            videoInvitationService.openDialog(mailTo);
        });


        $('#tab_content_active_call').on('click', '.end_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            callService.trigger.endCall(callId);
        });
        $('#tab_content_active_call').on('click', '.hold_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            callService.trigger.holdCall(callId);
        });
        $('#tab_content_active_call').on('click', '.unhold_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            callService.trigger.unholdCall(callId);
        });
        $('#tab_content_active_call').on('click', '.mute_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            if ($(this).hasClass('mute_call_active')) {
                callService.trigger.unmuteCall(callId);
            } else {
                callService.trigger.muteCall(callId);
            }
        });
        $('#tab_content_active_call').on('click', '.held_call_text', function () {
            windowService.openPopup();
        });
        $('#active_call').on('click', '#contact', function () {
            windowService.openPopup();
        });

        storageService.listen(listenerService.widgetListener);
        window.onbeforeunload = function () {
            self.unsubscribe();
        };
    };

    if (!window._testonly) {
        window.i_am_widget = true;
        window.onload = self.onload;
    }

    function setAppState(state) {
        $('.states').hide();
        appState = state;
        switch (state) {
            case 'AGREEMENT':
                $('#eula').attr('src', window.config.serverUrl + 'eula.html');
                $('#eula_container').show();
                $('#agree').on('click', function () {
                    var callback = {
                        onSuccess: function (result) {
                            setAppState('LOGIN');
                        },
                        onFailure: function (error) {
                        }
                    };
                    sforce.apex.execute("GenbandController", "setAgreement", {agreed: true}, callback);
                });
                break;
            case 'LOGIN':
                $('#login_container').show();
                $('#login_container').find('#login').on('click', function () {
                    var callback = {
                        onSuccess: function (result) {
                            self.reload();
                        },
                        onFailure: function (error) {
                        }
                    };
                    sforce.apex.execute("GenbandController", "setLoginState", {loginState: true}, callback);
                });
                break;
            case 'APP':
                setConnectionState('OFFLINE');
                $('#app_container').show();
                self.subscribe();
                $('#app_container').find('#logout').on('click', function () {
                    if (callService.hasCall()) {
                        message.warning('You have active calls. Please end them before logout!', 2000);
                    }
                    else {
                        var callback = {
                            onSuccess: function (result) {
                                self.unsubscribe();
                                self.reload();
                            },
                            onFailure: function (error) {
                            }
                        };
                        sforce.apex.execute("GenbandController", "setLoginState", {loginState: false}, callback);
                    }
                });
                break;
        }
    }

    function setConnectionState(state) {
        $('#connection_state').removeClass().text('');
        $("#tabs").hide();
        switch (state) {
            case 'OFFLINE':
                $('#connection_state').addClass('disconnected').text('Offline');
                break;
            case 'CONNECTING':
                $('#connection_state').addClass('connecting').text('Connecting');
                break;
            case 'CONNECTED':
                $('#connection_state').addClass('connected').text('Connected');
                $('#tabs').show();
                self.setTab('addressbook');
                rendererService.renderCalls();
                rendererService.renderIncomingCalls();
                checkCurrentContact();
                break;
        }
    }

    this.unsubscribe = function () {
        var numberOfTabs = windowService.getNumberOfTabs();
        windowService.removeTabId();
        if (numberOfTabs === 1) {
            if (!windowService.isPopupAlive()) {
                storageService.clear();
            }
        }
        if (!windowService.isPopupAlive()) {
            fcsService.unsubscribe();
        }
    };

    // only if tab is master (opened first) tab, subscribe
    this.subscribe = function (hideProcess) {
        if (hideProcess !== true) {
            setConnectionState('CONNECTING');
        }
        windowService.createTabId();
        if (windowService.amIMasterTab() && !fcsService.isConnected() && !windowService.isPopupAlive() && appState === 'APP') {
            fcsService.subscribe(function () {
                logger.log('Subscription successful');
                if (hideProcess !== true) {
                    setConnectionState('CONNECTED');
                }
                fcs.call.onReceived = function (call) {
                    if (!windowService.isPopupAlive()) {
                        if (windowService.checkPopupBlocker() === true) {
                            logger.warn('Popups blocked. You need unblock to receive calls!');
                            message.warning('Popups blocked. You need unblock to receive calls!');
                            call.ignore(function () {
                            }, function () {
                            });
                        }
                        else {
                            fcsService.onCallReceived(call);
                        }
                    }
                };
                logService.updateActivityHistory();
            }, function (error) {
                if(error == fcs.Errors.LOGIN_LIMIT_CLIENT){
                    message.error('Login failed, maximum allowed sessions reached', 4000);
                }
                else {
                    message.error('Subscription failed', 4000);
                }
                logger.warn('Subscription failed');
                setAppState('LOGIN');
            });
        } else {
            //logger.log('not master tab, no subscription');
            if (hideProcess !== true) {
                setConnectionState('CONNECTED');
            }
        }
    };

    this.enableTab = function (tab, set_tab) {
        $("#tab_" + tab).css("display", "inline-block");
        if (set_tab)
            self.setTab(tab);
    };
    this.disableTab = function (tab) {
        $("#tab_" + tab).css("display", "none");
        if (tab === currentTab) {
            self.setTab(prevTab);
        }
    };

    this.setTab = function (tab) {
        if (tab !== currentTab) {
            prevTab = currentTab;
            currentTab = tab;
            $('.tab_active').removeClass("tab_active");
            $('#tab_' + tab).addClass("tab_active");
            $('.tab_content').hide();
            $('#tab_content_' + tab).show();
            switch (tab) {
                case 'addressbook':
                    $('#addressbook_search').focus();
                    break;
            }
        }
    };

    function searchContact(text) {
        $("#contacts").html('<center>searching...</center>');
        contactService.searchSFContact(text, function (contacts) {
            $("#contacts").empty();
            if (contacts) {
                $.each(contacts, function (index, contact) {
                    if (contact) {
                        var detail = getContactHTML(contact);

                        if (detail.length > 0) {
                            $("#contacts").append('<div class="contact callable">' +
                                    '<div class="name">' + contact.Name + '</div>' +
                                    '<div class="detail">' + detail + '</div>' +
                                    '</div>');
                        }
                    }
                });
                rendererService.setNiceScroll($("#contacts"));
            }

            if ($("#contacts").html().length === 0) {
                $("#contacts").html('<center>No contacts found</center>');
            }
        });
    }

    function getContactHTML(contact) {
        if (contact) {
            var detail = "",
                    phones = [contact.Phone, contact.HomePhone, contact.MobilePhone, contact.OtherPhone, contact.AssistantPhone];
            $.each(phones, function (index, value) {
                if (value) {
                    detail += '<table class="contact_button phone_button" title="Call"><tr>';
                    detail += '<td class="text">' + value + '</td><td class="icon"></td></tr></table>';
                }
            });
            if (contact.Email) {
                detail += '<table class="contact_button email_button" title="Send Video Call Invitation"><tr>';
                detail += '<td class="text">' + contact.Email + '</td><td class="icon"></td></tr></table>';
            }
            return detail;
        }
        return '';
    }

    function checkCurrentContact() {
        contactService.checkCurrentContact(function (contact) {
            var detail = getContactHTML(contact);

            if (detail.length > 0) {
                $("#cc_name").text(contact.Name);
                $("#cc_id").text(contact.Id);
                $("#cc_info").html(detail);

                self.enableTab("contact_card", !callService.hasCall());
                rendererService.setNiceScroll($("#tab_content_contact_card"));
            }
        }, function () {
        });
    }
}();
