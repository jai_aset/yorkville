/* global fcsService, contactService, SFService, sforce, fcs */

var logger = new function () {

    var self = this,
            logFn = function (log) {
                console.info(log);
            },
            infoFn = function (log) {
                console.info(log);
            },
            debugFn = function (log) {
                console.debug(log);
            },
            warnFn = function (log) {
                console.warn(log);
            },
            errorFn = function (log) {
                console.error(log);
            };

    function printLog(handler, logObject) {
        var printFormat;

        if (handler && logObject) {
            printFormat = logObject.timestamp + ' - ' + logObject.logger + ' - ' + logObject.message;
            if (logObject.args) {
                handler(printFormat, logObject.args);
            } else {
                handler(printFormat);
            }
        }
    }


    function jslLogHandler(loggerName, level, logObject) {
        var LOG_LEVEL = fcs.logManager.Level;

        switch (level) {
            case LOG_LEVEL.DEBUG:
                self.debug(logObject.message, logObject);
                break;
            case LOG_LEVEL.FATAL:
            case LOG_LEVEL.ERROR:
                self.error(logObject.message, logObject);
                break;
            case LOG_LEVEL.WARN:
                self.warn(logObject.message, logObject);
                break;
            default:
                self.info(logObject.message, logObject);
        }
    }

    function createLogObject(message, logObject, logLevel, args) {
        // If logObject not defined, then it is an UI log and we need to define it
        if (!logObject) {
            logObject = {
                timestamp: Date.now(),
                logger: 'UI',
                level: logLevel,
                user: fcs.getUser(),
                message: message,
                args: args
            };
        }
        return logObject;
    }

    this.log = function (msg, logObject, args) {
        printLog(logFn, createLogObject(msg, logObject, 'LOG', args));
    };

    this.info = function (msg, logObject, args) {
        printLog(infoFn, createLogObject(msg, logObject, 'INFO', args));
    };

    this.debug = function (msg, logObject, args) {
        printLog(debugFn, createLogObject(msg, logObject, 'DEBUG', args));
    };

    this.warn = function (msg, logObject, args) {
        printLog(warnFn, createLogObject(msg, logObject, 'WARN', args));
    };

    this.error = function (msg, logObject, args) {
        if (msg.message) {
            args = {stack: msg.stack};
            msg = msg.message;
            logObject = null;
        }
        printLog(errorFn, createLogObject(msg, logObject, 'ERROR', args));
    };

    fcs.logManager.initLogging(jslLogHandler, true);
}();
