/* global callService, timerService, contactService, storageService, audioToneService, fcsService, widget, windowService */

var rendererService = new function () {
    var self = this,
            rendering_calls = false,
            rendering_incomingcalls = false;


    // gets calls stored on localstorage and prepares UI
    this.renderCalls = function () {
        if (!rendering_calls && (fcsService.isConnected() || (window.i_am_widget || window.render_is_needed))) {
            rendering_calls = true;
            var activeCall = callService.getActiveCall();
            var inactiveCalls = callService.getInactiveCalls();

            if (activeCall || inactiveCalls) {
                if (activeCall) {
                    $("#active_call").show();
                    timerService.setCallTimer(activeCall.start_time);
                    if (window.i_am_widget) {
                        $("#active_call").find("#contact").html("<a id='contact_redirect' href='javascript:'>" + activeCall.contact + "</a>");
                        $("#active_call").find("#contact").attr('title', activeCall.number);
                        $("#active_call").find("#contact a").click(function () {
                            if (activeCall.sf_contact_id && window.__currentContactId !== activeCall.sf_contact_id) {
                                windowService.openContactPage(activeCall.sf_contact_id);
                            }
                        });
                        self.setStatus(storageService.getItem('call_status'));
                    } else if (window.i_am_popup) {
                        contactService.setContactTitle(activeCall.number, activeCall.contact);
                        if (activeCall.contactPhoto) {
                            $("#media").css('background-image', "url('" + activeCall.contactPhoto + "')");
                        } else {
                            $("#media").css('background-image', "url('" + window.config.serverUrl + "/assets/avatar_big.png')");
                        }
                    }
                    $("#active_call").attr("call_id", activeCall.id);
                    if (activeCall.mute === 1) {
                        $("#active_call").find(".mute_call").addClass("mute_call_active").attr('title', 'Unmute');
                    } else {
                        $("#active_call").find(".mute_call").removeClass("mute_call_active").attr('title', 'Mute');
                    }
                } else {
                    timerService.stopCallTimer();
                    $("#active_call").hide();
                }

                $("#inactive_calls").empty().hide();
                if (inactiveCalls) {
                    $("#inactive_calls").show();
                    jQuery.each(inactiveCalls, function (callId, call) {
                        $("#inactive_calls").append("<div class='container held_call' call_id='" + callId + "'>" +
                                "<table style='width:100%;'><tr>" +
                                "<td class='held_call_text' title='" + call.number + "'>" + call.contact + "</td>" +
                                "<td><span class='end_call' title='End call'></td>" +
                                "<td><span class='bordered unhold_call' title='Unhold'></span></td>" +
                                "</tr></table>" +
                                "</div>");
                    });
                }

                var totalNumberOfCalls = (inactiveCalls != null ? Object.keys(inactiveCalls).length : 0) + (activeCall != null ? 1 : 0);
                var totalNumberOfCallsLabel = "No Call";

                if(totalNumberOfCalls > 0){
                    totalNumberOfCallsLabel = 'Total Number of Calls: ' + totalNumberOfCalls;
                }

                $("#total_number_of_calls").text(totalNumberOfCallsLabel);

                if (window.i_am_widget) {
                    widget.enableTab('active_call', true);
                } else if (window.i_am_popup) {
                    window.resizeTo(480, $("#popup").height() + 90);
                }
            } else {
                timerService.stopCallTimer();
                if (window.i_am_widget) {
                    widget.disableTab('active_call');
                } else if (window.i_am_popup && !callService.hasIncomingCall() && !callService.hasOutgoingCall()) {
                    setTimeout(function () {
                        window.close();
                    }, 500);
                }
            }
            rendering_calls = false;
            window.render_is_needed = false;
        }
    };

    // gets incomingcalls stored on localstorage and prepares UI
    this.renderIncomingCalls = function () {
        if (!rendering_incomingcalls) {
            rendering_incomingcalls = true;
            if (callService.hasIncomingCall()) {
                if (window.i_am_popup) {
                    audioToneService.play(audioToneService.RING_IN);
                }
                var incomingCalls = callService.getIncomingCalls();
                var html = "";
                jQuery.each(incomingCalls, function (callId, call) {
                    html += rendererService.templateEngine($('#incoming_call_tmpl').html(), {
                        callId: call.id,
                        contact: call.contact,
                        canReject: call.canReject,
                        contactPhoto: call.contactPhoto,
                        canReceiveRemoteVideo: call.canReceiveRemoteVideo
                    });
                });
                this.showModal(html);

                $('#modal').on('click', '.audio_answer', function () {
                    var callId = $(this).parents(".incoming_call").attr("call_id");
                    callService.answerIncomingCall(callId, false);
                    if (window.i_am_popup) {
                        fcsService.handleIncomingCall();
                    }
                    else {
                        windowService.openPopup();
                    }
                });
                $('#modal').on('click', '.video_answer', function () {
                    var callId = $(this).parents(".incoming_call").attr("call_id");
                    callService.answerIncomingCall(callId, true);
                    if (window.i_am_popup) {
                        fcsService.handleIncomingCall();
                    }
                    else {
                        windowService.openPopup();
                    }
                });
                $('#modal').on('click', '.ignore_call', function () {
                    var callId = $(this).parents(".incoming_call").attr("call_id");
                    callService.ignoreIncomingCall(callId);
                    if (window.i_am_popup) {
                        fcsService.handleIncomingCall();
                    }
                });
                $('#modal').on('click', '.decline_call', function () {
                    var callId = $(this).parents(".incoming_call").attr("call_id");
                    callService.declineIncomingCall(callId);
                    if (window.i_am_popup) {
                        fcsService.handleIncomingCall();
                    }
                });


            } else {
                if (window.i_am_popup) {
                    audioToneService.stop(audioToneService.RING_IN);
                }
                this.hideModal();
            }
            rendering_incomingcalls = false;
        }
    };




    // sets status of active call as text
    this.setStatus = function (state) {
        $("#status").text(state);
        if (window.i_am_popup)
            storageService.setItem('call_status', state);
    };

    this.templateEngine = function (html, options) {
        var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0;
        var add = function (line, js) {
            js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                    (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
            return add;
        };
        while (match = re.exec(html)) {
            add(html.slice(cursor, match.index))(match[1], true);
            cursor = match.index + match[0].length;
        }
        add(html.substr(cursor, html.length - cursor));
        code += 'return r.join("");';
        return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
    };

    this.showModalMessage = function (message) {
        this.showModal('<div id="modal_text">' + message + '</div>');
    };
    this.showModal = function (html) {
        this.hideModal();
        $('body').append('<div id="modal">' + html + '</div>');
        $("#modal")
                .width($(window).width())
                .height($(window).height())
                .show();
        this.setNiceScroll($("#modal"));

    };

    this.hideModal = function () {
        $("#modal").remove();
    };

    this.setNiceScroll = function (obj) {
        if (obj && obj.length > 0) {
            if (obj.getNiceScroll())
                obj.getNiceScroll().remove();
            obj.niceScroll();
        }
    };
}();
