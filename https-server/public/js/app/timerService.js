var timerService = new function () {
    var callTimerIntervalHandle;
    // sets timer of active call
    this.setCallTimer = function (time) {
        if (time) {
            var minutes = 0,
                    seconds = 0;

            var diff = ((new Date()).getTime() - time) / 1000;
            minutes = parseInt(diff / 60);
            seconds = parseInt(diff % 60);

            function calculateAndOutputFormattedTime() {
                seconds++;
                if (seconds > 59) {
                    minutes++;
                    seconds = 0;
                }
                var minText = minutes > 9 ? minutes : "0" + minutes;
                var secText = seconds > 9 ? seconds : "0" + seconds;
                $("#timer").text(minText + ":" + secText);
            }

            if (!callTimerIntervalHandle) {
                callTimerIntervalHandle = setInterval(function () {
                    calculateAndOutputFormattedTime();
                }, 1000);
            }
        }else{
            this.stopCallTimer();
        }
    };
    // stops timer
    this.stopCallTimer = function () {
        $("#timer").text("00:00");
        if (callTimerIntervalHandle) {
            clearInterval(callTimerIntervalHandle);
            callTimerIntervalHandle = null;
        }
    };
}();