var message = new function () {
    var self = this;
    function displayAlert(message, timeout, transparent, pushdown) {
        var alr = self.alert();
        alr.text(message);
        alr.addClass('transparent');
        if (transparent === false) {
            alr.removeClass('transparent');
        }
        if (pushdown === true) {
            alr.addClass('pushdown');
        }
        alr.fadeIn();
        //0 means infinite timeout
        if (timeout && timeout !== 0) {
            setTimeout(function () {
                closeAlert();
            }, timeout);
        }
    }
    function closeAlert() {
        var alr = self.alert();
        alr.fadeOut(function(){
            alr.attr('class','');
        });
    }
    
    this.alert = function(){
        return $('#alert');
    };

    this.error = function (message, timeout, transparent, pushdown) {
        if (message) {
            var alr = self.alert();
            alr.addClass('error');
            displayAlert(message, timeout, transparent, pushdown);
        }
    };
    
    this.warning = function (message, timeout, transparent, pushdown) {
        if (message) {
            var alr = self.alert();
            alr.addClass('warning');
            displayAlert(message, timeout, transparent, pushdown);
        }
    };
    
    this.info = function (message, timeout, transparent, pushdown) {
        if (message) {
            var alr = self.alert();
            alr.addClass('info');
            displayAlert(message, timeout, transparent, pushdown);
        }
    };
}();