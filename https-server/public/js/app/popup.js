/* global fcsService, audioToneService, listenerService, noteService, callService, rendererService, storageService, dtmfService, logger */

var popup = new function () {
    var sfdcOnload = window.onload,
            self = this;

    this.onload = function () {
        if (sfdcOnload) {
            sfdcOnload();
        }
        setInterval(function () {
            storageService.setItem("popup_opened", (new Date()).getTime());
        }, 1000);

        $("body").css("margin", 0).css("padding", 0).css('background-color', '#CFEEF8');

        audioToneService.init();
        subscribe();
        storageService.listen(listenerService.popupListener);

        $('#popup').on('click', '.end_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            fcsService.endCall(callId);
            window.render_is_needed = true;
            rendererService.renderCalls();
        });
        $('#popup').on('click', '.hold_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            fcsService.holdCall(callId);
        });
        $('#popup').on('click', '.unhold_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            fcsService.unholdCall(callId);
        });
        $('#popup').on('click', '.mute_call', function () {
            var callId = $(this).parents(".container").attr("call_id");
            if ($(this).hasClass('mute_call_active')) {
                fcsService.unmuteCall(callId);
            } else {
                fcsService.muteCall(callId);
            }
        });

        $('#popup').find('.keypad').show().off('click').on('click', function () {
            var callId = $(this).parents(".container").attr("call_id");
            if ($(this).hasClass('keypad_active')) {
                $(this).removeClass('keypad_active');
            } else {
                $(this).addClass('keypad_active');
            }
            $('#dial_pad').toggle();
            $('#dial_pad').find('.dialpad_btn').off('click').on('click', function () {
                var dtmf_tone = $(this).attr("data-val");
                var call_display = $('#dial_pad').find('#call_display');
                call_display.val(call_display.val() + dtmf_tone);
                fcsService.sendDTMF(callId, dtmf_tone);
            });

            $('#dial_pad').find('#erase').on('click', function () {
                $('#dial_pad').find('#call_display').val('');
            });
        });

        $('#popup').on('click', '.add_note', function () {
            closeDialPad();
            noteService.openDialog();
        });


        $('#audio_answer').on('click', function () {
            var callId = $('#incoming_call').attr('call_id');
            fcsService.answerAudioCall(callId);
        });

        $('#video_answer').on('click', function () {
            var callId = $('#incoming_call').attr('call_id');
            fcsService.answerVideoCall(callId);
        });

        $('#decline_call').on('click', function () {
            var callId = $('#incoming_call').attr('call_id');
            fcsService.declineCall(callId);
        });

        $('#ignore_call').on('click', function () {
            var callId = $('#incoming_call').attr('call_id');
            fcsService.ignoreCall(callId);
        });

        window.onbeforeunload = function () {
            fcs.setAsync(false);
            fcsService.unsubscribe();
            storageService.clear();
            window.render_is_needed = true;
            rendererService.renderCalls();
        };

        window.onresize = function () {
            window.resizeTo(480, $("#popup").height() + 90);
        };
    };

    if (!window._testonly) {
        window.i_am_popup = true;
        window.onload = self.onload;
    }

    function closeDialPad() {
        $('#dial_pad').hide();
        $('#popup').find('.keypad').removeClass('keypad_active');
    }

    function subscribe() {
        rendererService.showModalMessage('connecting');
        fcsService.subscribe(function () {
            logger.log('Subscription successful');
            rendererService.hideModal();
            fcs.call.onReceived = function (call) {
                fcsService.onCallReceived(call);
            };
            initialCallCheck();
        }, function () {
            logger.warn('Subscription failed');
            rendererService.showModalMessage('Connection failed');
            setTimeout(function () {
                window.close();
            }, 3000);
        });
    }

    function initialCallCheck() {
        if (callService.hasOutgoingCall()) {
            fcsService.startCall();
        }
        else if (callService.hasIncomingCall()) {
            fcsService.handleIncomingCall();
        }
        else {
            logger.warn('no call found in initial check');

            // to prevent from empty popup
            rendererService.renderCalls();
        }
    }

}();
