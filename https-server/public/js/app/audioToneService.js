var audioToneService = new function () {

    // initializes audio tone playing
    this.init = function () {
        var $audioTonesDiv = $('#audio_tones');

        this.useAudioTag = (typeof window.HTMLAudioElement !== "undefined");

        if (this.useAudioTag) {
            this.RING_IN = $("<audio id='ring_in_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringin.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringin.ogg' type='audio/ogg'></audio>")[0];
            this.RING_OUT = $("<audio id='ring_out_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringout.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/ringout.ogg' type='audio/ogg'></audio>")[0];
            this.BUSY = $("<audio id='busy_audio' loop='loop'><source src='" + window.config.serverUrl + "/assets/audio_tones/busy.mp3' type='audio/mp3'><source src='" + window.config.serverUrl + "/assets/audio_tones/busy.ogg' type='audio/ogg'></audio>")[0];
        }
        else {
            this.RING_IN = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/ringin.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            this.RING_OUT = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/ringout.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            this.BUSY = $('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="0" height="0"><param name="movie" value="' + window.config.serverUrl + '/assets/audio_tones/busy.SWF" /><param name="quality" value="high" /><param name="play" value="false"><param name="loop" value="true"></object>')[0];
            $audioTonesDiv.removeAttr('style');
        }

        $audioTonesDiv.append(this.RING_IN);
        $audioTonesDiv.append(this.RING_OUT);
        $audioTonesDiv.append(this.BUSY);
    };

    // start playing tone
    this.play = function (tone) {
        if (!tone || tone.readyState === 0) {
            return;
        }

        if (this.useAudioTag) {
            tone.play();
        } else {
            tone.Play();
        }
    };

    // stops playing tone
    this.stop = function (tone) {
        if (!tone || tone.readyState === 0) {
            return;
        }

        if (this.useAudioTag) {
            tone.pause();
            tone.currentTime = 0;
        }
        else {
            tone.StopPlay();
        }
    };
}();