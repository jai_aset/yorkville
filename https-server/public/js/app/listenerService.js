/* global fcsService, rendererService, storageService, widget, callLog, logService, contactService, windowService */

var listenerService = new function () {
    this.popupListener = function (event) {
        switch (event.key) {
            case 'outgoing_call':
                fcsService.startCall();
                break;
            case 'incoming_call':
                if(fcsService.isConnected()){
                    fcsService.handleIncomingCall();
                }
                break;
            case 'calls':
                rendererService.renderCalls();
                break;
            case 'end_call':
                fcsService.endCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'hold_call':
                fcsService.holdCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'unhold_call':
                fcsService.unholdCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'mute_call':
                fcsService.muteCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            case 'unmute_call':
                fcsService.unmuteCall(event.newValue);
                storageService.removeItem(event.key);
                break;
            default:
                if (event.key.indexOf(windowService.getPrefix()) !== 0 && fcsService.isConnected()) {
                    rendererService.renderCalls();
                }
                break;
        }
    };

    this.widgetListener = function (event) {
        switch (event.key) {
            case 'call_status':
                rendererService.setStatus(event.newValue);
                break;
            case 'incoming_call':
                rendererService.renderIncomingCalls();
                break;
            case 'calls':
                rendererService.renderCalls();
                break;
            case 'popup_opened':
                if (!event.newValue) {
                    fcsService.unsubscribe(function () {
                        widget.subscribe(true);
                    });
                }
                break;
            default:
                // if any tab closed, call subscribe (it will check master and connectivity inside)
                if (event.key.indexOf(windowService.getPrefix()) === 0 && !event.newValue) {
                    widget.subscribe(true);
                }
                break;
        }
    };

    this.callLogListener = function (event) {
        switch (event.key) {
            case 'popup_opened':
                if (!event.newValue) {
                    logService.refreshCallLogGrid();
                }
                break;
        }
    };
}();
