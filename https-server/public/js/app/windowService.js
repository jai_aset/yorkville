/* global storageService */

var windowService = new function () {
    var self = this, prefix = 'sfdctab_', popupWin;

    this.getPrefix = function () {
        return prefix;
    };

    this.createTabId = function () {
        if (!window.id) {
            self.removeTabId();
            var id = (new Date()).getTime();
            window.id = id;
            var contactId = '';
            if (window.__currentContactId) {
                contactId = window.__currentContactId;
            }
            storageService.setItem(prefix + window.id, '{"id":' + window.id + ', "contact_id":"' + contactId + '", "time":' + (new Date()).getTime() + '}');

            // in every 2 secs; update to set the tab as alive and check if existance of other tabs (opened before the current tab).
            window.checkMasterAliveInterval = setInterval(function () {
                storageService.setItem(prefix + window.id, '{"id":' + window.id + ', "contact_id":"' + contactId + '", "time":' + (new Date()).getTime() + '}');
                checkMasterAlive();
            }, 2000);
        }
    };

    this.removeTabId = function () {
        var id = window.id;
        if (id) {
            storageService.removeItem(prefix + id);
        }
    };

    this.getNumberOfTabs = function () {
        var count = 0, i;
        for (i in window.localStorage) {
            if (i.indexOf(prefix) === 0) {
                count++;
            }
        }
        return count;
    };

    this.amIMasterTab = function () {
        var tabId, i;
        for (i in window.localStorage) {
            if (i.indexOf(prefix) === 0) {
                var tabId = JSON.parse(storageService.getItem(i)).id;
                if (tabId < window.id) {
                    return false;
                }
            }
        }
        return true;
    };

    // if any tab (opened before the current tab) which is closed and has a record on localstorage, remove it
    function checkMasterAlive() {
        var tabId, i;
        for (i in window.localStorage) {
            if (i.indexOf(prefix) === 0) {
                var tabId = JSON.parse(storageService.getItem(i)).id;
                if (tabId < window.id) {
                    var time = JSON.parse(storageService.getItem(i)).time,
                            currentTime = (new Date()).getTime();
                    if (!time || currentTime - time > 3000) {
                        storageService.removeItem(i);
                    }
                }
            }
        }
        widget.subscribe(true);
    }

    this.openPopup = function () {
        if (popupWin && !popupWin.closed) {
            focusPopup();
        } else if (!self.isPopupAlive()) {
            var width = 470;
            var height = 550;
            var left = (screen.width - width) / 2;
            var top = (screen.height - height) / 2;
            popupWin = window.open('/apex/CallPopup?inline=1', 'sfdcpopup', 'height=' + height + ',width=' + width + ',left=' + left + ',top=' + top + ',resizable=no,scrollbars=yes,toolbar=no,status=no');
            storageService.setItem("popup_opened", (new Date()).getTime());
        } else {
            focusPopup();
        }
    };
    function focusPopup() {
        if (popupWin && !popupWin.closed)
            popupWin.focus();
    }

    this.getPopup = function () {
        return popupWin;
    };

    this.openContactPage = function (contactId) {
//        function isContactPageOpened(contactId) {
//            var i, tabContactId;
//            for (i in window.localStorage) {
//                if (i.indexOf('sfdctab_') === 0) {
//                    var tabContactId = JSON.parse(storageService.getItem(i)).contact_id;
//                    if (contactId === tabContactId) {
//                        return true;
//                    }
//                }
//            }
//            return false;
//        }
//        if (isContactPageOpened(contactId)) {
//            window.open('/' + contactId, contactId).focus();
//        }else {
//            window.open('/' + contactId, '_blank').focus();
//        }

        window.open('/' + contactId, '_blank').focus();
    };

    //TODO: popup blocker need to be checked
    this.checkPopupBlocker = function () {
        var popupBlockerControl = window.open(window.location, '', 'width=1, height =1');
        if (!popupBlockerControl || popupBlockerControl.closed || typeof popupBlockerControl.closed === 'undefined') {
            return true;
        }
        else {
            popupBlockerControl.close();
            return false;
        }
    };

    this.isPopupAlive = function () {
        var lastPopupUpdate = storageService.getItem("popup_opened");
        if (lastPopupUpdate && (new Date()).getTime() - lastPopupUpdate < 2000) {
            return true;
        }
        return false;
    };
}();