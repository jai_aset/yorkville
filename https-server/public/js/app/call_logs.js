/* global fcsService, listenerService, storageService, contactService, SFService, logService */

var callLog = new function () {
    var sfdcOnload = window.onload,
            self = this;

    this.onload = function () {
        if (sfdcOnload) {
            sfdcOnload();
        }
        $("body").css("margin", 0).css("padding", 0);

        $("#spidr_call_logs").jqGrid && $("#spidr_call_logs").jqGrid({
            datatype: "local",
            autowidth: true,
            height: 230,
            rowNum: 10,
            rowList: [10, 20, 30],
            colNames: ['Type', 'Name', 'Address', 'Start Time'],
            colModel: [
                {name: 'type_label', width: 40, index: 'type_label', align: "center"},
                {name: 'name', index: 'name'},
                {name: 'address', index: 'address'},
                {name: 'startTime', index: 'startTime', sorttype: "date", formatter: 'date',
                    formatoptions: {srcformat: 'D M d Y H:i:s', newformat: 'M d g:i A'}}
            ],
            pager: "#spidr_call_logs_pager",
            viewrecords: true,
            caption: "Call Logs",
            hidegrid: false
        });

        storageService.listen(listenerService.callLogListener);
        logService.refreshCallLogGrid();
    };
    if (!window._testonly) {
        window.i_am_call_logs = true;
        window.onload = self.onload;
    }
}();
