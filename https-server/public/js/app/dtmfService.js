var dtmfService = new function () {
    var audioContext,
            TONE_DTMF_FREQ = {
                "1": {freq1: "697", freq2: "1209"},
                "2": {freq1: "697", freq2: "1336"},
                "3": {freq1: "697", freq2: "1477"},
                "4": {freq1: "770", freq2: "1209"},
                "5": {freq1: "770", freq2: "1336"},
                "6": {freq1: "770", freq2: "1477"},
                "7": {freq1: "852", freq2: "1209"},
                "8": {freq1: "852", freq2: "1336"},
                "9": {freq1: "852", freq2: "1477"},
                "*": {freq1: "941", freq2: "1209"},
                "0": {freq1: "941", freq2: "1336"},
                "#": {freq1: "941", freq2: "1477"}
            };

    this.sendDTMF = function (call, tone) {
        call.sendDTMF(tone);
        playDtmfTone(tone);
    };

    function playDtmfTone(tone) {
        var oscillator1,
                oscillator2,
                freq1 = TONE_DTMF_FREQ[tone].freq1,
                freq2 = TONE_DTMF_FREQ[tone].freq2,
                gainNode;

        audioContext = audioContext || new window.AudioContext();
        oscillator1 = audioContext.createOscillator();
        oscillator2 = audioContext.createOscillator();

        oscillator1.frequency.value = freq1;
        gainNode = audioContext.createGain ? audioContext.createGain() : audioContext.createGainNode();
        oscillator1.connect(gainNode, 0, 0);
        gainNode.connect(audioContext.destination);
        gainNode.gain.value = 0.1;
        oscillator1.start(0);

        oscillator2.frequency.value = freq2;
        gainNode = audioContext.createGain ? audioContext.createGain() : audioContext.createGainNode();
        oscillator2.connect(gainNode);
        gainNode.connect(audioContext.destination);
        gainNode.gain.value = 0.1;
        oscillator2.start(0);

        setTimeout(function () {
            oscillator1.disconnect();
            oscillator2.disconnect();
        }, 300);
    }
}();