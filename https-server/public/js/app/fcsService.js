/* global fcs, audioToneService, SFService, timerService, callService, rendererService, storageService, dtmfService, contactService, message, logger, phoneNumberService, windowService */

var fcsService = new function () {

    var calls = {},
            self = this,
            connected = false,
            subscriptionInProgress = false;

    window.config = {
        serverUrl: "<%=contextPath%>/",
        pluginLink: "<%=contextPath%>/plugin/gencomweb-plugin_3.1.502.0/GCFWEnabler.exe"
    };

    this.isConnected = function () {
        return connected;///fcs.isConnected();
    };

    this.setup = function (credentials) {
        fcs.setup({
            "restUrl": credentials.rest_server,
            "restPort": credentials.rest_port,
            "protocol": "https",
            "cors": "true",
            "notificationType": fcs.notification.NotificationTypes.WEBSOCKET,
            "websocketIP": credentials.ws_server,
            "websocketPort": credentials.ws_port,
            "websocketProtocol": "wss",
            "continuity": true,
            "services": ["call"]
        });
    };

    this.setUserAuth = function (credentials) {
        fcs.setUserAuth(credentials.username, credentials.password);
    };

    this.notificationStart = function (onSuccess, onFailure, isAnonymous) {
        fcs.notification.start(function () {
            logger.log("Notification start successful");
            onSuccess();
        }, function (error) {
            logger.warn("Notification start failed: ", error);
            onFailure(error);
        }, isAnonymous === true);
    };

    this.initMedia = function (onSuccess, onFailure) {

        SFService.loadCredentials(function (credentials) {
            if(credentials.webRtcDTLS == "false"){
                credentials.webRtcDTLS = false;
            }
            else {
                credentials.webRtcDTLS = true;
            }

            fcs.call.initMedia(onSuccess, onFailure, {
                iceserver: {
                    url: credentials.iceServerUrl
                },
                pluginMode: {
                    mode: "auto"
                },
                notificationType: "websocket",
                webrtcdtls: credentials.webRtcDTLS
            });
        });
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.setup, fcs.call.initMedia and fcs.notification.start functions
     * 
     * @param {type} onSuccess Success callback for fcs subscribtion
     * @param {type} onFailure Fail callback for fcs subscribtion
     * 
     * @example 
     * fcsService.subscribe(function(){
     *     window.console.log('Subscription successful');
     * },function(){
     *     window.console.log('Subscription failed');
     * });
     */
    this.subscribe = function (onSuccess, onFailure) {
        if (!subscriptionInProgress) {
            connected = false;
            SFService.loadCredentials(function (credentials) {
                self.setUserAuth(credentials);
                self.setup(credentials);
                self.initMedia(function () {
                    logger.log("Media initialized successfully");
                    subscriptionInProgress = true;
                    self.notificationStart(function () {
                        connected = true;
                        subscriptionInProgress = false;
                        if (onSuccess)
                            onSuccess();
                    }, function (error) {
                        subscriptionInProgress = false;
                        if (onFailure)
                            onFailure(error);
                    });
                }, function (error) {
                    logger.warn("Media initialization failed: ", error);
                    if (window.i_am_widget) {
                        if (error === fcs.call.MediaErrors.WRONG_VERSION || error === fcs.call.MediaErrors.NOT_FOUND) {
                            $("#plugin_install").show();
                            $('#download_plugin').on('click', function () {
                                window.open(window.config.pluginLink, "_blank");
                            });
                        }
                    }
                    onFailure();
                });
            });
        }
    };

    /**
     * Subcribes the window/popup to JSL services
     * Contains fcs.notification.stop function
     * 
     * @example 
     * fcsService.unsubscribe();
     */
    this.unsubscribe = function (successCallback) {
        jQuery.each(calls, function (callId, call) {
            self.endCall(callId);
        });
        if (self.isConnected()) {
            connected = false;
            if (successCallback)
                fcs.notification.stop(successCallback, function () {
                }, true);
            else
                fcs.notification.stop();
        }
    };

    this.onCallReceived = function (call) {
        var callId = call.getId();
        var callObj = {
            id: callId,
            contact: call.callerName,
            number: call.callerNumber,
            canReject: call.canReject(),
            canReceiveRemoteVideo: call.isVideoNegotationAvailable(callId)
        };
        callService.addIncomingCall(callObj);
        rendererService.renderIncomingCalls();
        if (window.i_am_widget) {
            windowService.openPopup();
        }
        contactService.getSFContacts(call.callerNumber, function (contactsMap) {
            var contacts = contactsMap[call.callerNumber];
            if (contacts && contacts.length > 0 && contacts[0].Id) {
                var contact = contacts[0];
                callService.setIncomingCallSFContact(callId, contact.Name, contact.Id);
                rendererService.renderIncomingCalls();
            }
        });
        contactService.searchSpidrContact(call.callerNumber, function (contacts) {
            if (contacts) {
                var index;
                for (index in contacts) {
                    if (call.callerName === contacts[index].firstName + ' ' + contacts[index].lastName && contacts[index].photoUrl) {
                        callService.setIncomingCallContactPhoto(callId, contacts[index].photoUrl);
                        rendererService.renderIncomingCalls();
                    }
                }
            }
        });
    };

    //just for testing
    this.addtoCalls = function (callId, call) {
        calls[callId] = call;
    };
    //just for testing
    this.deleteFromCalls = function (callId) {
        delete calls[callId];
    };

    this.startCall = function () {
        logger.log('Starting call...');
        rendererService.setStatus("connecting");
        timerService.stopCallTimer();

        var call = callService.getOutgoingCall();

        var callNumberForStorage = phoneNumberService.standardizePhoneNumber(call.number).forCompare;
        callNumberForStorage = phoneNumberService.appendDomain(callNumberForStorage);

        call.number = phoneNumberService.standardizePhoneNumber(call.number).forCall;
        call.number = phoneNumberService.appendDomain(call.number);

        logger.log(call.number + ' is getting called...');

        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            self.holdCall(activeCall.id);
        }

        fcs.call.startCall(fcs.getUser(), '', call.number, function (outgoingCall) {
            logger.log('Call start to ' + call.number + ' successful');
            callService.deleteOutgoingCall();
            var callId = outgoingCall.getId();
            //calls[callId] = outgoingCall;
            self.addtoCalls(callId, outgoingCall);

            var activeCall = {};
            activeCall.id = callId;
            activeCall.type = 'out';
            activeCall.contact = call.contact;
            activeCall.sf_contact_id = call.sf_contact_id;
            activeCall.number = callNumberForStorage;
            activeCall.status = 'active';
            activeCall.mute = 0;

            callService.addCall(activeCall);
            rendererService.renderCalls();

            contactService.searchSpidrContact(call.number, function (contacts) {
                if (contacts) {
                    var index;
                    for (index in contacts) {
                        if (contacts[index].photoUrl) {
                            callService.setCallContactPhoto(callId, contacts[index].photoUrl);
                            rendererService.renderCalls();
                        }
                    }
                }
            });

            outgoingCall._isOutgoing = true;
            setStateChange(outgoingCall);
            setStreamAdded(outgoingCall);

        }, function (error) {
            logger.warn('Call start to ' + call.number + ' failed: ' + error);
            callService.deleteOutgoingCall();
            rendererService.setStatus("call failed");
            rendererService.renderCalls();
            setTimeout(function () {
                audioToneService.stop(audioToneService.BUSY);
            }, 2000);
        }, false, false);
    };

    this.handleIncomingCall = function () {
        var incomingCalls = callService.getIncomingCalls();
        jQuery.each(incomingCalls, function (callId, incomingCall) {
            if (!calls[callId]) {
                logger.info('Handling incoming call from ' + incomingCall.number);
                var call = fcs.call.getIncomingCallById(callId);
                if (call) {
                    //calls[callId] = call;
                    self.addtoCalls(callId, call);

                    call._isIncoming = true;
                    setStateChange(call);
                    setStreamAdded(call);
                }
            }

            //removes storage item with callId composed by getIncomingCallById
            storageService.removeItem(callId);

            var command = incomingCall.command;
            if (command === 'answer') {
                self.answerAudioCall(callId);
            }
            else if (command === 'answer_video') {
                self.answerVideoCall(callId);
            }
            else if (command === 'decline') {
                self.declineCall(callId);
            }
            else if (command === 'ignore') {
                self.ignoreCall(callId);
            }
        });
        rendererService.renderIncomingCalls();
    };

    function setStateChange(call) {
        if (call) {
            var callId = call.getId();

            call.onStateChange = function (state, statusCode) {
                logger.info("callStateChange \tstate:" + state + " \tstatusCode:" + statusCode, fcs.call.States);
                switch (state) {
                    case fcs.call.States.RINGING:
                        audioToneService.play(audioToneService.RING_OUT);
                        rendererService.setStatus("ringing");
                        break;
                    case fcs.call.States.ENDED:
                        rendererService.setStatus("ended");
                        if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                            audioToneService.play(audioToneService.BUSY);

                        setTimeout(function () {
                            if (call._isOutgoing && call._state == fcs.call.States.RINGING)
                                audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isIncoming) {
                                callService.deleteIncomingCall(callId);
                                rendererService.renderIncomingCalls();
                            }
                            rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.REJECTED:
                        rendererService.setStatus("rejected");
                        audioToneService.play(audioToneService.BUSY);
                        setTimeout(function () {
                            audioToneService.stop(audioToneService.BUSY);
                            callService.endCall(callId);
                            //delete calls[callId];
                            self.deleteFromCalls(callId);
                            if (call._isOutgoing)
                                rendererService.renderCalls();
                        }, 2000);
                        timerService.stopCallTimer();
                        break;
                    case fcs.call.States.RENEGOTIATION:
                    case fcs.call.States.TRANSFERRED:
                    case fcs.call.States.IN_CALL:
                        if (call._isIncoming)
                            audioToneService.stop(audioToneService.RING_IN);
                        else if (call._isOutgoing)
                            audioToneService.stop(audioToneService.RING_OUT);
                        rendererService.setStatus("in call");
                        callService.setCallStartTime(callId);
                        var activeCall = callService.getActiveCall();
                        timerService.setCallTimer(activeCall.start_time);
                        break;
                    case fcs.call.States.ON_REMOTE_HOLD:
                        rendererService.setStatus("call held remotely");
                        break;
                    case fcs.call.States.OUTGOING:
                        rendererService.setStatus("There is an outgoing call");
                        break;
                }
                call._state = state;
                rendererService.renderCalls();
                //rendererService.renderIncomingCalls();
            };
        }
    }

    function setStreamAdded(call) {
        if (call) {
            call.onStreamAdded = function (streamURL) {
                var activeCall = callService.getActiveCall();
                if (activeCall && call.getId() === activeCall.id) {
                    logger.info('Stream adding for ' + call.callerNumber + '...');
                    // Setting up source (src tag) of remote video container
                    if (streamURL) {
                        $("#remoteVideo").attr("src", streamURL);
                    }
                    if (call.canReceiveVideo()) {
                        logger.info('Call can receive video');
                        $("#remoteVideo").show();
                    }
                    else {
                        logger.info('Call cannot receive video');
                        $("#remoteVideo").hide();
                    }

                    if (!call.canSendVideo()) {
                        $("#localVideo").hide();
                    }
                    logger.info('Stream added for ' + call.callerNumber + ':' + streamURL);
                }
            };

            call.onLocalStreamAdded = function (streamURL) {
                logger.info('Local stream adding for ' + call.callerNumber + '...');
                // Setting up source (src tag) of remote video container
                if (call.canSendVideo()) {
                    logger.info('Call can send video');
                    $("#localVideo").attr("src", streamURL).attr('muted', true).show();
                }
                else {
                    logger.info('Call cannot send video');
                    $("#localVideo").attr("src", "");
                }
                logger.info('Local stream added for ' + call.callerNumber + ':' + streamURL);
            };
        }
    }

    function answerCall(call, video, resolution) {
        logger.log('Answering call from ' + call.callerNumber + '...');
        var callId = call.getId(),
                incomingCalls = callService.getIncomingCalls();
        var callObj = {};
        callObj.id = callId;
        callObj.type = 'in';
        callObj.contact = call.callerName;
        callObj.number = call.callerNumber;
        callObj.status = 'active';
        callObj.mute = 0;
        if (incomingCalls && incomingCalls[callId]) {
            callObj.contact = incomingCalls[callId].contact;
            if (incomingCalls[callId].sf_contact_id) {
                callObj.sf_contact_id = incomingCalls[callId].sf_contact_id;
            }
            if (incomingCalls[callId].contactPhoto) {
                callObj.contactPhoto = incomingCalls[callId].contactPhoto;
            }
        }
        var activeCall = callService.getActiveCall();
        if (activeCall) {
            logger.log('Active calls getting held...');
            if (activeCall.id !== callId) {
                self.holdCall(activeCall.id);
            }
        }
        callService.addCall(callObj);
        call.answer(function () {
            logger.log('Call answered successfully');
            callService.deleteIncomingCall(callId);
            rendererService.renderCalls();
            rendererService.renderIncomingCalls();
        }, function (e) {
            callService.deleteIncomingCall(callId);
            callService.endCall(callId);
            rendererService.renderCalls();
            message.error('Call answer failed', 2000);
            logger.warn('Call answer failed: ' + e);
        }, video, resolution);
    }

    this.answerAudioCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, false);
        }
    };

    this.answerVideoCall = function (callId) {
        var call = calls[callId];
        if (call) {
            answerCall(call, true, '320x240');
        }
    };

    this.endCall = function (callId) {
        var call = calls[callId];
        if (call) {
            audioToneService.stop(audioToneService.RING_OUT);
            call.end(function () {
                logger.log('Call ended!');
                if (callService.isActiveCall(callId)) {
                    rendererService.setStatus("call ended");
                }
                callService.endCall(callId);
                //delete calls[callId];
                self.deleteFromCalls(callId);
                setTimeout(function () {
                    rendererService.renderCalls();
                }, 1000);
            }, function (error) {
                message.error('Call could not be ended', 2000);
                logger.warn('Call could not be ended! ' + error);
            });
        }
    };

    this.holdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Holding call ' + call.callerNumber + '...');
            call.hold(function () {
                logger.log('Call held ' + call.callerNumber);
                callService.holdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be held ' + call.callerNumber, 2000);
                logger.warn('Call could not be held! ' + error);
            });
        }
    };

    this.unholdCall = function (callId) {
        var call = calls[callId];
        if (call) {
            logger.log('Unholding call ' + call.callerNumber + '...');
            var activeCall = callService.getActiveCall();
            if (activeCall) {
                logger.log('Active calls getting held...');
                this.holdCall(activeCall.id);
            }
            call.unhold(function () {
                logger.log('Call unheld ' + call.callerNumber);
                callService.unholdCall(callId);
                rendererService.renderCalls();
            }, function (error) {
                message.error('Call could not be retrieved' + call.callerNumber, 2000);
                logger.warn('Call could not be retrieved! ' + error);
            });
        }
    };

    this.muteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.mute();
            callService.muteCall(callId);
            rendererService.renderCalls();
        }
    };
    this.unmuteCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.unmute();
            callService.unmuteCall(callId);
            rendererService.renderCalls();
        }
    };

    this.ignoreCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.ignore(function () {
                logger.log('Ignore call successful');
            }, function () {
                message.error('Call could not be ignored', 2000);
                logger.warn('Ignore call failed');
            });
        }
        audioToneService.stop(audioToneService.RING_IN);
        callService.ignoreIncomingCall(callId);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.declineCall = function (callId) {
        var call = calls[callId];
        if (call) {
            call.reject(function () {
                logger.log('Decline call successful');
            }, function () {
                message.error('Call could not be rejected', 2000);
                logger.warn('Decline call failed');
            });
        }
        callService.declineIncomingCall(callId);
        audioToneService.stop(audioToneService.RING_IN);
        callService.deleteIncomingCall(callId);
        rendererService.renderCalls();
        rendererService.renderIncomingCalls();
    };

    this.sendDTMF = function (callId, tone) {
        var call = calls[callId];
        if (call) {
            dtmfService.sendDTMF(call, tone);
        }
    };

    // searches SF contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchDirectory = function (criteria, successCallback) {
        fcs.addressbook.searchDirectory(criteria, fcs.addressbook.SearchType.USERNAME, function (contacts) {
            if (successCallback)
                successCallback(contacts);
        }, function (error) {
            message.error('Search contact failed', 2000);
            logger.warn('Search contact failed :' + error);
        });
    };

    this.retrieveCalllog = function (onSuccess, onFailure) {
        SFService.loadCredentials(function (credentials) {
            self.setUserAuth(credentials);
            self.setup(credentials);
            fcs.calllog.retrieve(onSuccess, onFailure);
        });
    };
}();
