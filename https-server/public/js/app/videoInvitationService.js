/* global sforce, fcs, rendererService, message */

var videoInvitationService = new function () {
    var container_selector = '#send_invitation_dialog',
            invitationMailTo;

    // opens add note dialog (create if not exists, otherwise show)
    this.openDialog = function (mailTo) {
        invitationMailTo = mailTo;
        if (!$(container_selector).hasClass('ui-dialog-content')) {
            createDialog();
        }
        $(container_selector).dialog("open");
    };
    // creates add note dialog
    function createDialog() {
        $(container_selector).dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 175,
            height: 330,
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Send: function () {

                    var $this = $(container_selector),
                            email = $this.find('#invitation_email').val(),
                            valid_from = $this.find('#valid_from').val(),
                            valid_to = $this.find('#valid_to').val();

                    if (email) {
                        sendVideoInvitation(email, new Date(valid_from), new Date(valid_to));
                        $this.dialog("close");
                    }
                    else {
                        message.warning('Please enter an email address', 2000);
                    }
                },
                Cancel: function () {
                    $(container_selector).dialog("close");
                }
            },
            open: function () {
                var $this = $(container_selector);
                $this.find('#invitation_email').val(invitationMailTo);

                var $valid_from = $this.find("#valid_from"),
                        $valid_to = $this.find("#valid_to");

                $this.find('.valid_datepicker').datetimepicker({
                    yearRange: 'c:c',
                    minDateTime: new Date()
                });

                // Set date picker default states
                $valid_from.datetimepicker('setDate', null);
                $valid_to.datetimepicker('setDate', null)
                        .datetimepicker("disable");

                $valid_from.datetimepicker("option", "onSelect", function (text, datepicker) {
                    // Reset "Valid to", once "Valid from is set"
                    var minDate = $valid_from.datetimepicker('getDate'),
                            minDateTime2 = new Date(minDate.getTime() + 60000);
                    $valid_to.datetimepicker('setDate', null)
                            .datetimepicker('option', 'minDate', minDate)
                            .datetimepicker('option', 'minDateTime', minDateTime2)
                            .datetimepicker("enable");
                });
            }
        });
    }
    function sendVideoInvitation(toEmail, validFrom, validTo) {
        var invitationUrl = prepareInvitationUrl(validFrom, validTo),
                singleRequest = new sforce.SingleEmailMessage(),
                sendMailRes;


        singleRequest.subject = "Video Call Invitation From Genband User";
        singleRequest.htmlBody = rendererService.templateEngine($('#video_invitation_tmpl').html(), {
            currentUser: fcs.getUser(),
            invitationUrl: invitationUrl,
            validFrom: timeConverter(validFrom),
            validTo: timeConverter(validTo)
        });

        singleRequest.toAddresses = [toEmail];

        sendMailRes = sforce.connection.sendEmail([singleRequest]);

        if (sendMailRes[0].success) {
            message.info('Invitation email sent successfully', 2000);
            console.log("invitation email sent successfully");
        }
        else {
            message.error('Error in sending invitation email', 2000);
            console.log("error sending invitation email");
            console.log(sendMailRes);
        }
    }

    function prepareInvitationUrl(validFrom, validTo) {
        var url = window.config.serverUrl + "/genband_call.html#call?to=" + fcs.getUser() + "&rest_server=" + fcs.fcsConfig.restUrl + "&rest_port=" + fcs.fcsConfig.restPort + "&ws_server=" + fcs.fcsConfig.websocketIP + "&ws_port=" + fcs.fcsConfig.websocketPort;

        if (validFrom) {
            var validFromUnixTimestamp = (+validFrom);
            url = url + "&valid_from=" + validFromUnixTimestamp;
        }
        if (validTo) {
            var validToUnixTimestamp = (+validTo);
            url = url + "&valid_to=" + validToUnixTimestamp;
        }

        return url;
    }



    function timeConverter(dateTime) {
        function pad(n) {
            return n < 10 ? '0' + n : n;
        }
        if (!+dateTime) {
            return;
        }

        var d = new Date(dateTime),
                months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                year = d.getFullYear(),
                month = months[d.getMonth()],
                date = pad(d.getDate()),
                hour = pad(d.getHours()),
                min = pad(d.getMinutes()),
                time = month + ' ' + date + ', ' + year + ' ' + hour + ':' + min;

        return time;
    }
}();