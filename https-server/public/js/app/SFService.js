/* global  sforce, logger */

var SFService = new function () {
    var self = this;

    // initializes sforce session id
    this.initSFApi = function () {
        if (!window.__sfdcSessionId) {
            sforce.sessionId = getCookie('sid');
        } else {
            sforce.sessionId = window.__sfdcSessionId;
        }

        sforce.connection.sessionId = sforce.sessionId;

        if (sforce.connection.sessionId) {
            logger.log('sforce api initialized successfully');
        } else {
            logger.warn('sforce api initialization error');
        }
    };

    // returns ajax request for SF (apex classes)
    this.loadSFData = function (methodName, param) {
        if (!param)
            param = '';
        var requestBody = '<se:Envelope xmlns:se="http://schemas.xmlsoap.org/soap/envelope/">' +
                '<se:Header xmlns:sfns="http://soap.sforce.com/schemas/package/GenbandController">' +
                '<sfns:SessionHeader><sessionId>' + sforce.sessionId + '</sessionId></sfns:SessionHeader>' +
                '</se:Header>' +
                '<se:Body>' +
                '<' + methodName + ' xmlns="http://soap.sforce.com/schemas/package/GenbandController">' +
                param +
                '</' + methodName + '>' +
                '</se:Body>' +
                '</se:Envelope>';
        return $.ajax({
            url: '../services/Soap/package/GenbandController',
            type: 'POST',
            data: requestBody,
            headers: {
                "SOAPAction": '""',
                "Content-Type": "text/xml; charset=UTF-8"
            }
        });
    };

    // parses result retrieved from SF
    this.parseSFResult = function (xml) {
        return $(xml).find('result').text();
    };

    // retrieves credentials from SF async and calls successCallback with credentials object
    this.loadCredentials = function (successCallback) {
        $.when(
                self.loadSFData('loadGenbandCredentials')
                ).done(function (creds) {
                    var credentials = {};
                    jscreds = self.parseSFResult(creds);
                    parsedCreds = JSON.parse(jscreds);

                    var sp_server =  parsedCreds[2];
                    var sp_wsServer = parsedCreds[3];

                    credentials.username = parsedCreds[0];
                    credentials.password = parsedCreds[1];
                    credentials.iceServerUrl = parsedCreds[4];
                    credentials.webRtcDTLS = parsedCreds[3];
                    var url = document.createElement('a');

                    if (sp_server.indexOf("://") === -1) {
                        url.href = "https://" + sp_server;
                    } else {
                        url.href = sp_server;
                    }

                    credentials.rest_server = url.hostname;
                    credentials.rest_port = getPort(url);

                    var webSocketServerUrl = document.createElement('a');

                    if (sp_wsServer.indexOf("://") === -1) {
                        webSocketServerUrl.href = "http://" + sp_wsServer;
                    } else {
                        webSocketServerUrl.href = sp_wsServer;
                    }

                    credentials.ws_server = webSocketServerUrl.hostname;
                    credentials.ws_port = getPort(webSocketServerUrl);

                if (successCallback) {
                    successCallback(credentials);
                }
        });

        function getPort(url) {
            if (!url.port) {
                if (url.protocol === "https:") {
                    return 443;
                } else if (url.protocol === "http:") {
                    return 80;
                }
            } else {
                return url.port;
            }
        }
    };

    if (!window._testonly)
        this.initSFApi();
}();