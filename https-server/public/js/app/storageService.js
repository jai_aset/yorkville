var storageService = new function () {
    var self = this;
    
    // if any change in localstorage, calls storageEventHandler function with parameter StorageEvent
    this.listen = function (storageEventHandler) {
        window.addEventListener('storage', storageEventHandler, false);
    };

    // returns item from localstorage as string
    this.getItem = function (key) {
        return localStorage.getItem(key);
    };

    // returns item from localstorage as JSON object
    this.getItemAsJSON = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };

    // sets value of item
    this.setItem = function (key, value) {
        localStorage.setItem(key, value);
    };

    // converts JSON object to string and sets as value of item
    this.setItemFromJSON = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };

    // removes item from localstorage
    this.removeItem = function (key) {
        localStorage.removeItem(key);
    };

    // clears localstorage saved by app
    this.clear = function () {
        self.removeItem('calls');
        self.removeItem('call_status');
        self.removeItem('incoming_call');
        self.removeItem('outgoing_call');
        self.removeItem('end_call');
        self.removeItem('hold_call');
        self.removeItem('unhold_call');
        self.removeItem('mute_call');
        self.removeItem('unmute_call');
        self.removeItem('popup_opened');
    };
}();