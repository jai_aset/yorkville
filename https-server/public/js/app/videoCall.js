/* global fcs, fcsService, audioToneService */

(function () {
    var isAnonymous = true,
            contact = {},
            mediaDeferred = $.Deferred(),
            $videoContainer = $("#plug-in-container"),
            currentCall = null,
            intraframe = null,
            hashParams = getUrlHashParameters(window.location.hash),
            address = hashParams.to,
            streamURL;

    if (window._testonly) {
        window.setHashParams = function (hash) {
            hashParams = getUrlHashParameters(hash);
        };
        window.setCurrentCall = function (call) {
            currentCall = call;
        };
    }

    audioToneService.init();

    $(function () {
        $("#media_plugin_dialog").dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 358,
            height: 235,
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Download: function () {
                    window.open(window.config.pluginLink, "_blank");
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            open: function () {

            }
        });
    });

    $('#start_button').on('click', function () {
        var subscribeDeferred = $.Deferred(),
                valid_from = hashParams.valid_from,
                valid_to = hashParams.valid_to,
                rest_server = hashParams.rest_server,
                rest_port = hashParams.rest_port,
                webSocketServer = hashParams.ws_server,
                websocketPort = hashParams.ws_port,
                current_time = +new Date;

        $('.error').hide();
        // Check url validity
        if (valid_from && current_time < valid_from) {
            $('#meeting_not_started_error').show();
            return;
        }

        if (valid_to && current_time > valid_to) {
            $('#meeting_over_error').show();
            return;
        }

        contact.firstName = $('#first_name').val().trim();
        contact.lastName = $('#last_name').val().trim();

        if (!contact.firstName.length && !contact.lastName.length) {
            $('#no_name_error').show();
            return;
        }

        fcsService.setUserAuth({
            username: address
        });

        fcsService.setup({
            rest_server: rest_server,
            rest_port: rest_port,
            ws_server: webSocketServer,
            ws_port: websocketPort
        });

        fcsService.notificationStart(
                function () {
                    subscribeDeferred.resolve();
                },
                function (e) {
                    console.log("Subscribe error: ", e);
                    subscribeDeferred.reject();
                },
                isAnonymous
                );

        if (mediaDeferred.state() === "pending") {
            fcsService.initMedia(
                    function () {
                        window.console.log("Media initialized successfully");
                        mediaDeferred.resolve();
                    },
                    function (error) {
                        window.console.log("Media initialization error: ", error);
                        $('#media_plugin_dialog').dialog("open");
                        mediaDeferred.reject();
                    }
            );
        }

        $.when(mediaDeferred, subscribeDeferred).done(function () {
            fcs.call.startCall(
                    'sip:anonymous@invalid.net',
                    contact,
                    address,
                    videoCallSuccess,
                    videoCallFailure,
                    true,
                    true
                    );
        });
    });

    $('#end_call').on('click', function () {
        audioToneService.stop(audioToneService.RING_OUT);

        if (!currentCall) {
            return;
        }

        currentCall.end(function () {
            disposeStream();
            currentCall = null;
            toggleLoginPage(true);
        });
    });

    function videoCallSuccess(call) {
        audioToneService.play(audioToneService.RING_OUT);
        currentCall = call;

        var fcsCallStates = fcs.call.States;

        call.onStreamAdded = function (url) {
            streamURL = url;
            renderStream();
        };

        call.onStateChange = function (state, statusCode) {
            //Play the ring out when we receive the 180
            if (state === fcsCallStates.RINGING && statusCode !== "183") {
                audioToneService.play(audioToneService.RING_OUT);
            } else {
                audioToneService.stop(audioToneService.RING_OUT);
            }

            if (state === fcsCallStates.IN_CALL) {
                audioToneService.stop(audioToneService.RING_OUT);

                // Workaround for video plugin, which shows local streaming
                // only after container resize
                $videoContainer.height(400);
                startIntraFrame();
            } else if (state === fcsCallStates.ENDED) {
                disposeStream();
                toggleLoginPage(true);
            } else if (state === fcsCallStates.ON_HOLD) {
                disposeStream();
                stopIntraFrame();
            }
        };

        toggleLoginPage(false);
    }

    function videoCallFailure(e) {
        // TODO: Handle error case
    }

    function stopIntraFrame() {
        if (intraframe) {
            clearInterval(intraframe);
        }
    }

    function startIntraFrame() {
        intraframe = setInterval(function () {
            if (currentCall && currentCall.canSendVideo()) {
                currentCall.sendIntraFrame();
            } else {
                stopIntraFrame();
            }
        },
                5000);
    }

    function toggleLoginPage(show) {
        show ? $videoContainer.removeClass('visible') : $videoContainer.addClass('visible');
        $('#login_content').toggle(show);
        $('#buttons').toggle(!show);

        if (!show) {
            return;
        }

        clearResources(
                function () {
                    setTimeout(function () {
                        if (!window._testonly) {
                            window.location.reload();
                        }
                    }, 1000);
                },
                true,
                false
                );
    }

    function getUrlHashParameters(hash) {
        var paramsStart = hash.indexOf('?'),
                pairString,
                pairs = {},
                pairStrings = paramsStart > -1 ? hash.slice(paramsStart + 1).split('&') : [],
                pair = {};
        for (var i = 0; i < pairStrings.length; i++) {
            pairString = pairStrings[ i ];
            pair = pairString.split('='); // Split into name/value array
            pairs[ pair[ 0 ] ] = pair[ 1 ];
        }
        return pairs;
    }

    function clearResources(done, clearUserCredentials, synchronous) {
        fcs.notification.stop(function () {
            //onsuccess
            window.localStorage.removeItem("SubscriptionStamp");
        }, function () {
            //onfailure, can be used in the future
        }, true);
        if (clearUserCredentials) {
            window.localStorage.removeItem("USERNAME");
            window.localStorage.removeItem("PASSWORD");
        }
        if (typeof done === 'function') {
            done();
        }
    }

    function renderStream() {
        streamURL && fcs.call.createStreamRenderer(streamURL, $("#plug-in-container").get(0));
    }

    function disposeStream() {
        streamURL && fcs.call.disposeStreamRenderer(streamURL, $("#plug-in-container").get(0));
    }
})();