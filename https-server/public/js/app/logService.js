/* global fcsService, contactService, SFService, sforce, logger */

var logService = new function () {
    this.updateActivityHistory = function () {
        logger.info('Updating activity history...');
        getCallLogs(function (calllogs) {

            // Format local times in calllogs
            for (var i = 0; i < calllogs.length; i++){
                calllogs[i].sfTime = moment(calllogs[i].startTime).format('h:mm a').toString().toUpperCase();
            }

            SFService.loadSFData('logActivities', '<userID>' + window.UserContext.userId + '</userID><json>' + JSON.stringify(calllogs) + '</json>')
                    .done(function () {
                        logger.log('Activity history successfully updated');
                    })
                    .fail(function (e) {
                        logger.warn('Activity history update failed', e);
                    });
        });
    };
    //isSet is return format, if true, repetitions eliminated
    function getCallLogs(successCallback, isSet) {
        fcsService.retrieveCalllog(function (calllogs) {
            var calllog, distinctCalllogs = [];
            for (var i = 0; i < calllogs.length; i++) {
                calllog = calllogs[i];
                calllog.address = calllog.address.trim();
                if (distinctCalllogs.indexOf(calllog.address) < 0) {
                    distinctCalllogs.push(calllog.address);
                }
            }

            contactService.getSFContacts(JSON.stringify(distinctCalllogs),
                    function (contactsMap) {
                        var calllogsNew = [];
                        for (var i = 0; i < calllogs.length; i++) {
                            var calllog = calllogs[i], filteredContacts = contactsMap[calllog.address];
                            calllog.startTime = new Date(calllog.startTime);
                            calllog.mili = calllog.startTime.getTime();
                            switch (calllog.type) {
                                case 0:
                                    calllog.type_label = "Incoming";
                                    break;
                                case 1:
                                    calllog.type_label = "Missed";
                                    break;
                                case 2:
                                    calllog.type_label = "Outgoing";
                                    break;
                            }

                            if (isSet && filteredContacts.length > 0) {
                                var newCallLog = jQuery.extend({}, calllog);
                                newCallLog.name = filteredContacts[0].Name;
                                var altName = '';
                                for (var j = 1; j < filteredContacts.length; j++) {
                                    altName += filteredContacts[j].Name;
                                    if (j < filteredContacts.length - 1) {
                                        altName += ', ';
                                    }
                                }
                                if (altName !== '') {
                                    newCallLog.name += ' (' + altName + ')';
                                }

                                newCallLog.contactId = filteredContacts[0].Id;
                                newCallLog.id = calllog.id + ':' + calllog.mili + ':' + newCallLog.contactId;
                                calllogsNew.push(newCallLog);
                            }
                            else {
                                for (var j = 0; j < filteredContacts.length; j++) {
                                    var newCallLog = jQuery.extend({}, calllog);
                                    newCallLog.name = filteredContacts[j].Name;
                                    newCallLog.contactId = filteredContacts[j].Id;
                                    newCallLog.id = calllog.id + ':' + calllog.mili + ':' + newCallLog.contactId;
                                    calllogsNew.push(newCallLog);
                                }
                            }
                        }
                                                    
                        calllogsNew = calllogsNew.sort(compare);
                        successCallback(calllogsNew);
                    },
                    function () {

                    });

        });
    }

    function compare(a, b) {
        if (a.mili > b.mili)
            return -1;
        if (a.mili < b.mili)
            return 1;
        return 0;
    }

    this.refreshCallLogGrid = function () {
        logger.info('Refreshing call log grid...');
        getCallLogs(function (calllogs) {
            $('#spidr_call_logs').jqGrid('setGridParam', {data: calllogs}).trigger('reloadGrid');
            logger.log('Call log grid successfully refreshed');
        }, true);
    };
}();