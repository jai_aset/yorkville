/* global storageService, sforce, message */

var noteService = new function () {
    var container_selector = '#add_note_dialog',
            secondClick = false;

    // opens add note dialog (create if not exists, otherwise show)
    this.openDialog = function () {
        if (!$(container_selector).hasClass('ui-dialog-content')) {
            createDialog();
        }
        $(container_selector).dialog("open");
    };
    // creates add note dialog
    function createDialog() {
        $(container_selector).dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 400,
            height: 'auto',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Add: function () {
                    addNote();
                },
                Cancel: function () {
                    $(container_selector).dialog("close");
                }
            },
            open: function () {
                $('textarea#note').val('');
                $('#popup').find('.add_note').addClass('add_note_active');
                //var activeCall = storageApi.getActiveCall();
                //var note = sforce.connection.query("SELECT Body FROM Note WHERE Id = '" + activeCall.id + "'");
                //$('textarea#note').val(note.records.Body);
            },
            close: function () {
                $('#popup').find('.add_note_active').removeClass('add_note_active');
            }
        });
    }

    // validates form and saves contact to SF
    function addNote() {
        var $dialog = $(container_selector),
                $notes = $('textarea#note'),
                note = $notes.val(),
                result = null;

        if (note) {
            var activeCall = callService.getActiveCall(),
                    sfNote = new sforce.SObject('Note');
            sfNote.parentId = activeCall.sf_contact_id;
            var title = 'Call-Note (call ';
            if (activeCall.type === 'in')
                title += 'from ';
            else
                title += 'to ';
            title += activeCall.contact + ')';

            sfNote.Title = title;
            sfNote.Body = note;
            //sfNote.Id = activeCall.id;

            if (!secondClick) {
                secondClick = true;
                result = sforce.connection.upsert('Id', [sfNote]);
                secondClick = false;

                if (result[0].getBoolean('success')) {
                    message.info('Note is successfully saved',1500);
                    setTimeout(function () {
                        $dialog.dialog("close");
                    }, 1500);
                } else {
                    message.warning('Please add this user in contacts',1500);
                }
            }
        } else {
            message.warning('Please add a new note',1500);
        }
    }

}();