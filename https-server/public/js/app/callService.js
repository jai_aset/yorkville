/* global storageService */

var callService = new function () {
    // returns true if any record on object
    function has(calls) {
        if (calls && Object.keys(calls).length > 0)
            return true;
        return false;
    }


    // returns true if any outgoing call on localstorage
    this.hasOutgoingCall = function () {
        return has(this.getOutgoingCall());
    };
    // returns outgoing call as an object
    this.getOutgoingCall = function () {
        return storageService.getItemAsJSON('outgoing_call') || {};
    };
    // sets outgoing call
    this.addOutgoingCall = function (contact, number, entityId) {
        var call = {
            'contact': contact,
            'number': number,
            'sf_contact_id': entityId,
            'date': (new Date()).getTime()
        };
        storageService.setItemFromJSON('outgoing_call', call);
    };
    // deletes outgoing call
    this.deleteOutgoingCall = function () {
        storageService.removeItem('outgoing_call');
    };


    // returns true if any incoming call on localstorage
    this.hasIncomingCall = function () {
        return has(this.getIncomingCalls());
    };
    // returns incoming calls as an object
    this.getIncomingCalls = function () {
        return storageService.getItemAsJSON('incoming_call') || {};
    };
    // adds incoming call
    this.addIncomingCall = function (call) {
        var calls = this.getIncomingCalls();
        calls[call.id] = call;
        storageService.setItemFromJSON('incoming_call', calls);
    };
    // deletes incoming call
    this.deleteIncomingCall = function (callId) {
        var calls = this.getIncomingCalls();
        if (calls[callId])
            delete calls[callId];
        storageService.setItemFromJSON('incoming_call', calls);
    };
    // sets incoming call's command; answer/answer_video/decline/ignore/
    this.setIncomingCallCommand = function (callId, command) {
        var calls = this.getIncomingCalls();
        if (calls[callId])
            calls[callId].command = command;
        storageService.setItemFromJSON('incoming_call', calls);
    };
    // sets SF contact info of caller
    this.setIncomingCallSFContact = function (callId, contactName, contactId) {
        var calls = this.getIncomingCalls();
        if (calls[callId]) {
            calls[callId].contact = contactName;
            calls[callId].sf_contact_id = contactId;
        }
        storageService.setItemFromJSON('incoming_call', calls);
    };
    // sets contact photo url of caller
    this.setIncomingCallContactPhoto = function (callId, photoURL) {
        var calls = this.getIncomingCalls();
        if (calls[callId])
            calls[callId].contactPhoto = photoURL;
        storageService.setItemFromJSON('incoming_call', calls);
    };
    // sets incoming call's command as answer(if video true, sets as answer_video)
    this.answerIncomingCall = function (callId, video) {
        if (video)
            this.setIncomingCallCommand(callId, 'answer_video');
        else
            this.setIncomingCallCommand(callId, 'answer');
    };
    // sets incoming call's command as decline
    this.declineIncomingCall = function (callId) {
        this.setIncomingCallCommand(callId, 'decline');
    };
    // sets incoming call's command as ignore
    this.ignoreIncomingCall = function (callId) {
        this.setIncomingCallCommand(callId, 'ignore');
    };


    // sets call param and saves
    this.setCallParam = function (callId, param, value) {
        var calls = this.getCalls();
        if (calls[callId])
            (calls[callId])[param] = value;
        storageService.setItemFromJSON('calls', calls);
    };
    // returns true if any call on localstorage
    this.hasCall = function () {
        return has(this.getCalls());
    };
    // returns calls stored on localstorage as an object 
    this.getCalls = function () {
        return storageService.getItemAsJSON('calls') || {};
    };
    // adds new call object to calls object
    this.addCall = function (call) {
        var calls = this.getCalls();
        calls[call.id] = call;
        storageService.setItemFromJSON('calls', calls);
    };
    // deletes call from calls
    this.endCall = function (callId) {
        var calls = this.getCalls();
        if (calls[callId])
            delete calls[callId];
        storageService.setItemFromJSON('calls', calls);
    };
    // sets call status as hold
    this.holdCall = function (callId) {
        this.setCallParam(callId, 'status', 'hold');
    };
    // sets call status as active
    this.unholdCall = function (callId) {
        this.setCallParam(callId, 'status', 'active');
    };
    // sets mute param of call as 1 means muted
    this.muteCall = function (callId) {
        this.setCallParam(callId, 'mute', 1);
    };
    // sets mute param of call as 0 means unmuted
    this.unmuteCall = function (callId) {
        this.setCallParam(callId, 'mute', 0);
    };

    // sets start time of call to current time
    this.setCallStartTime = function (callId) {
        var calls = this.getCalls(),
                param = 'start_time';
        if (calls[callId] && !(calls[callId])[param])
            this.setCallParam(callId, param, (new Date()).getTime());
    };
    // sets SF contact id of callee
    this.setCallSFContact = function (callId, contactId) {
        this.setCallParam(callId, 'sf_contact_id', contactId);
    };
    // sets contact photo url of callee
    this.setCallContactPhoto = function (callId, photoURL) {
        this.setCallParam(callId, 'contactPhoto', photoURL);
    };
    // returns if any active call (not held) as object
    this.getActiveCall = function () {
        var calls = this.getCalls(),
                activeCall;
        jQuery.each(calls, function (callId, call) {
            if (call.status === 'active') {
                activeCall = call;
            }
        });
        return activeCall;
    };
    // returns if call is active
    this.isActiveCall = function (callId) {
        var calls = this.getCalls();
        var isActive = false;
        jQuery.each(calls, function (cid, call) {
            if (call.status === 'active' && cid === callId) {
                isActive = true;
            }
        });
        return isActive;
    };
    // returns if any held call as calls object
    this.getInactiveCalls = function () {
        var calls = this.getCalls();
        jQuery.each(calls, function (callId, call) {
            if (call.status === 'active') {
                delete calls[callId];
            }
        });
        if (Object.keys(calls).length > 0)
            return calls;
        return;
    };


    this.trigger = new function () {
        // adds new item to localstorage for popup to end call
        this.endCall = function (callId) {
            storageService.setItem('end_call', callId);
            window.render_is_needed = true;
            rendererService.renderCalls();
        };

        // adds new item to localstorage for popup to hold call
        this.holdCall = function (callId) {
            storageService.setItem('hold_call', callId);
        };

        // adds new item to localstorage for popup to unhold call
        this.unholdCall = function (callId) {
            storageService.setItem('unhold_call', callId);
        };

        // adds new item to localstorage for popup to mute call
        this.muteCall = function (callId) {
            storageService.setItem('mute_call', callId);
        };

        // adds new item to localstorage for popup to unmute call
        this.unmuteCall = function (callId) {
            storageService.setItem('unmute_call', callId);
        };
    }();

}();