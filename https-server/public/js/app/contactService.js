/* global sforce, callService, SFService, fcsService, message, storageService, logger, windowService */

var contactService = new function () {
    var container_selector = '#add_contact_dialog',
            dialogCaller,
            self = this;

    // opens add contact dialog (create if not exists, otherwise show)
    this.openDialog = function (number) {
        dialogCaller = number;
        if (!$(container_selector).hasClass('ui-dialog-content')) {
            createDialog();
        }
        $(container_selector).dialog("open");
    };
    // creates add contact dialog
    function createDialog(caller) {
        $(container_selector).dialog({
            dialogClass: "no-close",
            autoOpen: false,
            width: 400,
            height: 'auto',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Add: function () {
                    addContact();
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            open: function () {
                var number;
                // Set default value of phone field
                if (dialogCaller) {
                    number = dialogCaller.substr(0, dialogCaller.indexOf('@'));
                    if (number && !isNaN(number)) {
                        $(this).find('#add_contact_phone').val(number);
                    } else {
                        $(this).find('#add_contact_phone').val(dialogCaller);
                    }
                }
                $('#contact-existance').hide();
            }
        });
    }

    // validates form and checks existing (similar) contacts
    function addContact() {
        var $dialog = $(container_selector),
                caller = $dialog.find('#add_contact_phone').val(),
                firstName = $dialog.find('#add_contact_firstname').val(),
                lastName = $dialog.find('#add_contact_lastname').val(),
                type = $('#add_contact_select_type').val(),
                contactId = null;

        $dialog.off('click');
        if (lastName) {
            var contactInfo = {
                'firstName': firstName || '',
                'lastName': lastName
            },
            contact = new sforce.SObject('Contact'),
                    contacts = sforce.apex.execute("GenbandController", "checkContactExistence", contactInfo);

            if (contacts.length) {
                $('#contact-existance').show();
                $('.existed-contacts').empty();

                contacts.forEach(function (item) {
                    if (item.Name) {
                        $('.existed-contacts').append('<div class="contacts-container"><span class="existed-contact" data-id="' + item.Id + '">' + item.Name + '</span></div>');
                    }
                });

                $dialog.on('click', '.existed-contact', function () {
                    var $this = $(this),
                            type = $('#add_contact_select_type').val(), // Checking If User Change His Decision
                            result = null;

                    contact.Id = $this.data('id');
                    contact[type] = caller;

                    result = sforce.connection.update([contact]);

                    if (result[0].getBoolean('success')) {
                        self.setContactTitle(caller);
                        $dialog.dialog("close");
                    } else {
                        message.error('Contact update failed', 2000);
                    }
                });

                $dialog.on('click', '#add-contact-anyway', function () {
                    contactId = addContactToSalesforce(caller, type, firstName, lastName);
                    self.setContactTitle(caller);
                    $dialog.dialog("close");
                });
            } else {
                contactId = addContactToSalesforce(caller, type, firstName, lastName);
                self.setContactTitle(caller);
                $dialog.dialog("close");
            }
        } else {
            message.warning('Please Enter a Last Name', 2000);
        }
    }

    // saves contact to SF
    function addContactToSalesforce(phoneNumber, type, firstName, lastName) {
        var objContact = new sforce.SObject("Contact"),
                result = null;

        objContact[type] = phoneNumber;
        objContact.FirstName = firstName;
        objContact.LastName = lastName;
        objContact.OwnerId = window.UserContext.userId;

        result = sforce.connection.create([objContact]);

        return result[0].id;
    }

    // sets contact title of active call according to SF contact 
    this.setContactTitle = function (number, name) {
        logger.log('Setting contact title for number ' + number);
        var activeCall = callService.getActiveCall();
        if (activeCall.sf_contact_id) {
            logger.log('There is an active call with sf contact id: ' + activeCall.sf_contact_id);
            $("#active_call").find("#contact").html("<a id='contact_redirect' href='javascript:'>" + activeCall.contact + "</a>");
            $('#contact_redirect').click(function () {
                windowService.openContactPage(activeCall.sf_contact_id);
            });
        } else {
            logger.log('There is no active call to set title');
            if (!name)
                name = number;
            //number = '(212) 842-5611';//'meetme@genband.com';
            $("#active_call").find("#contact").text(name);
            $("#active_call").find("#contact").attr('title', number);
            if (number) {
                logger.log('Getting SF contacts with number: ' + number);
                self.getSFContacts(number, function (contactsMap) {
                    logger.log('Contacts map found: ' + JSON.stringify(contactsMap));
                    var contacts = contactsMap[number];
                    if (contacts && contacts.length > 0 && contacts[0].Id) {
                        var contact = contacts[0];
                        logger.log('The contact is found from map:' + JSON.stringify(contact));
                        callService.setCallSFContact(activeCall.id, contact.Id);
                        $("#active_call").find("#contact").html("<a id='contact_redirect' href='javascript:'>" + contact.Name + "</a>");
                        $('#contact_redirect').click(function () {
                            windowService.openContactPage(contact.Id);
                        });
                    } else {
                        logger.log('Contact not found from map. click for add');
                        $("#active_call").find("#contact").html("<a href='javascript:' id='contact_title' target='_parent'>" + number + " (Add Contact)</a>");
                        $('#popup').on('click', '#contact_title', function () {
                            if (!$("#add_contact_dialog").hasClass('ui-dialog-content')) {
                                contactService.openDialog(number);
                            }
                            $("#add_contact_dialog").dialog("open");
                        });
                    }
                });
            }
        }
    };

    // gets SF contact according to search param
    this.getSFContacts = function (callerId, onSuccess, onFailure) {
        SFService.loadSFData('getContacts', '<logAddresses>' + callerId + '</logAddresses>')
                .done(function (data) {
                    var result,
                            response = xmlToJson(data),
                            resultJson = response['soapenv:Envelope']['soapenv:Body']['getContactsResponse']['result']['#text'];
                    result = JSON.parse(resultJson);
                    onSuccess(result);
                })
                .fail(function () {
                    if (onFailure)
                        onFailure();
                });
    };

    // converts XML to JSON
    function xmlToJson(xml) {
        var obj = {};
        if (xml.nodeType === 1) {
            if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType === 3) {
            obj = xml.nodeValue;
        }
        if (xml.hasChildNodes()) {
            for (var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof (obj[nodeName]) === "undefined") {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if (typeof (obj[nodeName].push) === "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
        return obj;
    }

    // searches SF contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchSFContact = function (criteria, successCallback) {
        var callbacks = {
            onSuccess: function onSuccess(queryResult, source) {
                var contacts = null;
                if (queryResult.size > 0) {
                    contacts = queryResult.getArray('records');
                }
                if (successCallback)
                    successCallback(contacts);
            },
            onFailure: function (error) {
                logger.warn('searchContact: ' + error);
            }
        };
        var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone FROM Contact";
        if (criteria)
            query += " WHERE Name LIKE '%" + criteria + "%'";
        sforce.connection.query(query, callbacks);
    };

    // gets all SF contacts calls successCallback with contacts parameter
    this.getAllSFContacts = function (successCallback) {
        self.searchSFContact(null, successCallback);
    };

    // searches Spidr contacts regarding to criteria and calls successCallback with contacts parameter
    this.searchSpidrContact = function (criteria, successCallback) {
        fcsService.searchDirectory(criteria, successCallback);
    };

    // checks whether user navigated to a contact page. If so, calls successCallback with contact parameter
    this.checkCurrentContact = function (successCallback, failureCallback) {
        var entityId = window.__currentEntityId;
        var entityType = window.__currentEntityType;

        var contactId = window.__currentContactId;
//        if (contactId && contactId.length > 0) {
//            var callbacks = {
        var query = '';
        logger.log('checkCurrent[Entity]: entityId ' + entityId + 'entityType ' + entityType);
//        logger.log('checkCurrentContact: contactId ' + contactId + 'leadId ' + leadId + 'opportunityId ' + opportunityId + 'personId ' + personId + 'accountId ' + accountId);
        if (entityType && entityId)
        {
           logger.log('checkCurrent[Entity]: passed type check  ' + entityType);
           
           switch (entityType)
           {
               case 'Lead':
                   query = "SELECT Id, Name, Email, Phone, MobilePhone FROM Lead " +
                    "WHERE Id = '" + entityId + "'";
                    break;
                case 'Opportunity':
                    query = "SELECT Id, Name, Email_Address__c, Telephone__c, Cell_Phone__c FROM Opportunity " +
                    "WHERE Id = '" + entityId + "'";
                    break;
                case 'Contact':
                    query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
                          "FROM Contact WHERE Id='" + entityId + "'";
                   break;
                
                default:
                    break;
           }
           var callbacks = {
                onSuccess: function onSuccess(queryResult, source) {
                    var contacts = null;
                    if (queryResult.size > 0) {
                        window.name = entityId;
                        contacts = queryResult.getArray('records');
                        $.each(contacts, function (index, contact) {
                            if (contact && successCallback) {
                                successCallback(contact);
                            }
                        });
                    }
                },
                onFailure: function (error) {
                    logger.warn('checkCurrentContact: ' + error);
                }
            };
//            var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
//                    "FROM Contact WHERE Id='" + contactId + "'";
            sforce.connection.query(query, callbacks);
        }
        // leaving in contactid query for backwards compatibility
        else if (contactId && contactId.length > 0) {
            var callbacks = {
                onSuccess: function onSuccess(queryResult, source) {
                    var contacts = null;
                    if (queryResult.size > 0) {
                        window.name = contactId;
                        contacts = queryResult.getArray('records');
                        $.each(contacts, function (index, contact) {
                            if (contact && successCallback) {
                                successCallback(contact);
                            }
                        });
                    }
                },
                onFailure: function (error) {
                    logger.warn('checkCurrentContact: ' + error);
                }
            };
            var query = "SELECT Id, Name, Email, Phone, HomePhone, MobilePhone, OtherPhone, AssistantPhone " +
                    "FROM Contact WHERE Id='" + contactId + "'";
            sforce.connection.query(query, callbacks);
        }

        else if (failureCallback) {
            failureCallback();
        }
    };

}();