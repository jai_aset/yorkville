var https = require('https'),
    httpProxy = require('http-proxy'),
    express = require('express'),
    app = express(),
    path = require('path'),
    fs = require( 'fs' ),
    privateKey = fs.readFileSync( 'certificate/localhost.key' ).toString(),
    certificate = fs.readFileSync( 'certificate/localhost.cert' ).toString(),
    options = {key: privateKey, cert: certificate},
    proxyOptions = {host: '47.168.150.222', port: 8580},
    httpsServer;

app.use(express.static(path.join(__dirname, 'target/dev')));

app.all('*', function(req, res) {
    console.log(req);

    var proxy = new httpProxy.RoutingProxy();
    proxy.proxyRequest(req, res, proxyOptions);

    proxy.on('proxyError', function (err, req, res) {
        console.log(err);
        res.writeHead(500, {
          'Content-Type': 'text/plain'
        });

        res.end('Something went wrong.');
    });
});

httpsServer = https.createServer(options, app);
httpsServer.listen(7000);

console.log('Listening on port 7000');