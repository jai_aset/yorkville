# Genband SPiDR RTC Client for Salesforce

## Dependencies
### Install nodeJS
<https://nodejs.org/>
### Install ant
Available mirrors:
- <https://code.google.com/archive/p/winant/>
- <https://ant.apache.org/bindownload.cgi>

## Project Installation
* The project uses GruntJS javascript task runner.
(More about GruntJS <http://gruntjs.com/>)
* Open sfdc folder from command line and run following code to install grunt and phantomjs:
```
npm install -g grunt-cli phantomjs
```
* And then run this code to install grunt dependencies:
```
npm install
```

## Open Project
* If you want to use the "Build Button" of the Netbeans IDE, open/import downloaded project as HTML5 project with an existing source.

## Build
* You can build project for development from command line with following command;
```
grunt build_dev
```

## Deployment
* Before the deployment you must enter valid SFDC credentials (username, password and security token) from credentials.json file. 
* Put your valid SFDC credentials into `credentials.json` file.
* You can deploy the project to SFDC developer site for development purpose from command line with following command;
```
grunt deploy_dev
```
* You can deploy project to SFDC developer site for prod configuration (to prepare it for sandbox) from command line with following command;
```
grunt deploy_prod
```

## Run NodeJS https server
* You can run NodeJS https server from command line with following command;
```
node server.js
```

## Unit Test Environment
* In order to setup unit test environment download and install Karma with following code;
```
npm install karma --save-dev
```
* Install necessary Karma plug-ins.
```
npm install karma-jasmine karma-chrome-launcher --save-dev
```
* Install Karma command line interface.
```
npm install -g karma-cli
```
* Now you can run Karma from command line with following command;
```
karma start my.conf.js
```
* Download jasmine html reporter plug-in (OPTIONAL)
```
npm install karma-jasmine-html-reporter --save-dev
```

## Use Karma with Grunt
* Download grunt-karma with following code:
```
npm install grunt-karma --save-dev
```
* You can run tests on PhantomJS browser from command line with following command;
```
grunt tests
```
* You can run tests on Chrome browser from command line with following command;
```
grunt tests_chrome
```

## Force-com IDE (optional)
* You can use Eclipse as IDE for force-com part
https://developer.salesforce.com/page/Force.com_IDE_Installation