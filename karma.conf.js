/* global module */

// Karma configuration

module.exports = function (config) {
    config.set({
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],
        // list of files / patterns to load in the browser
        files: [
            'https-server/public/js/lib/jquery-1.10.2.min.js',
            'https-server/public/js/lib/jquery.nicescroll.min.js',
            'https-server/public/js/lib/jqgrid/js/jquery.jqGrid.min.js',
            'https-server/public/js/lib/jquery-ui/js/jquery-ui.custom.min.js',
            'https-server/public/js/lib/jquery-timepicker.js',
            'https-server/public/js/lib/fcs-jsl-3.1.1.js',
            'https-server/public/js/lib/moment.min.js',
            'https-server/test/**/globals.js',
            'https-server/test/**/*.js',
            'https-server/public/js/app/**/*.js'
        ],
        //will report all the tests that are slower than given time limit (in ms)
        reportSlowerThan: 50,
        // list of files to exclude
        exclude: [
            //'https-server/public/js/**/SFService.js'
        ],
        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'html', 'coverage', 'junit'],
        junitReporter: {
            outputFile: 'target/test/TESTS-report.xml'
        },
        coverageReporter: {
            // specify a common output directory
            dir: 'target/test/coverage',
            reporters: [
                {type: 'html', subdir: 'report-html'},
                {type: 'cobertura', subdir: '.', file: 'cobertura.xml'},
                {type: 'lcovonly', subdir: '.', file: 'report-lcovonly.txt'},
                {type: 'teamcity', subdir: '.', file: 'teamcity.txt'},
                {type: 'text', subdir: '.', file: 'text.txt'},
                {type: 'text-summary', subdir: '.', file: 'text-summary.txt'},
                {type: 'text'},
                {type: 'text-summary'}
            ]
        },
        // web server port
        port: 9876,
        // enable / disable colors in the output (reporters and logs)
        colors: true,
        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true

    });
};
